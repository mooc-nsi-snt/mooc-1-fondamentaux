# B2-M2 Programmation impérative, procédurale et autres paradigmes de programmation

## Objectifs

[![Vidéo 1 B2-M2-S0 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B2-M2-S0-V2.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B2-M2-S0-V2.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B2-M2/B2-M2-S0.srt" target="_blank">Sous-titre de la vidéo</a> -  <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B2-M2/B2-M2-S0-script.md" target="_blank">Transcription de la vidéo </a>


## Prérequis
Prerequis indiqués dans la présentation du Bloc 2

## Sommaire

1. Paradigmes des langages de programmation
2. Compilateur et interpréteur
3. Les langages impératifs
4. Typage des langages de programmation
5. Fonctions et modules
6. Histoire et taxonomie des langages de programmation

## Temps d'apprentissage

- 4 à 6 heures
*Ce temps est donné à titre indicatif. Il peut varier en fonction des participants.*


## Sources

Pour rédiger cette partie, nous nous sommes basé sur les supports :

- [Yves Roggeman, LANGAGES DE PROGRAMMATION, VOL. I (3ème édition) 2020](https://files.inria.fr/LearningLab_public/C045TV/DOCS-REFERENCE/Roggeman-Langages_I.pdf)

-  [Yves Roggeman, LANGAGES DE PROGRAMMATION, VOL. II (4ème édition) 2020](https://files.inria.fr/LearningLab_public/C045TV/DOCS-REFERENCE/Roggeman-Langages_II.pdf)

   voir aussi
   [https://fr.wikipedia.org/wiki/Paradigme_(programmation)](https://fr.wikipedia.org/wiki/Paradigme_(programmation))
   [http://projet.eu.org/pedago/sin/ICN/1ere/4-langages.pdf]( http://projet.eu.org/pedago/sin/ICN/1ere/4-langages.pdf) 

Nous invitons le lecteur à utiliser ces références pour compléter l'information résumée ici.


## Présentation enseignant

### Thierry Massart

Thierry Massart est professeur à l'Université Libre de Bruxelles (ULB) où, depuis plus de 25 ans, il enseigne la programmation principalement aux étudiants de Sciences Informatique et de l'école Polytechnique de l'ULB.
