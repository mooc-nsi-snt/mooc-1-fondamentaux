# Les langages impératifs

1. Paradigmes des langages de programmation
2. Compilateur et interpréteur
3. **Les langages impératifs**
4. Typage des langages de programmation
5. Fonctions et modules
6. Histoire et taxonomie des langages de programmation

####

* **Thierry Massart**.  Un langage est dit impératif s'il utilise le paradigme de programmation impérative...

[![Vidéo 1 B2-M2-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B2-M2-S3.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B2-M2-S3.mp4) 

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B2-M2/B2-M2-S3.srt" target="_blank">Sous-titre de la vidéo</a> -  <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B2-M2/B2-M2-S3-script.md" target="_blank">Transcription de la vidéo </a>


## Texte complémentaire 

<a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/Le_Mooc/bloc_2_langages_imperatifs_et_autres/2-2_paradigmes/2-2-3_Les_langages_imperatifs/2-2-3-2_Instructions.html" target="_blank">Texte complément à la video</a>
