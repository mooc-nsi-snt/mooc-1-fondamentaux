# B2-M4 Récursivité

## Objectifs du module

Introduire la notion de récursivité : son utilité pour résoudre divers problèmes algorithmiques et les mécanismes mis en oeuvre par l’interpréteur Python pour la réaliser. 

## Sommaire

    1. Introduction à la récursivité
    2. Mécanismes
    3. Structures de données récursives
    4. Mécanismes de transmission de valeurs et de résultats entre les instances récursives
    5. Traduction de fonction récursive en fonction itérative
    6. Gestion de la mémoire à l'exécution
    7. Exercices

## Prérequis

- Idem Bloc 2

## Temps d'apprentissage : 

- 4 à 6 heures
- *Ce temps est donné à titre indicatif. Il peut varier en fonction des participants.* 
 
## Enseignant
### Thierry Massart

Thierry Massart est professeur à l'Université Libre de Bruxelles (ULB) où, depuis plus de 25 ans, il enseigne la programmation principalement aux étudiants de Sciences Informatique et de l'école Polytechnique de l'ULB..
