import turtle
from math import pi, sin, cos

def koch(size, level):
    """
    trace un segment de Koch
    :param size: (int ≥ 0) taille du segment pour turtle
    :param level: (int ≥ 0) niveau de "profondeur" du segment 
                  (0 = segment simple) 
    :return: None
    """
    if level > 0:
        dl = int(size / 3.0)
        koch(dl, level - 1)
        turtle.left(60)
        koch(dl, level - 1)
        turtle.right(120)
        koch(dl, level - 1)
        turtle.left(60)
        koch(dl, level - 1)
    else:
        turtle.fd(size)


def draw_koch(size, level):
    turtle.penup()
    turtle.goto(-int(size / 2.0), +int(size / 3.3))
    turtle.pendown()
    koch(size, level)
    turtle.right(120)
    koch(size, level)
    turtle.right(120)
    koch(size, level)


# trace un flocon de Koch de niveau 4 et de taille de segment 200
turtle.speed(0)
turtle.color('red','pink')
turtle.begin_fill()
draw_koch(200, 4)
turtle.end_fill()
turtle.hideturtle()
turtle.done()
