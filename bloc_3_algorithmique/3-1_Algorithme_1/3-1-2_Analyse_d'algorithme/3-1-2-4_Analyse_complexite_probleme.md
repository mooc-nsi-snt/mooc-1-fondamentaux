## Complexité d'algorithme : analyse de la complexité d'un problème

1. Complexité d'algorithme : introduction et schéma algorithmique
2. Calcul de la complexité d'un algorithme
3. Complexité : ordres de grandeur et comparaison des algorithmes
4. **Analyse de la complexité d'un problème. Conclusion**
5. Preuve d'algorithme


[![Vidéo 4  B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1g.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1g.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S2/B3-M1-S2-video3c.srt" target="_blank">Sous-titre de la vidéo</a>

## Supports de présentation (diapos)

[Support de la présentation des vidéos 2 à 7](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S2/B3-M1-S2-part1.pdf)

