## Arbres binaires. 8/10

  1. Introduction à la séquence
  2. Types abstraits en algorithmique
  3. Pourquoi, pour qui ? 
  4. La pile
  5. La file
  6. Arbres - définition, propriétés, comptage part1 & part 2
  7. **Arbres binaires**
  8. Parcours d'arbre
  9. Feuilles étiquetées

[![Vidéo 8 B3-M1-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S4-V8.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S4-V8.mp4)

## Supports de présentation (diapos)

[Support de la présentation des vidéos 6 à 10](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S4/B3-M1-S4-part2.pdf)