## Présentation du module : Interface web (javascript, PHP..)

## Objectifs du module

Les sites web sont un domaine de l'informatique populaire, très visible et recrutant beaucoup et à tous les niveaux. Le but de ce module est de présenter, en peu de temps, les technologies du Web les plus représentatives : HTML, PHP et javascript. Les exercices amèneront l'apprenant à créer un site web dynamique minimaliste reprenant ces différents langages et les associant avec les bases de données.
Enfin, les framework propriétaires, très utilisés pour les sites web professionnels et reposant sur les technologies présentées ici, seront évoqués, les bases étant posées pour permettre à l'apprenant intéressé par la mise en œuvre de ces outils de suivre les tutoriaux disponibles sur Internet.


## Sommaire

   -  1 Introduction à HTML 5
      -  1-1 Base et structure d'une page web : le HTML
      -  1-2 HTML : éléments de base du HTML
      -  1-3 HTML : mise en forme et structures avancées
   -  2 Déploiement d'un site web
   -  3 Interactions et gestion dynamique dans une page web
   -  4 Frameworks de développement et outils de gestion de contenu
   
## TP 
   
Dans cette section, un <a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/Le_Mooc/bloc_4_architecture_robotique_reseaux_ihm/4-4_Technologies_web/4-4-5-TP/Introduction_aux_TPs.html">TP "Installation d'un serveur web"</a> vous est proposé : il vous permettra de faire, ensuite, les autres TP.</p>

## Prérequis

Avoir suivi les modules Réseaux, BDD, python, OS

## Temps d'apprentissage : 

* 15h

* *Ce temps est donné à titre indicatif. Il peut varier en fonction des participants.*


# Présentation de l'enseignant 

**Rodrigue Chakode**

Entrepreneur, Spécialiste des plateformes et applications cloud. 

[![Vidéo 1 B4-M4-S0 Présentation du module ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M4-S1-video1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M4-S1-video1.mp4)
