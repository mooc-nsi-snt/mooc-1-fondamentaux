# TP4 - Déploiement d'un site web (120 mn)

## Hébergement local
Une possibilité est d'héberger le site web localement, sur un serveur, une machine virtuelle ou un nano-ordinateur (raspberry Pi).
Pour une disponibilité correcte du site, il faut que la machine :

  * soit allumée en permanence,       
  * soit à jour du point de vue sécurité,    
  * ait un mot de passe robuste (pas de carte raspberry pi sur Internet avec login pi et mot de passe raspberry !!)  
  * ait une url fixe (le site http://freeddns.noip.com permet d'associer un nom de domaine fixe à l'IP d'une box Internet, même si l'IP change régulièrement).
  
Il est alors complexe et souvent payant d'obtenir un certificat SSL pérenne pour passer en https.

C'est pourquoi la plupart des particuliers et associations passent par un hébergement chez un fournisseur Cloud (voir le paragraphe en fin de TP)

L'installation d'un serveur web personnel est cependant très formateur pour bien comprendre les technologies web. L'installation de la machine dans une DMZ limite les risques si la machine est attaquée.

On travaille dans cet exemple avec un serveur apache installé sur une machine virtuelle. Il est possible de faire les mêmes expérimentations sur une raspberry Pi ou sur une machine physique. Les adeptes de Windows et Mac trouveront facilement comment installer wamp (pour windows apache mysql php) ou mamp (pour mac apache mysql php).

<img src="image-15.png" alt="serveur apache sur VM" width="600px;">

### Installation du serveur web

Sur une machine Linux (PC, machine virtuelle ou une carte raspberry Pi), mettre à jour les dépôts et les paquets puis installer Apache : 
* sudo apt update
* sudo apt upgrade
* sudo apt install apache2

Le serveur HTTP (serveur web) installé, il est possible de l’interroger avec un client HTTP (un navigateur web).
* Si le client est extérieur, on indique l’adresse IP du serveur (si c'est une machine virtuelle, l'accès depuis un client extérieur sera expliqué plus loin dans le TP).
* Si le navigateur est sur le serveur, on utilise l’adresse localhost 127.0.0.1.
On obtient alors la page Web par défaut de Apache.

<img src="image-14.png" alt="serveur http" width="600px;">


Il est possible de vérifier le fonctionnement du serveur apache en exécutant la commande suivante sur une console du serveur web :
• sudo service apache2 status

### Dépôt de la page web sur le serveur

Une fois le serveur installé, il faut déposer dans le dossier /var/www/html les fichiers du site web : index.html et les autres (images, feuilles de style...).
Pour plus de sécurité, le dossier www n'est modifiable que par root et c'est mieux de le laisser ainsi.
Pour la période de conception et test du site, non visible sur Internet, il est possible de se donner les droits en écriture sur le dossier :
* sudo chmod +007 /var/www/html
Avant de rendre le site visible, on retirera les droits en écriture sur le dossier html à l'utilisateur :
* sudo chmod -002 /var/www/html

L'autre solution est de travailler dans un dossier Documents et de faire des copies en ligne de commande :
* sudo cp nom_fichier_source /var/www/html

Supprimer le fichier index.html d'apache. Ce fichier appartient à root, on peut le supprimer avec la commande suivante : 
* sudo rm /var/www/html/index.html 

Copier le fichier index.html réalisé précédemment et ses dépendances dans /var/www/html.
Il est possible que le serveur web (qui utilise par défaut le nom d'utilisateur www-data) n'ait pas accès aux nouveaux fichier. On donne accès aux fichiers et sous-dossiers de /var/www/html au serveur web par la commande suivante : 
* sudo chgrp -R www-data /var/www/html

Afficher la page web personnelle sur le serveur web local d'adresse localhost 127.0.0.1

<img src="image-17.png" alt="page web" width="600px;">


### Visibilité depuis le réseau local
Si le serveur web est sur la machine hôte ou sur un nano-ordinateur raspberry pi, il suffit d'indiquer depuis le navigateur d'une autre machine du réseau local l'adresse IP du serveur pour afficher sa page web.

Si le serveur est sur une machine virtuelle, il faut rediriger un port de la machine physique vers le port 80 de la machine virtuelle, comme cela se ferait avec un routeur. 


<img src="image-23.png" alt="serveur web" width="600px;">


Pour cela, VirtualBox propose un menu de redirection de ports qu'il est mieux d'utiliser machine virtuelle éteinte.
Choisir pour cela la machine virtuelle hébergeant le serveur puis Configuration > Réseaux > Avancé > Redirection de ports.


<img src="image-20.png" alt="redirection port virtualBox" width="600px;">


Ensuite, choisir un port de la machine physique pour la redirection vers le port 80 de la machine invitée (la machine invitée est la machine virtuelle). Prendre 8080 par exemple, pour éviter un conflit avec un éventuel serveur web qui tournerait sur la machine hôte.

<img src="image-21.png" alt="port 8080 pour serveur web" width="600px;">

On peut ainsi accéder à la page web du serveur hébergé sur la machine virtuelle depuis la machine hôte, sur le port 8080 :

<img src="image-18.png" alt="url page web port 8080" width="600px;">

Il est alors possible d'accéder à la page web depuis le navigateur d'une autre machine du réseau (par exemple un smartphone connecté en Wifi), toujours sur le port 8080, en indiquant l'adresse IP de la machine hôte.

<img src="image-19.png" alt="adresse IP page web" width="600px;">

Si jamais cela ne fonctionne pas, cela peut être du au firewall de la machine hôte qui bloque le port 8080. Sur Linux, on autorise le port 8080, via le firewall ufw : 
* sudo ufw allow 8080/tcp
* sudo ufw enable
* sudo ufw status pour vérifier que la configuration est correcte

<img src="image-22.png" alt="autorisation port 8080 sous linux " width="600px;">

Sur windows, les menus de configuration permettent de configurer le firewall de la machine.

### Visibilité depuis internet

Pour que le site web soit visible depuis Internet, par exemple depuis un smartphone connecté en 4G, il faut rediriger le port 8080 du domicile ou de l'établissement vers le serveur web (port 80 si c'est une machine physique ou port 8080 de la machine physique hébergeant la machine virtuelle serveur web).
Les box ont des possibilités assez limitées pour cela. Sur la LiveBox d'Orange, cela peut se faire dans le menu firewall mais au prix d'une configuration fastidieuse de l'ensemble des ports. L'autre solution nommée par abus DMZ redirige tous les ports vers une même machine (qui doit alors avoir un firewall efficace !).

<img src="image-25.png" alt="visibilité site web" width="600px;">

Une fois cette configuration effectuée, le serveur web est accessible depuis Internet (depuis un msartphone connecté en 4G par exemple). La page web https://ip.lafibre.info/ permet d'obtenir très simplement l'IP publique de sa box.

<img src="image-26.png" alt="configuration serveur web 1" width="600px;">

<img src="image-28.png" alt="configuration serveur web 2" width="600px;">

Avec Wireshark lancé sur la machine hôte et sur la machine virtuelle (invitée),

* on observe la requête vers le port 8080 venant de l'IP publique 92.184.117.87 du routeur du téléphone (en IPv4, les téléphones ont souvent une IP privée cachée par le routeur du fournisseur d'accès) vers la machine hôte. La box a donc bien redirigé les requêtes vers la machine hôte.

* on voit arriver la requête de connexion (SYN) vers le port 80 sur la machine invitée 10.0.2.15 depuis l'adresse IP privée de VirtualBox 192.168.2.2. VirtualBox a donc bien redirigé les requêtes arrivant sur le port 8080 de la machine hôte vers le port 80 de la machine virtuelle.

<img src="image-24.png" alt="configuration serveur web 3" width="600px;">


Remarque, il est possible de demander gratuitement la redirection d'un nom de domaine vers l'IP de la box, même si celle-ci n'est pas fixe. Ce service s'appelle dyndns. Le site https://my.noip.com/ propose notamment ce service gratuitement. Il faut alors remplir les informations du site dans le menu dyndns de la box pour qu'elle envoie régulièrement son IP à noip.com pour qu'il fasse la mise à jour de la sa table DNS.

<img src="image-27.png" alt="configuration serveur web 4" width="600px;">

Pour garder un serveur web chez soi, il est préférable de l'isoler dans une vrai DMZ avec peu d'accès depuis l'extérieur. Le firewall d'un routeur, même grand public moyenne gamme, permettra de ne rediriger que les ports choisis judicieusement et de bloquer l'accès du serveur au réseau de la maison, de sorte d'empêcher un éventuel hackeur d'accéder au reste de la maison si il prenait le contrôle du serveur web.

<img src="image-29.png" alt="configuration serveur web 5" width="600px;">

## Hébergement chez un hébergeur Cloud (PaaS)
Il est possible de louer un serveur chez un fournisseur Cloud. Pour un site web professionnel, la société en charge du site pourra louer une machine virtuelle VPS (Virtual Private Server), sur laquelle elle installera apache ou nginx, PHP, une base de données,...

Plus simplement, pour un site web associatif ou personnel, il est possible de louer un nom de domaine et un hébergement web. L'hébergeur se charge de la maintenance hardware et software (les importantes mises à jour de sécurité notamment). L'isolation entre son site web et les autres sites hébergés sur le même serveur est sans doute moins bonne et l'on est tributaire des choix de versions des logiciels par l'hébergeur.
Il est possible de trouver des hébergeurs gratuits, mais souvent limités et avec des noms de domaines peu attirants (https://www.hostinger.fr). OVHcloud, société française concurrente des géants Amazon Web Service, GoogleCloud et Microsoft Azure, propose une offre gratuite, avec seulement 10 Mo de stockage... OVH propose une solution viable avec PHP, mySQL et https à 2,99 euros par mois et Hostinger à 0.99 euro par mois.
Chez OVH, on peut déposer ses fichiers dans le dossier /www de son site web via un client FTP (Filezilla par exemple) et on peut gérer sa base de données via PhpMyAdmin.

<img src="image-13.png" alt="configuration serveur web 6" width="600px;">

Très souvent les hébergeurs proposent avec leur offre l'utilisation de CMS (Content Management System), logiciels de création de sites web présentés un peu plus loin dans le module.


