# TP6 - Introduction à Javascript (120 mn)

Le fait que PHP soit interprété sur le serveur et génère des pages HTML fait que ce n’est pas très réactif et que la mise à jour se fait par page HTML complète.

Javascript est un langage interprété directement dans le navigateur client, ce qui améliore l’interactivité. De plus, Javascript permet de mettre à jour des éléments de la page indépendamment, ce qui là encore améliore la réactivité de la page.

<img src="image-47.png" alt="Javascript" width="500px;">


Par contre, le code Javascript étant interprété sur le navigateur, il ne peut être confidentiel.
Selon le site https://w3techs.com, javascript est la solution retenue par près de 98% des sites utilisant un langage interprété côté client. Par exemple, un *clic droit > Code source de la page* sur https://framadate.org/create_poll.php montre les script javascript de la page. Sur des sites moins ouverts, les scripts sont parfois peu lisibles.


<img src="image-48.png" alt="Javascript" width="500px;">

Ce TP propose de mettre en œuvre quelques fonctionnalités de javascript, juste pour en comprendre le fonctionnement, sans avoir la prétention de former des développeurs.

Le langage javascript étant exécuté sur le navigateur, il n'est pas nécessaire d'avoir un serveur web pour le tester. Il suffit d'ouvrir le fichier html sur le navigateur du PC.
De même, le langage étant interprété dans le navigateur, c'est l'outil Inspecter du navigateur qui est utilisé pour déboguer le javascript, en l'absence d'environnement de développement dédié. Il s'ouvre depuis la page web par *clic droit > Inspecter*. L'onglet Débogueur possédant les outils habituels points d'arrêt et avance pas à pas :

<img src="image-51.png" alt="Javascript" width="500px;">

Le code javascript doit être écrit entre 2 balises `<script>` et `</script>`, dans un fichier html ou php.

## Affichage de l'heure en javascript

En PHP, l'affichage dynamique de l'heure implique de recharger l'ensemble de la page régulièrement, ce qui génère un clignotement peu agréable et gène l'utilisation des formulaires. En javascript par contre, le code, exécuté sur le navigateur, ne va rafraîchir que la ligne d'affichage de l'heure.

<img src="image-49.png" alt="Javascript" width="500px;">

Le script fait appel à la fonction __Date()__, une des nombreuses fonctions standard de javascript, dont une liste est donnée sur https://www.w3schools.com/js/.
Il est appelé régulièrement par l'événement périodique lancé par la fonction __setInterval(...)__

## Jeu interactif

La création d'un petit jeu permet de mettre en valeur l'intérêt majeur de javascript : la réactivité.
En cela, javascript est très proche de la bibliothèque Tkinter de python. On crée un espace de jeu dont le style est défini dans l'en-tête.

<img src="image-50.png" alt=" jeu interactif avec Javascript" width="500px;">

Dans un script du corps de la page html, on récupère le contexte et on crée les variables nécessaires pour le jeu :

<img src="image-52.png" alt=" jeu interactif avec Javascript" width="500px;">

On crée une fonction pour dessiner la balle et une autre pour dessiner la raquette : 

<img src="image-53.png" alt=" jeu interactif avec Javascript" width="500px;">


On peut alors créer la fonction principale du jeu, appelant les fonctions de dessin de la balle et de la raquette et calculant les nouvelles positions de la balle et de la raquette en fonction des appuis des touches et des rebonds.

<img src="image-54.png" alt=" jeu interactif avec Javascript" width="500px;">

Enfin, on configure les événements permettant de récupérer l'état des touches et de lancer la fonction de jeu toutes les 10 ms.

<img src="image-55.png" alt=" jeu interactif avec Javascript" width="500px;">






