# Routage 3/5: Le routage dynamique - Le protocole RIP

1. Le routage statique
2. Le protocole NAT
3. **Le routage dynamique - Le protocole RIP**
4. Le routage dynamique - Le protocole OSPF
5. Le routage dynamique - Le protocole BGP

####

- **Anthony Jutons**. Nous avons vu dans les cours précédent le routage statique et la traduction d'adresse NAT. Rappelons que le routage consiste à acheminer un paquet d'une machine source à la machine destination. Sur Internet, un ensemble de routeurs, très grand et changeant constamment, est en charge de l'acheminement des paquets. Le routage statique entre tous ces routeurs est alors inenvisageable. Dans ce dernier item, nous verrons donc ce qu'apporte le routage dynamique et comment il fonctionne.

[![Vidéo 3part1 B4-M3-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S4_video3-1_RoutDyn_RIP.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S4_video3-1_RoutDyn_RIP.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S4-script-videos3-5.md" target="_blank">Script vidéos 3 à 5</a>

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S4_3_RoutageDynamique.pdf" target="_blank">Supports de présentation des vidéos 3 à 5</a>

