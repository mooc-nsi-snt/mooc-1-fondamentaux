# Routage 4/5: Le routage dynamique - Le protocole OSPF

1. Le routage statique
2. Le protocole NAT
3. Le routage dynamique - Le protocole RIP
4. **Le routage dynamique - Le protocole OSPF**
5. Le routage dynamique - Le protocole BGP

####

- **Anthony Juton**. Dès 1979, RIP est remplacé par un algorithme de routage par état de liens sur ARPAnet. OSPF, normalisé en 1990, est le descendant de ce protocole. Il continue d'évoluer (la version 3 publiée en 2008 prend en compte ipv6) et est très utilisé pour le routage dynamique internet.

[![Vidéo 3part2 B4-M3-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S4_video3-2_OSPF.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S4_video3-2_OSPF.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S4-script-videos3-5.md" target="_blank">Script vidéos 3 à 5</a>

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S4_3_RoutageDynamique.pdf" target="_blank">Supports de présentation des vidéos 3 à 5</a>
