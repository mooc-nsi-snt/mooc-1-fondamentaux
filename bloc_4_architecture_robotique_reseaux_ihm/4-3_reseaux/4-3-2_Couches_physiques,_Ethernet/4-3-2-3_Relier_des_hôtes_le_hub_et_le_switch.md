# Couches Physiques, Ethernet 3/3: Relier des hôtes : le hub et le switch

1. La couche physique
2. Le protocole Ethernet
3. **Relier des hôtes : le hub et le switch**

####

- Dans cette video, **Geoffrey  Vaquette**. Dans cette vidéo, nous regardons comment relier des hôtes et plus particulièrement deux appareils : le hub et le switch.

[![Vidéo 3 B4-M3-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S2_video3_Hub&Switch.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S2_video3_Hub&Switch.mp4)


#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S2-script-video3.md" target="_blank">Transcription vidéo 3</a>

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S2-video3-diapos.pdf" target="_blank">Supports de présentation de la vidéo 3</a>


