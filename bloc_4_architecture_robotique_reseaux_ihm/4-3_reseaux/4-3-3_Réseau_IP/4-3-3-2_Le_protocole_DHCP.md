# Réseau IP 2/3: Le protocole DHCP

1. Le protocole IPV4
2. **Le protocole DHCP**
3. Le protocole IPV6

####

- **Geoffrey Vaquette**.  Dans cette  vidéo consacrée au protocole DHCP, nous allons voir comment les machines obtiennent de façon automatique une configuration IP. 

[![Vidéo 2 B4-M3-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S3-video_2_DHCP.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S3-video_2_DHCP.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S3-script-video2.md" target="_blank">Transcription vidéo</a>

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S3-videos1-2-diapos.pdf" target="_blank">Supports de présentation des vidéos 1-3</a>


