# Réseau IP 3/3: Le protocole IPV4

1. Le protocole IPV4
2. Le protocole DHCP
3. **Le protocole IPV6** ( 2 vidéos)

####

- **Anthony Juton**. Les deux séquences précédentes ont présenté la couche réseau IPv4, permettant l'identification des machines et l'acheminement des paquets. Les grands principes d'IPv4 sont ceux d'IPv6, nombre d'équipements locaux ayant une durée de vie importante sont en IPv4 et certaines institutions trainent à adopter IPv6 en local. IPv4 garde donc son intérêt, d'autant plus qu'il est plus facile à manipuler en travaux pratiques.
Cependant, peu à peu, IPv4 est remplacé par le protocole IPv6, objet de cette séquence.


[![Vidéo 3part1 B4-M3-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S3_video3-IPv6-p1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S3_video3-IPv6-p1.mp4)


[![Vidéo 3part2 B4-M3-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S3_video3-IPv6-p2.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S3_video3-IPv6-p2.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S3-script-video3part1&2.md" target="_blank">Script vidéos part 1 & 2</a>

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S3_3_IPv6.pdf" target="_blank">Supports de présentation des vidéos</a>

