# Réseau IP 1/3: Le protocole IPV4

1. **Le protocole IPV4** (2 vidéos)
2. Le protocole DHCP
3. Le protocole IPV6

####

 - **Geoffrey Vaquette**.  L'objectif principal de ce chapitre est de comprendre les mécanismes d'échanges de messages entre des hôtes de différents réseaux. Pour cela, nous parlerons dans un premier temps du protocole IPv4 et des adresses associées.

[![Vidéo 1part1 B4-M3-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S3-video1_IPv4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S3-video1_IPv4.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S3-script-video1part1.md" target="_blank">Transcription vidéo 1</a>

----

[![Vidéo 1part2 B4-M3-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S3-video2_IPv4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S3-video2_IPv4.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S3-script-video1part2.md" target="_blank">Transcription vidéo 2</a>

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S3-videos1-2-diapos.pdf" target="_blank">Supports de présentation des vidéos 1-3</a>



