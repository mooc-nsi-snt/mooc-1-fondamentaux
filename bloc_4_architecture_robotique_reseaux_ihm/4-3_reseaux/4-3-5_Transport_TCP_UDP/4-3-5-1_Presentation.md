# 4.3.5 Transport TCP/UDP 1/4:  la couche transport

1. **Présentation de la couche transport**
2. Le protocole UDP
3. Le protocole TCP
4. La gestion des congestions via TCP

######

-  Dans les chapitres précédents, nous avons vu comment le réseau Internet était organisé et comment il permettait l'acheminement d'un paquet IP de sa source à sa destination. Des paquets sont parfois perdus, les paquets correspondant à un même message peuvent arriver dans le désordre et les machines étant multitâche, il est nécessaire d'indiquer de séparer les flux liés à chaque service. C'est pourquoi les communications sur Internet utilisent une couche transport. **Anthony Juton** présente dans cette vidéo le rôle de la couche transport.

[![Vidéo 1 B4-M3-S5 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S5_video1_Intro.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S5_video1_Intro.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S5-script-video1.md" target="_blank">Script vidéo</a>

## Supports de présentation (diapos) 

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S5_1_TCP_UDP.pdf" target="_blank">Supports de présentation des vidéos 1 à 3</a>

