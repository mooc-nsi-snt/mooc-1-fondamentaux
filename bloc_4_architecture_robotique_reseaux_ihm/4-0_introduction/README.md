# Sommaire du Bloc 4 ARS 

- 4.1 Architecture matérielle 
    - 4.1.1 Introduction à l'architecture des systèmes informatiques
    - 4.1.2 Architecture minimale d'un système informatique
    - 4.1.3 Architecture des systèmes embarqués
    - 4.1 4 Architecture des ordinateurs
	- 4.1.5 Conférences

- 4.2 Système d'exploitation
    - 4.2.1 Principe et fonctionnement d'un système d'exploitation
    - 4.2.2 Processus
	- 4.2.3 Processus légers ou threads
    - 4.2.4 Communication par socket
    - 4.2.5 Virtualisation
	
- 4.3 Réseaux
	- 4.3.1 Introduction aux réseaux
	- 4.3.2 Couches physiques, Ethernet
    - 4.3.3 Réseau IP
    - 4.3.4 Routage
    - 4.3.5 Transport TCP/UDP
    - 4.3.6 Application / Service
    - 4.3.7 Sécurité
    - 4.3.8 Autres réseaux

- 4.4 Technologies web
    - 4.4.1 Introduction à HTML 5
	- 4.4.2 Déploiement d'un site web
    - 4.4.3 Interactions et gestion dynamique dans une page web
    - 4.4.4 Frameworks de développement et outils de gestion de contenu
