1
00:00:01,043 --> 00:00:02,300
Dans les deux premières sections,

2
00:00:02,400 --> 00:00:04,279
nous avons introduit le vocabulaire

3
00:00:04,379 --> 00:00:05,879
de la programmation orientée objet

4
00:00:05,979 --> 00:00:07,131
à l'aide du langage Python.

5
00:00:07,994 --> 00:00:10,150
Dans cette troisième section,

6
00:00:10,250 --> 00:00:11,738
nous parlons des concepts clés

7
00:00:11,838 --> 00:00:13,715
qui font ce paradigme.

8
00:00:15,527 --> 00:00:16,658
D'abord, qu'est-ce qu'un objet ?

9
00:00:17,059 --> 00:00:18,499
C'est une structure de données

10
00:00:18,599 --> 00:00:20,405
qui répond à un ensemble de messages.

11
00:00:20,888 --> 00:00:23,393
Cette structure de données définit l'état de l'objet.

12
00:00:23,990 --> 00:00:25,384
L'ensemble des messages,

13
00:00:26,008 --> 00:00:27,031
son comportement.

14
00:00:27,909 --> 00:00:30,188
Les données, on parle aussi de champs,

15
00:00:30,288 --> 00:00:32,474
décrivent donc la structure interne de l'objet

16
00:00:32,574 --> 00:00:33,883
et sont appelées ses attributs.

17
00:00:35,228 --> 00:00:36,164
L'ensemble des messages

18
00:00:36,264 --> 00:00:37,549
forme ce qu'on appelle l'interface.

19
00:00:37,927 --> 00:00:39,191
Et c'est cette interface

20
00:00:39,291 --> 00:00:40,314
qui permet à l'objet

21
00:00:40,414 --> 00:00:42,385
de communiquer avec le monde extérieur.

22
00:00:43,766 --> 00:00:45,488
La réponse à la réception d'un message

23
00:00:45,588 --> 00:00:47,294
par un objet est appelée une méthode.

24
00:00:49,739 --> 00:00:51,677
Voilà un graphique qui résume un objet.

25
00:00:52,151 --> 00:00:55,390
Nous avons à l'extérieur la zone verte

26
00:00:55,490 --> 00:00:56,853
qui est l'interface de l'objet

27
00:00:56,953 --> 00:00:59,518
et qui protège les données privées

28
00:00:59,618 --> 00:01:02,459
que l'objet ne veut pas exposer au monde extérieur.

29
00:01:02,559 --> 00:01:04,318
On y retrouve des méthodes privées

30
00:01:04,618 --> 00:01:05,767
et ses attributs.

31
00:01:06,910 --> 00:01:08,808
L'interface est constituée donc

32
00:01:08,908 --> 00:01:09,924
de méthodes publiques

33
00:01:10,024 --> 00:01:11,576
et de méthodes un peu particulières

34
00:01:11,676 --> 00:01:12,912
que sont les assesseurs

35
00:01:13,012 --> 00:01:13,780
et les mutateurs

36
00:01:14,213 --> 00:01:15,827
qui vont permettre à l'objet

37
00:01:15,927 --> 00:01:18,116
soit d'exposer certaines valeurs

38
00:01:18,216 --> 00:01:19,050
de ses attributs,

39
00:01:19,150 --> 00:01:22,203
ce sont les assesseurs,

40
00:01:22,303 --> 00:01:23,763
soit de changer

41
00:01:23,863 --> 00:01:25,254
la valeur de certains attributs,

42
00:01:25,354 --> 00:01:26,155
ce sont les mutateurs.

43
00:01:28,870 --> 00:01:30,705
Quatre éléments constituent

44
00:01:31,830 --> 00:01:35,044
la clé d'un langage de programmation orienté objet.

45
00:01:35,500 --> 00:01:36,661
Tout d'abord, l'encapsulation.

46
00:01:37,050 --> 00:01:38,951
Nous avons déjà parlé de l'encapsulation

47
00:01:39,051 --> 00:01:39,991
avec le langage Python.

48
00:01:40,283 --> 00:01:41,317
Cette encapsulation

49
00:01:41,417 --> 00:01:42,059
prend deux formes.

50
00:01:42,523 --> 00:01:44,024
D'abord, le fait que l'objet

51
00:01:44,776 --> 00:01:46,939
décrive et contienne à la fois

52
00:01:47,039 --> 00:01:48,598
son état et son comportement.

53
00:01:49,776 --> 00:01:51,066
Ensuite,

54
00:01:51,166 --> 00:01:53,142
que l'objet puisse effectivement

55
00:01:53,673 --> 00:01:55,393
cacher, encapsuler

56
00:01:55,493 --> 00:01:56,623
un certain nombre d'informations

57
00:01:57,015 --> 00:01:59,177
et n'exposer que ce qu'il veut exposer

58
00:01:59,277 --> 00:02:00,911
au travers de son interface.

59
00:02:03,668 --> 00:02:04,630
Le deuxième élément,

60
00:02:04,730 --> 00:02:05,485
c'est l'héritage.

61
00:02:05,926 --> 00:02:07,620
L'héritage, c'est ce qui va permettre

62
00:02:07,720 --> 00:02:08,708
à un objet

63
00:02:11,307 --> 00:02:13,551
de descendre, d'hériter donc

64
00:02:13,651 --> 00:02:14,845
d'un autre objet.

65
00:02:14,945 --> 00:02:16,020
Ici,par exemple,

66
00:02:16,903 --> 00:02:19,211
nos objets voiture et bus

67
00:02:19,311 --> 00:02:20,352
sont tous les deux

68
00:02:20,452 --> 00:02:21,875
des objets de type véhicule.

69
00:02:24,984 --> 00:02:26,223
Le polymorphisme

70
00:02:26,323 --> 00:02:28,283
et la redéfinition de méthodes.

71
00:02:28,383 --> 00:02:29,935
C'est ce qui permet par exemple

72
00:02:30,035 --> 00:02:32,538
à notre cavalier et notre fou

73
00:02:32,638 --> 00:02:34,450
qui sont tous les deux des pièces d'échec

74
00:02:34,950 --> 00:02:36,276
de définir

75
00:02:37,573 --> 00:02:40,236
leur propre méthode move.

76
00:02:40,859 --> 00:02:43,372
Cette méthode va décrire

77
00:02:43,472 --> 00:02:45,931
comment la pièce peut se déplacer sur l'échiquier.

78
00:02:49,630 --> 00:02:52,033
Dernier élément, la programmation générique.

79
00:02:53,109 --> 00:02:54,354
Elle concerne essentiellement

80
00:02:54,454 --> 00:02:56,634
les langages à typage statique

81
00:02:56,734 --> 00:03:01,100
et c'est cet élément qui permet

82
00:03:01,551 --> 00:03:03,439
à un objet pile

83
00:03:03,630 --> 00:03:06,445
de renfermer des objets de natures différentes.

84
00:03:06,545 --> 00:03:08,404
Dans notre exemple, on a à gauche

85
00:03:08,911 --> 00:03:10,593
la pile des appels récursifs

86
00:03:10,846 --> 00:03:11,907
d'une fonction récursive,

87
00:03:12,007 --> 00:03:12,884
et à droite,

88
00:03:12,984 --> 00:03:14,270
une pile de chaînes de caractères

89
00:03:14,370 --> 00:03:15,746
qui permet, par exemple,

90
00:03:15,846 --> 00:03:18,487
de voir qu'une expression parenthésée

91
00:03:18,587 --> 00:03:19,769
est correctement parenthésée.

92
00:03:22,645 --> 00:03:24,187
Voilà, nous avons résumé

93
00:03:24,594 --> 00:03:28,597
les principaux éléments constitutifs

94
00:03:28,697 --> 00:03:30,664
d'un langage de programmation orienté objet.

95
00:03:31,577 --> 00:03:32,997
Pour aller plus loin

96
00:03:33,097 --> 00:03:34,404
et avoir des exemples

97
00:03:34,504 --> 00:03:36,122
dans d'autres langages de programmation,

98
00:03:36,222 --> 00:03:37,652
comme C++ et Java,

99
00:03:37,752 --> 00:03:40,365
nous vous invitons à consulter le document texte

100
00:03:40,465 --> 00:03:41,775
qui accompagne cette vidéo.

