1
00:00:05,203 --> 00:00:08,689
Il est temps d'utiliser nos tableaux et nos dictionnaires

2
00:00:08,789 --> 00:00:10,709
pour manipuler des données en table.

3
00:00:11,365 --> 00:00:13,031
Les données en table sont des données

4
00:00:13,131 --> 00:00:14,537
enregistrées dans des fichiers textes

5
00:00:14,637 --> 00:00:15,460
sous forme de lignes,

6
00:00:15,560 --> 00:00:16,857
on parle d'enregistrements.

7
00:00:16,957 --> 00:00:19,659
Chaque enregistrement possède plusieurs informations

8
00:00:19,759 --> 00:00:20,508
appelées champs

9
00:00:20,608 --> 00:00:23,409
séparées par un délimiteur.

10
00:00:23,509 --> 00:00:25,903
Le délimiteur est souvent un seul caractère

11
00:00:26,003 --> 00:00:28,491
comme la virgule, le point-virgule, la tabulation.

12
00:00:28,591 --> 00:00:30,919
Un champ peut posséder un nom,

13
00:00:31,019 --> 00:00:32,150
on parle de descripteur,

14
00:00:32,250 --> 00:00:33,680
un type et une valeur.

15
00:00:34,125 --> 00:00:36,305
Chaque enregistrement d'un même fichier

16
00:00:36,405 --> 00:00:38,047
partage donc les mêmes descripteurs

17
00:00:38,147 --> 00:00:39,122
dans le même ordre.

18
00:00:39,222 --> 00:00:42,134
Le fait que les données soient au format texte

19
00:00:42,234 --> 00:00:44,633
en font un format privilégié

20
00:00:44,733 --> 00:00:46,438
pour les données ouvertes

21
00:00:46,538 --> 00:00:49,489
puisqu'il n'y a pas besoin d'application particulière

22
00:00:49,589 --> 00:00:51,145
pour lire et manipuler ces données.

23
00:00:51,638 --> 00:00:53,772
Un simple tableur peut suffire

24
00:00:53,872 --> 00:00:55,538
et dans ce MOOC, nous allons voir

25
00:00:55,638 --> 00:00:57,510
comment utiliser le langage Python

26
00:00:57,610 --> 00:00:59,898
pour manipuler ces données.

27
00:01:00,232 --> 00:01:02,066
Grâce au langage Python, nous allons voir

28
00:01:02,166 --> 00:01:05,655
un petit peu ce qu'il y a derrière les fonctions

29
00:01:05,755 --> 00:01:07,506
que nous pouvons trouver dans nos tableurs,

30
00:01:07,606 --> 00:01:09,424
mais également, nous verrons

31
00:01:09,524 --> 00:01:11,162
que sur certaines grosses tables,

32
00:01:11,262 --> 00:01:14,691
il est plus aisé d'utiliser un langage de programmation

33
00:01:14,791 --> 00:01:15,860
pour manipuler les données

34
00:01:15,960 --> 00:01:18,292
que d'ouvrir ce fichier volumineux

35
00:01:18,392 --> 00:01:19,169
dans un tableur.

36
00:01:20,303 --> 00:01:22,171
Je vous propose donc tout de suite

37
00:01:22,271 --> 00:01:23,688
d'ouvrir un interprète interactif

38
00:01:23,788 --> 00:01:25,943
et de commencer à charger des données en table

39
00:01:26,043 --> 00:01:26,871
et à les manipuler.

40
00:01:26,971 --> 00:01:30,294
Je vous propose donc à commencer à charger des données

41
00:01:30,394 --> 00:01:33,298
et à les manipuler ensemble.

42
00:01:33,833 --> 00:01:35,481
Les données que nous allons utiliser

43
00:01:35,581 --> 00:01:38,890
sont des données issues d'un site

44
00:01:38,990 --> 00:01:40,150
qui propose des informations

45
00:01:40,250 --> 00:01:42,804
sur différents pays et différentes villes.

46
00:01:43,268 --> 00:01:45,867
La première table que nous allons manipuler

47
00:01:47,402 --> 00:01:50,300
est constituée de six descripteurs

48
00:01:50,400 --> 00:01:53,391
code, nom, capitale, aire, pop, continent

49
00:01:53,720 --> 00:01:57,911
et d'un peu plus de 250 enregistrements.

50
00:01:58,818 --> 00:02:03,020
Ce sont des informations sur les pays.

51
00:02:05,429 --> 00:02:08,151
La première chose à faire, c'est de

52
00:02:08,788 --> 00:02:11,227
pouvoir lire ce fichier

53
00:02:11,327 --> 00:02:13,179
et récupérer les informations

54
00:02:13,279 --> 00:02:14,365
que nous allons ranger

55
00:02:14,465 --> 00:02:18,132
dans des structures manipulables par Python.

56
00:02:18,629 --> 00:02:22,108
En l'occurrence ici, nous avons écrit une fonction

57
00:02:22,208 --> 00:02:23,537
qui prend trois paramètres,

58
00:02:23,637 --> 00:02:26,875
le nom de notre fichier, qui est une chaîne de caractères,

59
00:02:27,028 --> 00:02:31,274
un délimiteur qui par défaut sera ici le point-virgule

60
00:02:31,374 --> 00:02:34,569
puisque c'est celui qui est utilisé dans notre fichier,

61
00:02:35,639 --> 00:02:37,801
et un encodage de caractères

62
00:02:37,901 --> 00:02:40,331
qui par défaut sera "utf-8".

63
00:02:40,767 --> 00:02:42,767
Nous allons ranger donc nos données

64
00:02:42,867 --> 00:02:43,738
dans un tableau

65
00:02:43,838 --> 00:02:46,590
que nous avons appelé tab.

66
00:02:47,155 --> 00:02:48,835
Nous ouvrons notre fichier.

67
00:02:49,288 --> 00:02:52,801
Nous voyons que dans ce fichier

68
00:02:52,901 --> 00:02:56,486
les descripteurs sont effectivement présents

69
00:02:56,586 --> 00:02:58,337
et donc la première ligne est particulière.

70
00:02:58,437 --> 00:03:02,148
Faisons donc la lecture de la première ligne

71
00:03:02,248 --> 00:03:05,758
grâce à la méthode readline() sur les fichiers Python

72
00:03:07,303 --> 00:03:09,861
Nous utilisons la méthode strip()

73
00:03:09,961 --> 00:03:11,513
qui permet de supprimer

74
00:03:11,613 --> 00:03:15,984
les éventuels blancs, tabulations, retours à la ligne

75
00:03:16,084 --> 00:03:18,174
qui pourraient exister en début et en fin de ligne,

76
00:03:18,274 --> 00:03:20,757
donc notamment ici, le strip va supprimer

77
00:03:20,857 --> 00:03:23,192
le retour à la ligne de chacune de nos lignes.

78
00:03:24,490 --> 00:03:27,557
Et enfin, nous découpons par la méthode split()

79
00:03:28,305 --> 00:03:30,586
à laquelle on passe le délimiteur

80
00:03:31,170 --> 00:03:33,257
nous allons découper notre chaîne de caractères

81
00:03:33,357 --> 00:03:35,244
en plusieurs chaînes

82
00:03:35,344 --> 00:03:39,659
et donc récupérer ainsi les valeurs.

83
00:03:42,564 --> 00:03:44,025
Dans cette première ligne particulière,

84
00:03:44,125 --> 00:03:46,534
les valeurs ne sont donc pas des valeurs de champs

85
00:03:46,634 --> 00:03:49,655
mais bien les noms des descripteurs

86
00:03:50,327 --> 00:03:55,312
que nous allons ranger dans une liste appelée cles.

87
00:03:56,922 --> 00:03:59,750
Et ensuite nous allons pouvoir parcourir effectivement

88
00:03:59,850 --> 00:04:02,567
les enregistrements de notre fichier,

89
00:04:02,667 --> 00:04:03,612
ligne par ligne,

90
00:04:04,123 --> 00:04:07,815
appliquer les mêmes techniques pour découper notre ligne

91
00:04:07,915 --> 00:04:09,595
et récupérer les champs

92
00:04:09,695 --> 00:04:13,144
que nous allons ranger dans un tableau appelé data.

93
00:04:13,754 --> 00:04:16,874
Ensuite, par la méthode dict() que nous avons déjà vue

94
00:04:16,974 --> 00:04:20,648
et la fonction zip(), nous allons créer

95
00:04:20,748 --> 00:04:23,622
un dictionnaire pour chacun de ces enregistrements.

96
00:04:25,332 --> 00:04:32,534
Voyons cette fonction de lecture à l’œuvre.

97
00:04:32,634 --> 00:04:35,855
Nous allons donc lire notre fichier de pays

98
00:04:35,955 --> 00:04:37,892
et les ranger donc dans un tableau

99
00:04:37,992 --> 00:04:40,132
que nous avons nommé L_PAYS

100
00:04:40,232 --> 00:04:42,301
L pour liste puisque nous sommes dans Python.

101
00:04:42,871 --> 00:04:44,899
Et nous pouvons regarder qu'effectivement

102
00:04:44,999 --> 00:04:47,769
un des enregistrements est bien un dictionnaire

103
00:04:47,869 --> 00:04:50,084
avec ses descripteurs

104
00:04:50,184 --> 00:04:52,014
et les valeurs associées.

105
00:04:53,704 --> 00:04:56,919
Il existe un module qui s'appelle csv

106
00:04:58,357 --> 00:05:02,042
donc vous avez ici une version de la fonction lecture

107
00:05:02,142 --> 00:05:04,784
qui réalise exactement la même tâche

108
00:05:04,884 --> 00:05:07,010
que la version précédente

109
00:05:07,110 --> 00:05:11,301
mais en utilisant une méthode du module csv

110
00:05:11,401 --> 00:05:12,970
qui s'appelle DictReader()

111
00:05:13,070 --> 00:05:16,483
qui permet de réaliser exactement la même chose,

112
00:05:16,583 --> 00:05:18,159
c'est-à-dire de construire les dictionnaires

113
00:05:18,259 --> 00:05:21,360
en prenant comme descripteurs

114
00:05:21,460 --> 00:05:23,252
les informations de la première ligne.

115
00:05:24,031 --> 00:05:25,678
Et donc cette fonction-là 

116
00:05:25,778 --> 00:05:31,669
réalise exactement la même opération.

117
00:05:31,769 --> 00:05:34,678
Nous allons tester cette fonction

118
00:05:34,778 --> 00:05:39,325
sur, cette fois, notre fichier de villes

119
00:05:39,425 --> 00:05:40,910
qui contient un peu plus d'enregistrements

120
00:05:41,010 --> 00:05:44,022
puisque nous avons presque 197000 villes

121
00:05:44,122 --> 00:05:45,370
dans le fichier.

122
00:05:46,193 --> 00:05:49,645
Voilà pour la lecture des données

123
00:05:50,096 --> 00:05:54,887
soit directement, sans utiliser de module particulier,

124
00:05:54,987 --> 00:05:56,623
soit en utilisant le module csv

125
00:05:56,723 --> 00:05:58,847
qui offre la fonction DictReader()

126
00:05:58,947 --> 00:05:59,561
qui est bien pratique

127
00:05:59,661 --> 00:06:03,643
lorsque notre fichier de données,

128
00:06:03,743 --> 00:06:04,419
notre table,

129
00:06:05,133 --> 00:06:06,801
possède effectivement une première ligne

130
00:06:06,901 --> 00:06:07,826
avec les descripteurs.

131
00:06:09,958 --> 00:06:11,247
Je vous retrouve dans une prochaine vidéo

132
00:06:11,347 --> 00:06:13,659
pour voir les manipulations sur ces données.

