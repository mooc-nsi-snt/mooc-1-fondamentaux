1
00:00:01,767 --> 00:00:03,408
L'exemple que nous allons prendre

2
00:00:03,508 --> 00:00:04,630
est un exemple,

3
00:00:04,730 --> 00:00:06,589
vous allez me dire, encore celui-là,

4
00:00:07,545 --> 00:00:10,190
mais c'est le plus simple que je connaisse,

5
00:00:10,507 --> 00:00:12,475
et celui qui est suffisamment riche

6
00:00:12,575 --> 00:00:14,503
pour parler de schéma algorithmique.

7
00:00:14,827 --> 00:00:17,182
C'est pour ça que ça m'intéresse comme exemple,

8
00:00:17,282 --> 00:00:18,885
c'est le calcul de la puissance.

9
00:00:20,099 --> 00:00:22,399
Calculer x puissance n par exemple

10
00:00:22,682 --> 00:00:25,311
lorsqu'on a l'opérateur multiplier à sa disposition.

11
00:00:27,424 --> 00:00:28,363
Alors on le voit bien,

12
00:00:28,463 --> 00:00:29,868
ça, c'est facile à calculer,

13
00:00:29,968 --> 00:00:31,849
c'est x fois x fois x fois x

14
00:00:31,949 --> 00:00:32,875
le tout n fois.

15
00:00:33,692 --> 00:00:36,194
Et ce qui va nous intéresser,

16
00:00:36,294 --> 00:00:37,303
c'est de regarder

17
00:00:37,403 --> 00:00:38,898
comment je peux calculer ça.

18
00:00:39,546 --> 00:00:40,631
Donc je me retrouve

19
00:00:41,353 --> 00:00:42,706
à faire effectivement

20
00:00:42,806 --> 00:00:46,716
x fois x fois x n fois

21
00:00:48,611 --> 00:00:50,756
et donc, la question qui va être intéressante,

22
00:00:50,856 --> 00:00:51,601
c'est comment

23
00:00:51,942 --> 00:00:54,018
est-ce que je peux réaliser cette opération.

24
00:00:54,385 --> 00:00:55,870
Alors, c'est assez facile,

25
00:00:56,887 --> 00:00:58,441
vous avez tous l'idée,

26
00:00:58,541 --> 00:00:59,736
vous avez tous fait de la programmation,

27
00:01:00,465 --> 00:01:01,984
et donc c'est assez facile de se dire,

28
00:01:02,435 --> 00:01:04,554
c'est simple, si je veux faire

29
00:01:05,478 --> 00:01:08,022
les n opérations,

30
00:01:08,122 --> 00:01:10,166
il suffit que j'accumule le résultat

31
00:01:10,266 --> 00:01:11,662
et que je construise

32
00:01:12,033 --> 00:01:15,323
mon algorithme avec une accumulation

33
00:01:15,630 --> 00:01:17,462
dans une variable pour calculer.

34
00:01:17,788 --> 00:01:19,403
Ça, c'est facile.

35
00:01:19,503 --> 00:01:20,982
L'algorithme le plus simple

36
00:01:21,082 --> 00:01:22,088
que l'on puisse imaginer,

37
00:01:22,390 --> 00:01:24,234
c'est faire un for.

38
00:01:24,920 --> 00:01:26,944
Pour i égale 1 à n,

39
00:01:27,256 --> 00:01:29,182
je vais avoir une variable p

40
00:01:29,282 --> 00:01:30,021
dans laquelle,

41
00:01:32,427 --> 00:01:34,015
la variable p qui est ici,

42
00:01:35,277 --> 00:01:36,675
dans laquelle je vais mettre

43
00:01:37,504 --> 00:01:39,635
l'accumulation du résultat.

44
00:01:39,943 --> 00:01:42,060
Et puis je vais avoir mon itération

45
00:01:42,160 --> 00:01:43,075
qui se retrouve ici,

46
00:01:44,877 --> 00:01:46,239
qui va me permettre de dire

47
00:01:46,339 --> 00:01:47,303
pour i égale 1 à n,

48
00:01:47,594 --> 00:01:49,569
je fais p égale p fois x.

49
00:01:49,669 --> 00:01:51,351
Donc ça, c'est assez clair,

50
00:01:51,451 --> 00:01:52,559
c'est assez facile à faire,

51
00:01:52,915 --> 00:01:53,816
on comprend bien.

52
00:01:53,916 --> 00:01:55,269
Alors, ce n'est pas la seule façon

53
00:01:55,369 --> 00:01:56,465
d'exprimer cet algorithme,

54
00:01:56,565 --> 00:01:57,882
cet algorithme, on aurait pu aussi

55
00:01:57,982 --> 00:01:59,880
l'exprimer avec des conditions tant que,

56
00:01:59,980 --> 00:02:03,344
avec un certain nombre d'autres opérateurs.

57
00:02:03,674 --> 00:02:05,803
Je vous mets la deuxième version

58
00:02:05,903 --> 00:02:06,808
avec un while.

59
00:02:08,257 --> 00:02:11,375
Dans ce cas-là, je vais avoir besoin d'un indice

60
00:02:11,475 --> 00:02:12,667
qui me permet de dire

61
00:02:12,767 --> 00:02:13,823
quelle est la quantité

62
00:02:14,369 --> 00:02:16,353
qui a été accumulée dans la variable p.

63
00:02:16,670 --> 00:02:19,672
Donc cet indice, c'est l'indice i qui est ici

64
00:02:19,984 --> 00:02:21,691
et qui me sert de condition d'arrêt.

65
00:02:21,969 --> 00:02:24,032
Tant que i est inférieur ou égal à n

66
00:02:24,132 --> 00:02:25,505
je fais p égale p fois x.

67
00:02:26,155 --> 00:02:29,466
J'initialise évidemment par 1,

68
00:02:29,566 --> 00:02:31,565
qui est l'élément neutre de l'opérateur *.

69
00:02:33,987 --> 00:02:35,791
Donc ces deux versions,

70
00:02:35,891 --> 00:02:36,853
c'est la même version

71
00:02:36,953 --> 00:02:38,678
d'un algorithme itératif,

72
00:02:38,924 --> 00:02:41,751
qui peut s'exprimer encore d'une troisième façon

73
00:02:41,851 --> 00:02:42,831
de manière itérative

74
00:02:42,931 --> 00:02:45,392
mais cette fois-ci, sur un schéma récursif

75
00:02:46,128 --> 00:02:47,870
dans lequel on va dire

76
00:02:47,970 --> 00:02:49,810
pour calculer x puissance n,

77
00:02:49,910 --> 00:02:51,803
je peux calculer x puissance n - 1

78
00:02:52,040 --> 00:02:53,409
et le multiplier par x

79
00:02:53,509 --> 00:02:54,749
pour me donner x puissance n.

80
00:02:55,015 --> 00:02:57,043
Donc ici, la relation, c'est

81
00:02:57,143 --> 00:02:59,448
x puissance n égale

82
00:02:59,922 --> 00:03:01,895
x puissance n - 1

83
00:03:02,256 --> 00:03:03,803
fois x.

84
00:03:03,903 --> 00:03:05,210
Donc je me retrouve ici

85
00:03:05,310 --> 00:03:06,772
avec un truc qui est assez sympa

86
00:03:07,648 --> 00:03:09,680
sauf que je n'ai plus ma variable d'accumulation,

87
00:03:09,960 --> 00:03:11,956
donc là, j'ai fait disparaître le p,

88
00:03:12,240 --> 00:03:14,246
sauf que le p, en fait,

89
00:03:14,346 --> 00:03:16,048
il est caché dans le schéma récursif,

90
00:03:16,553 --> 00:03:17,995
c'est-à-dire que l'accumulation

91
00:03:18,095 --> 00:03:19,686
se fait grâce

92
00:03:19,977 --> 00:03:21,803
à la pile des appels récursifs.

93
00:03:22,451 --> 00:03:25,035
Donc ça, c'est un algorithme simple,

94
00:03:25,135 --> 00:03:26,292
vous en avez plein d'autres,

95
00:03:26,622 --> 00:03:29,823
des itérations que vous pouvez utiliser avec vos élèves,

96
00:03:30,759 --> 00:03:32,355
qu'il s'agisse de faire des sommes,

97
00:03:32,455 --> 00:03:34,141
qu'il s'agisse de faire ce genre de choses,

98
00:03:34,403 --> 00:03:37,972
vous avez ce genre de choses qui apparaît.

99
00:03:38,242 --> 00:03:39,739
Donc ce schéma, c'est sans doute

100
00:03:39,839 --> 00:03:41,519
le schéma le plus simple d'une itération.

101
00:03:42,238 --> 00:03:44,308
Ici, le nombre d'itérations que vous faites

102
00:03:44,408 --> 00:03:45,100
est fixé.

103
00:03:45,366 --> 00:03:47,048
Alors maintenant, je vais me replacer

104
00:03:47,148 --> 00:03:48,509
dans un contexte un peu plus général

105
00:03:48,609 --> 00:03:49,745
et voir comment

106
00:03:49,845 --> 00:03:51,132
qu'est-ce que je peux dire

107
00:03:51,232 --> 00:03:52,302
sur ce type d'algorithme.

108
00:03:53,010 --> 00:03:54,772
Remettons-nous dans le cadre général.

109
00:03:55,496 --> 00:03:57,615
À la base, on avait vu qu'il y avait un problème

110
00:03:58,242 --> 00:03:59,333
avec les données,

111
00:03:59,433 --> 00:04:00,682
donc c'est un problème de traitement de données.

112
00:04:00,962 --> 00:04:02,185
Derrière, je vais

113
00:04:02,285 --> 00:04:04,022
utiliser des algorithmes

114
00:04:04,299 --> 00:04:06,313
couplés à des structures de données évidemment

115
00:04:06,977 --> 00:04:09,105
que je vais transformer après en programme.

116
00:04:09,205 --> 00:04:10,849
Je fais une transposition dans un programme

117
00:04:10,949 --> 00:04:11,656
avec les données

118
00:04:11,756 --> 00:04:13,608
que je vais pouvoir exécuter sur une machine.

119
00:04:13,880 --> 00:04:15,823
Et à la fin, j'obtiens mon résultat.

120
00:04:15,923 --> 00:04:18,907
Ça, c'est ce qu'on a vu à la séquence précédente.

121
00:04:19,772 --> 00:04:21,279
Maintenant, la question qui va se poser,

122
00:04:21,379 --> 00:04:22,085
c'est

123
00:04:22,316 --> 00:04:23,906
quand je vais faire un calcul,

124
00:04:24,006 --> 00:04:25,610
je vais utiliser une machine

125
00:04:26,197 --> 00:04:27,323
et quand j'ai le terme

126
00:04:27,423 --> 00:04:28,759
utilisation d'une machine,

127
00:04:28,859 --> 00:04:29,776
ça sous-entend

128
00:04:29,876 --> 00:04:31,108
que je vais consommer

129
00:04:31,364 --> 00:04:32,487
de la ressource

130
00:04:32,587 --> 00:04:33,428
sur la machine.

131
00:04:33,907 --> 00:04:35,947
Cette consommation de ressources,

132
00:04:36,277 --> 00:04:37,518
on peut la voir comme ça,

133
00:04:37,618 --> 00:04:39,144
ça peut être du temps de calcul,

134
00:04:39,244 --> 00:04:40,610
ça peut être de l'espace mémoire,

135
00:04:40,710 --> 00:04:41,770
ça peut être du réseau

136
00:04:41,870 --> 00:04:43,616
si vous avez des communications,

137
00:04:43,716 --> 00:04:45,288
et donc en gros, j'utilise

138
00:04:45,598 --> 00:04:47,347
de la ressource physique,

139
00:04:48,252 --> 00:04:49,849
j'utilise de l'énergie.

140
00:04:50,300 --> 00:04:51,642
Si on était un physicien,

141
00:04:51,935 --> 00:04:53,374
on pourrait se poser la question :

142
00:04:53,683 --> 00:04:55,814
voilà, j'ai envie de

143
00:04:56,248 --> 00:04:58,648
faire monter un poids de l'altitude 0

144
00:04:59,107 --> 00:05:00,800
à l'altitude 1000,

145
00:05:01,124 --> 00:05:02,846
quelle est la quantité d'énergie

146
00:05:02,946 --> 00:05:04,701
ou de travail qu'il faut que je fournisse

147
00:05:05,057 --> 00:05:06,354
pour arriver à amener le poids

148
00:05:06,454 --> 00:05:07,645
de 1 kilo par exemple

149
00:05:07,745 --> 00:05:08,668
à l'altitude 1000 ?

150
00:05:09,008 --> 00:05:10,951
C'est exactement la même chose ici,

151
00:05:11,051 --> 00:05:13,077
c'est-à-dire que les ressources,

152
00:05:13,177 --> 00:05:15,150
c'est ce qui va être à votre disposition

153
00:05:15,250 --> 00:05:16,390
pour réaliser un calcul.

154
00:05:17,030 --> 00:05:19,764
Alors, dans l'informatique ultra-moderne,

155
00:05:20,949 --> 00:05:22,351
il y a des liens très forts

156
00:05:22,451 --> 00:05:23,632
entre la notion d'énergie

157
00:05:23,909 --> 00:05:25,165
et la notion de calcul.

158
00:05:25,500 --> 00:05:27,752
On pourra retrouver ça dans les ouvertures

159
00:05:28,518 --> 00:05:29,638
à la fin du cours mais

160
00:05:29,882 --> 00:05:31,399
c'est déjà beaucoup plus avancé,

161
00:05:31,499 --> 00:05:32,712
le lien entre physique,

162
00:05:32,812 --> 00:05:33,566
énergie

163
00:05:33,666 --> 00:05:34,745
et informatique.

164
00:05:34,845 --> 00:05:35,911
Néanmoins, on a des ressources

165
00:05:36,011 --> 00:05:37,017
et c'est ça qui est important.

166
00:05:37,654 --> 00:05:39,052
Et la question qui se pose,

167
00:05:39,152 --> 00:05:41,058
c'est est-ce que

168
00:05:41,316 --> 00:05:43,081
je vais avoir suffisamment de ressources

169
00:05:43,181 --> 00:05:44,282
pour réaliser mon calcul.

170
00:05:45,210 --> 00:05:47,131
Donc ça, c'est une question importante.

171
00:05:48,103 --> 00:05:50,001
Est-ce que ça va me prendre beaucoup d'énergie ?

172
00:05:50,101 --> 00:05:51,150
Pas beaucoup d'énergie ?

173
00:05:51,250 --> 00:05:52,171
De l'espace mémoire ?

174
00:05:52,271 --> 00:05:53,160
Pas d'espace mémoire ?

175
00:05:53,544 --> 00:05:55,170
Et on sera confronté

176
00:05:55,773 --> 00:05:56,888
à cette question

177
00:05:56,988 --> 00:05:58,270
dans toute l'informatique,

178
00:05:58,370 --> 00:05:59,651
dans tout ce qu'on va faire en informatique.

179
00:06:00,786 --> 00:06:02,321
Alors, pour faire ça,

180
00:06:02,421 --> 00:06:03,605
qu'est-ce que j'ai à ma disposition ?

181
00:06:03,886 --> 00:06:04,896
J'ai le problème,

182
00:06:06,880 --> 00:06:09,046
je commence à avoir des schémas algorithmiques

183
00:06:09,146 --> 00:06:10,762
donc je commence à avoir des algorithmes,

184
00:06:12,456 --> 00:06:13,505
j'ai des données,

185
00:06:13,917 --> 00:06:15,646
et donc, ce que je vais essayer de faire,

186
00:06:15,746 --> 00:06:17,414
c'est de voir quel est le lien entre les données

187
00:06:17,514 --> 00:06:18,319
qui sont là

188
00:06:19,070 --> 00:06:21,230
et les ressources qui sont utilisées,

189
00:06:21,936 --> 00:06:23,177
qui seront utilisées.

190
00:06:23,277 --> 00:06:24,675
Alors attention, ça va traverser

191
00:06:24,775 --> 00:06:25,536
plein, plein de choses.

192
00:06:26,569 --> 00:06:27,669
Ça va traverser

193
00:06:28,016 --> 00:06:29,842
le programme, les données, et cætera,

194
00:06:29,942 --> 00:06:31,295
ça va traverser plein, plein de choses.

195
00:06:31,619 --> 00:06:33,984
Donc ici, en fait, il y a plein, plein d'étapes,

196
00:06:35,026 --> 00:06:35,818
ici,

197
00:06:36,839 --> 00:06:38,408
puisque vous allez avoir

198
00:06:38,508 --> 00:06:39,936
à regarder la façon

199
00:06:40,689 --> 00:06:43,186
dont vous écrivez le programme,

200
00:06:43,490 --> 00:06:45,119
les langages de programmation, et cætera.

201
00:06:45,370 --> 00:06:46,920
Donc il y a plein de paramètres qui vont jouer

202
00:06:47,294 --> 00:06:48,736
pour faire le lien entre une donnée

203
00:06:49,395 --> 00:06:50,829
et le temps d'exécution,

204
00:06:50,929 --> 00:06:52,501
le temps de traitement de cette donnée.

205
00:06:53,189 --> 00:06:54,844
Alors, pour faire simple,

206
00:06:55,160 --> 00:06:56,180
en fait, on va essayer de

207
00:06:56,921 --> 00:06:58,082
regarder

208
00:06:58,659 --> 00:07:00,117
les choses, je dirais,

209
00:07:00,615 --> 00:07:01,768
ici, par exemple

210
00:07:01,868 --> 00:07:03,447
pour le calcul de la puissance,

211
00:07:03,743 --> 00:07:04,159
on va se dire

212
00:07:04,259 --> 00:07:05,862
je vais juste regarder n.

213
00:07:06,559 --> 00:07:08,859
Ce n'est pas la peine de connaître vraiment x,

214
00:07:08,959 --> 00:07:10,297
je vais juste regarder n.

215
00:07:10,397 --> 00:07:12,313
Mais si vous travaillez avec des grands entiers,

216
00:07:12,400 --> 00:07:13,105
il est évident

217
00:07:13,205 --> 00:07:14,794
que ça risque de vous poser des problèmes.

218
00:07:17,348 --> 00:07:19,466
Donc on va essayer

219
00:07:19,566 --> 00:07:21,532
d'intuiter et de prédire

220
00:07:21,632 --> 00:07:22,614
d'une certaine manière

221
00:07:22,714 --> 00:07:24,916
comment l'algorithme

222
00:07:25,016 --> 00:07:26,125
va utiliser les ressources

223
00:07:26,225 --> 00:07:28,227
après sa transposition, son écriture, et cætera.

224
00:07:28,567 --> 00:07:31,290
Donc ça, c'est l'étape importante.

225
00:07:32,042 --> 00:07:33,607
Est-ce que je peux construire

226
00:07:33,707 --> 00:07:34,863
un prédicteur de coût

227
00:07:35,140 --> 00:07:38,637
qui, à partir d'un algorithme A

228
00:07:38,737 --> 00:07:39,649
et d'une donnée d,

229
00:07:39,959 --> 00:07:40,812
me permet de dire

230
00:07:40,912 --> 00:07:42,031
la quantité de ressources

231
00:07:42,420 --> 00:07:44,982
qui sera utilisée par l'exécution de l'algorithme A ?

232
00:07:45,658 --> 00:07:46,908
Alors, évidemment,

233
00:07:47,511 --> 00:07:48,834
ça comprendrait

234
00:07:48,934 --> 00:07:50,433
l'exécution de l'algorithme

235
00:07:50,533 --> 00:07:52,746
qui a été programmé dans un langage de programmation,

236
00:07:53,187 --> 00:07:55,111
qui a été rendu exécutable

237
00:07:55,211 --> 00:07:56,498
sur une machine donnée,

238
00:07:56,800 --> 00:07:58,348
avec des ressources fixées,

239
00:07:58,652 --> 00:08:00,683
et donc, il y a tout ça qui est derrière.

240
00:08:00,783 --> 00:08:02,434
Donc j'essaie de construire un prédicteur

241
00:08:04,752 --> 00:08:06,083
du temps d'exécution,

242
00:08:06,183 --> 00:08:08,036
du temps d'utilisation de la mémoire, et cætera.

243
00:08:08,410 --> 00:08:09,955
Évidemment, ça va être à la louche.

244
00:08:10,252 --> 00:08:11,396
On se rend bien compte

245
00:08:11,496 --> 00:08:12,407
que l'on n'est pas capable

246
00:08:12,507 --> 00:08:13,958
d'appréhender toute la chaîne

247
00:08:14,058 --> 00:08:14,740
d'un seul coup,

248
00:08:14,994 --> 00:08:16,283
mais l'idée, c'est d'avoir

249
00:08:17,152 --> 00:08:18,680
un ordre de grandeur,

250
00:08:18,780 --> 00:08:20,339
une façon dont ça va évoluer.

251
00:08:20,439 --> 00:08:21,832
Si vous avez par exemple

252
00:08:21,932 --> 00:08:23,968
un problème de voyageur de commerce,

253
00:08:24,068 --> 00:08:24,782
et vous voulez avoir,

254
00:08:26,885 --> 00:08:29,061
un chemin qui passe par toutes les villes

255
00:08:29,161 --> 00:08:30,062
une et une seule fois,

256
00:08:30,162 --> 00:08:31,785
qui soit le plus court possible,

257
00:08:31,885 --> 00:08:32,685
vous voyez bien que

258
00:08:32,785 --> 00:08:34,283
si vous avez un problème avec 10 villes,

259
00:08:34,383 --> 00:08:35,452
si je passe à 15 villes,

260
00:08:35,552 --> 00:08:38,432
est-ce que je vais pouvoir y arriver.

261
00:08:39,222 --> 00:08:41,801
C'est plus comment ça va varier

262
00:08:41,901 --> 00:08:43,057
en fonction de la taille des données.

