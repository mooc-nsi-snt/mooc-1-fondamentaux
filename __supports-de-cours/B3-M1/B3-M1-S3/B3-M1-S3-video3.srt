1
00:00:01,045 --> 00:00:02,935
Si je fais maintenant un petit catalogue

2
00:00:03,035 --> 00:00:04,792
de tous ces algorithmes de tri.

3
00:00:05,578 --> 00:00:07,162
Alors, ces algorithmes de tri,

4
00:00:07,262 --> 00:00:08,317
j'ai essayé de les regrouper

5
00:00:08,417 --> 00:00:09,494
en différents schémas,

6
00:00:11,263 --> 00:00:14,316
que l'on peut retrouver dans bon nombre d'algos.

7
00:00:14,625 --> 00:00:15,742
Le premier schéma,

8
00:00:15,842 --> 00:00:16,979
c'est un algorithme d'extraction.

9
00:00:17,079 --> 00:00:18,109
On va extraire

10
00:00:18,473 --> 00:00:20,003
le plus grand ou le plus petit

11
00:00:20,469 --> 00:00:21,722
de façon itérative

12
00:00:21,822 --> 00:00:23,708
pour retrouver effectivement

13
00:00:23,808 --> 00:00:24,893
quelque chose qui est trié.

14
00:00:25,308 --> 00:00:27,330
Ça, c'est un schéma de base

15
00:00:27,621 --> 00:00:28,621
qu'il faut connaître.

16
00:00:29,427 --> 00:00:31,848
Ensuite, on peut travailler

17
00:00:31,948 --> 00:00:33,116
non pas par extraction

18
00:00:33,216 --> 00:00:34,103
mais par accrétion,

19
00:00:34,203 --> 00:00:35,592
c'est-à-dire on va rajouter

20
00:00:35,913 --> 00:00:37,047
au fur et à mesure des éléments.

21
00:00:37,352 --> 00:00:38,340
Ça typiquement,

22
00:00:38,440 --> 00:00:39,542
ça va être le tri par insertion,

23
00:00:39,642 --> 00:00:40,674
le tri de Shell, et cætera,

24
00:00:41,091 --> 00:00:42,439
et dans lesquels effectivement

25
00:00:42,539 --> 00:00:43,718
on va avoir un ensemble

26
00:00:43,818 --> 00:00:44,760
que l'on va augmenter

27
00:00:44,860 --> 00:00:46,122
et que l'on va maintenir trié,

28
00:00:46,222 --> 00:00:47,368
on maintient la structure.

29
00:00:48,964 --> 00:00:55,739
Ensuite, on peut avoir d'autres formes de tri

30
00:00:55,839 --> 00:00:58,010
sur lesquelles on ne va pas trop insister dans ce chapitre

31
00:00:58,110 --> 00:00:59,879
mais qui sont tout à fait au programme

32
00:00:59,979 --> 00:01:02,344
et utilisables en terminale,

33
00:01:02,649 --> 00:01:04,595
les opérations qui sont locales

34
00:01:04,695 --> 00:01:05,642
c'est-à-dire que

35
00:01:05,869 --> 00:01:07,139
dans l'algorithme de tri,

36
00:01:07,239 --> 00:01:09,323
vous n'autorisez que quelques échanges

37
00:01:09,423 --> 00:01:10,874
au niveau de votre tableau

38
00:01:11,292 --> 00:01:12,680
entre particulier entre voisins,

39
00:01:12,780 --> 00:01:16,168
c'est ce qui va conduire au bubble sort et au tri cocktail,

40
00:01:16,498 --> 00:01:19,051
et puis vous avez d'autres façons de faire

41
00:01:19,151 --> 00:01:20,404
qui reposent sur des arguments,

42
00:01:20,504 --> 00:01:21,975
par exemple des arguments aléatoires,

43
00:01:22,386 --> 00:01:23,999
le tri par seau,

44
00:01:24,295 --> 00:01:26,951
qui suppose que vos données sont plus ou moins réparties

45
00:01:27,317 --> 00:01:29,814
de façon aléatoire sur un intervalle

46
00:01:29,914 --> 00:01:32,740
et vous allez faire une distribution dans différents seaux

47
00:01:32,840 --> 00:01:34,013
pour pouvoir trier les choses.

48
00:01:34,290 --> 00:01:36,663
Là aussi, ça se rapproche également

49
00:01:37,159 --> 00:01:39,539
de tout ce qu'on appelle

50
00:01:42,650 --> 00:01:45,530
les tris rapides

51
00:01:45,630 --> 00:01:46,656
par seau

52
00:01:46,756 --> 00:01:49,754
et dont l'analyse en moyenne est excellente.

53
00:01:50,435 --> 00:01:52,130
Ensuite on va avoir des tris

54
00:01:52,230 --> 00:01:54,193
qui reposeront sur d'autres principes,

55
00:01:54,293 --> 00:01:55,262
le diviser pour régner,

56
00:01:55,666 --> 00:01:58,190
alors là, j'ai souligné deux tris

57
00:01:58,290 --> 00:01:59,681
qu'il faut impérativement connaître,

58
00:02:00,059 --> 00:02:02,927
le tri fusion ou partition fusion,

59
00:02:03,027 --> 00:02:04,034
merge sort en anglais,

60
00:02:04,339 --> 00:02:05,459
qui repose sur

61
00:02:05,559 --> 00:02:07,202
j'ai à trier mes données

62
00:02:07,302 --> 00:02:09,847
donc je vais trier un paquet de gauche, un paquet de droite

63
00:02:09,947 --> 00:02:11,972
et je vais fusionner les résulats,

64
00:02:12,288 --> 00:02:14,125
et le tri par segmentation,

65
00:02:14,527 --> 00:02:16,118
appelé aussi quick sort,

66
00:02:17,001 --> 00:02:18,603
qui revient à dire

67
00:02:18,703 --> 00:02:21,854
je vais essayer de diviser mon paquet en deux parties,

68
00:02:22,125 --> 00:02:23,735
les plus grands et les plus petits,

69
00:02:23,835 --> 00:02:26,006
et à partir de là, je trie récursivement

70
00:02:26,436 --> 00:02:27,503
mes deux parties.

71
00:02:27,948 --> 00:02:30,571
Et puis vous avez encore différents types de tri

72
00:02:30,671 --> 00:02:32,598
dont les arguments sont différents,

73
00:02:32,698 --> 00:02:34,069
par exemple le tri par comptage,

74
00:02:34,169 --> 00:02:35,312
le tri spaghetti

75
00:02:35,412 --> 00:02:37,525
que j'aurais pu mettre aussi aux tris par extraction.

