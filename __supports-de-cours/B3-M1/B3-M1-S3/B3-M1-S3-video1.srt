1
00:00:01,111 --> 00:00:03,539
Dans cette séquence de vidéos,

2
00:00:03,639 --> 00:00:05,123
nous allons aborder un problème

3
00:00:05,223 --> 00:00:06,518
qui est un problème classique

4
00:00:06,618 --> 00:00:08,155
qu'on appelle le problème du tri.

5
00:00:08,421 --> 00:00:09,609
Alors le problème du tri,

6
00:00:09,709 --> 00:00:11,741
c'est tout simplement ré-organiser des données

7
00:00:12,799 --> 00:00:14,666
de façon à pouvoir être plus efficace

8
00:00:14,766 --> 00:00:15,986
pour les traiter par la suite.

9
00:00:17,945 --> 00:00:19,887
Pourquoi on utilise le problème du tri ?

10
00:00:19,987 --> 00:00:20,949
Tout simplement

11
00:00:21,049 --> 00:00:23,347
parce que c'est un problème qui est important,

12
00:00:23,447 --> 00:00:24,693
j'ai envie de dire, mais pas seulement,

13
00:00:24,959 --> 00:00:27,555
l'idée de cette séquence,

14
00:00:27,655 --> 00:00:28,744
c'est de vous montrer une méthode

15
00:00:28,844 --> 00:00:30,340
pour essayer de concevoir des algorithmes

16
00:00:30,440 --> 00:00:31,310
pour résoudre des problèmes,

17
00:00:32,447 --> 00:00:34,659
et on a un certain nombre de principes algorithmiques

18
00:00:34,857 --> 00:00:37,926
qui seront, je dirais, ré-utilisés

19
00:00:38,026 --> 00:00:39,748
dans les algorithmes de tri.

20
00:00:39,848 --> 00:00:41,756
Donc nous aurons une première partie

21
00:00:41,856 --> 00:00:44,266
qui va justifier pourquoi est-ce qu'on essaie de trier,

22
00:00:45,024 --> 00:00:46,833
et une deuxième partie sur deux tris,

23
00:00:47,133 --> 00:00:48,909
le tri par insertion et le tri par sélection

24
00:00:49,009 --> 00:00:51,025
qui sont des tris que j'appelle itératifs

25
00:00:51,125 --> 00:00:52,077
c'est-à-dire dans lesquels

26
00:00:52,284 --> 00:00:53,213
petit à petit,

27
00:00:53,313 --> 00:00:54,753
on construit la liste

28
00:00:54,853 --> 00:00:55,888
ou le tableau trié

29
00:00:56,168 --> 00:00:58,170
et on a des incréments de 1

30
00:00:58,270 --> 00:00:59,284
c'est-à-dire que

31
00:00:59,431 --> 00:01:01,778
petit à petit, on rajoute un élément

32
00:01:01,878 --> 00:01:03,273
et puis ça augmente

33
00:01:03,749 --> 00:01:05,754
la qualité de ce qu'on est en train de regarder

34
00:01:05,969 --> 00:01:07,083
jusqu'à arriver à la fin

35
00:01:07,183 --> 00:01:08,635
quand on aura traiter le dernier élément.

36
00:01:08,886 --> 00:01:12,579
C'est cette idée qui est développée dans cette séquence.

