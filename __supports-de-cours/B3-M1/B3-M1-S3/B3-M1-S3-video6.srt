1
00:00:00,951 --> 00:00:03,507
Dans cette partie de séquence,

2
00:00:03,882 --> 00:00:05,299
nous allons étudier

3
00:00:05,399 --> 00:00:06,965
juste deux algorithmes de tri,

4
00:00:07,065 --> 00:00:09,411
le tri par sélection, le tri par insertion.

5
00:00:09,766 --> 00:00:11,710
Ce qui est important à retenir dans cette séquence,

6
00:00:11,810 --> 00:00:14,498
ce n'est pas tant les algorithmes en eux-mêmes

7
00:00:14,598 --> 00:00:16,062
mais les principes qui sont sous-jacents,

8
00:00:16,600 --> 00:00:19,914
un principe d'augmentation et un principe de diminution,

9
00:00:20,014 --> 00:00:21,059
qui sont cachés derrière,

10
00:00:21,478 --> 00:00:23,916
et d'autre part, la méthode que l'on emploie

11
00:00:24,016 --> 00:00:26,132
c'est-à-dire que lorsque l'on va développer un algorithme,

12
00:00:26,232 --> 00:00:27,383
on va avoir une certaine rigueur

13
00:00:27,756 --> 00:00:29,001
dans l'écriture de l'algorithme

14
00:00:29,101 --> 00:00:30,278
puis dans son analyse,

15
00:00:30,378 --> 00:00:31,378
puis dans la suite.

16
00:00:31,478 --> 00:00:32,731
Donc on va reparler

17
00:00:32,831 --> 00:00:34,571
du problème du tri.

18
00:00:35,037 --> 00:00:36,215
Le problème du tri,

19
00:00:36,315 --> 00:00:37,599
c'est quelque chose d'important

20
00:00:39,066 --> 00:00:40,287
que l'on va retrouver partout.

21
00:00:40,663 --> 00:00:43,243
On considère à la base, dans l'énoncé du problème,

22
00:00:43,343 --> 00:00:44,443
un ensemble d'éléments.

23
00:00:44,701 --> 00:00:46,328
Chaque élément possède une clé

24
00:00:46,428 --> 00:00:47,933
c'est-à-dire que chaque élément

25
00:00:48,768 --> 00:00:49,931
dispose d'une valeur

26
00:00:50,031 --> 00:00:52,781
selon laquelle on va vouloir trier les éléments.

27
00:00:53,259 --> 00:00:55,269
Par exemple, on a envie de trier

28
00:00:55,369 --> 00:00:57,723
selon la note si on veut faire un classement,

29
00:00:57,993 --> 00:00:59,527
on a envie de trier selon l'âge

30
00:00:59,627 --> 00:01:01,674
si on a envie de regarder

31
00:01:03,257 --> 00:01:04,608
des propriétés liées à l'âge

32
00:01:04,708 --> 00:01:06,126
ou des gens qui auront les droits ou pas

33
00:01:06,226 --> 00:01:07,308
de faire quelque chose.

34
00:01:08,341 --> 00:01:10,448
Donc on a un ensemble d'éléments,

35
00:01:10,548 --> 00:01:12,625
et on suppose que

36
00:01:12,974 --> 00:01:14,204
les valeurs des clés

37
00:01:14,529 --> 00:01:16,532
qui sont associées à chacun des éléments

38
00:01:16,772 --> 00:01:18,812
sont dans un espace qui est totalement ordonné

39
00:01:19,760 --> 00:01:21,520
c'est-à-dire que étant données deux clés,

40
00:01:21,798 --> 00:01:23,553
je vais pouvoir dire laquelle est la plus petite,

41
00:01:23,653 --> 00:01:24,523
laquelle est la plus grande.

42
00:01:24,928 --> 00:01:27,360
Alors évidemment, c'est un ordre dans lequel

43
00:01:27,460 --> 00:01:28,909
vous pouvez avoir égalité des clés,

44
00:01:29,009 --> 00:01:30,692
ce n'est pas un souci.

45
00:01:31,409 --> 00:01:34,348
L'objectif, c'est de structurer l'ensemble des éléments

46
00:01:34,448 --> 00:01:35,671
de manière à pouvoir accéder

47
00:01:35,974 --> 00:01:38,161
à l'élément de rang k directement

48
00:01:38,261 --> 00:01:42,096
c'est-à-dire le k-ième en partant du plus petit.

49
00:01:43,666 --> 00:01:46,087
L'opérateur que nous avons à notre disposition,

50
00:01:46,187 --> 00:01:47,530
c'est la comparaison de deux éléments

51
00:01:47,972 --> 00:01:49,657
qui peut nous renvoyer trois valeurs,

52
00:01:50,188 --> 00:01:52,693
une valeur qui est a est plus petit que b,

53
00:01:52,793 --> 00:01:55,688
la clé de l'élément a est plus petite que celle de l'élément b,

54
00:01:55,971 --> 00:01:57,161
on peut avoir l'égalité

55
00:01:57,467 --> 00:01:58,828
ou on peut avoir supérieur.

56
00:01:59,347 --> 00:02:02,557
Et dans pratiquement tous les exemples qu'on va prendre maintenant,

57
00:02:02,751 --> 00:02:04,407
on va utiliser un tableau d'éléments

58
00:02:04,507 --> 00:02:06,925
c'est-à-dire que vous allez accéder directement

59
00:02:07,304 --> 00:02:09,583
à l'élément qui est à la position i.

60
00:02:10,030 --> 00:02:13,643
Alors, la position au départ, ce n'est pas celle triée,

61
00:02:13,743 --> 00:02:14,953
et souvent à l'arrivée, on aura

62
00:02:15,427 --> 00:02:17,376
une position triée.

63
00:02:19,892 --> 00:02:21,892
Quelques remarques.

64
00:02:24,380 --> 00:02:25,707
Dans beaucoup d'ouvrages,

65
00:02:25,807 --> 00:02:28,274
et lorsqu'on a l'habitude de faire de l'algorithmique,

66
00:02:28,553 --> 00:02:31,665
en fait, on se passe de l'association clé/élément

67
00:02:31,991 --> 00:02:33,427
et on trie directement des clés,

68
00:02:33,794 --> 00:02:36,846
sachant que, aux clés, on associera des références aux éléments.

69
00:02:37,320 --> 00:02:39,796
Donc, dans la plupart des exemples que vous trouverez,

70
00:02:39,896 --> 00:02:42,083
on va trier des entiers, on va trier des nombres,

71
00:02:42,183 --> 00:02:43,077
on va trier des choses comme ça,

72
00:02:43,409 --> 00:02:46,119
mais il faut bien penser que d'un point de vue des applications,

73
00:02:46,718 --> 00:02:48,779
c'est uniquement derrière

74
00:02:48,879 --> 00:02:51,797
les objets, les éléments auxquels font référence ces clés

75
00:02:51,897 --> 00:02:53,324
qui vont être ordonnés.

76
00:02:53,424 --> 00:02:55,510
C'est quelque chose d'important.

77
00:02:55,610 --> 00:02:57,704
Par exemple, vous voulez trier des étudiants

78
00:02:57,804 --> 00:02:59,741
en fonction de leur numéro de carte d'étudiant,

79
00:03:00,024 --> 00:03:02,681
vous allez dire je trie les numéros de carte d'étudiant

80
00:03:02,781 --> 00:03:04,446
mais c'est sous-entendu derrière

81
00:03:04,546 --> 00:03:06,354
que si vous voulez l'étudiant ayant tel numéro

82
00:03:06,718 --> 00:03:08,852
vous accédiez au dossier de cet étudiant.

83
00:03:09,584 --> 00:03:11,400
Le deuxième point, c'est que la clé

84
00:03:11,500 --> 00:03:13,917
ne caractérise pas nécessairement l'élément

85
00:03:15,800 --> 00:03:17,182
que vous recherchez.

86
00:03:18,169 --> 00:03:19,920
Si vous prenez des dates de naissance,

87
00:03:20,020 --> 00:03:22,778
vous pouvez très bien avoir des jumeaux

88
00:03:22,878 --> 00:03:24,257
et donc ils ont la même date de naissance.

89
00:03:25,875 --> 00:03:28,208
Ça, c'est quelque chose d'important.

90
00:03:28,308 --> 00:03:31,315
La clé n'est pas un représentant,

91
00:03:33,034 --> 00:03:35,643
enfin je dirais, n'identifie pas complètement

92
00:03:35,743 --> 00:03:36,624
un élément.

93
00:03:37,503 --> 00:03:39,294
Enfin, la clé peut être

94
00:03:39,527 --> 00:03:41,211
aussi construite à la volée.

95
00:03:41,311 --> 00:03:43,601
C'est-à-dire qu'elle va être évaluée,

96
00:03:43,701 --> 00:03:45,250
on va regarder par exemple

97
00:03:45,350 --> 00:03:47,173
quelle est la propriété

98
00:03:47,273 --> 00:03:49,815
ou quelle est la valeur de la clé sur tel élément,

99
00:03:49,915 --> 00:03:51,312
on va appliquer une fonction

100
00:03:51,412 --> 00:03:53,629
qui calcule la clé sur cet élément-là.

101
00:03:55,169 --> 00:03:58,258
Alors, on a un certain nombre de conventions

102
00:03:58,358 --> 00:03:59,605
qu'on va essayer de suivre.

103
00:04:01,484 --> 00:04:03,709
Il y a des moments où je prendrai quelques libertés

104
00:04:03,985 --> 00:04:07,487
mais vous êtes capables de faire le lien.

105
00:04:07,587 --> 00:04:08,599
Première chose,

106
00:04:08,699 --> 00:04:10,499
on utilise un tableau

107
00:04:10,599 --> 00:04:12,005
indicé de 1 à n,

108
00:04:12,105 --> 00:04:14,162
ça, c'est la tradition algorithmique,

109
00:04:14,262 --> 00:04:15,771
ou de 0 à n - 1,

110
00:04:15,871 --> 00:04:17,513
ça, c'est en lien avec l'implémentation,

111
00:04:17,613 --> 00:04:18,731
en particulier en Python

112
00:04:18,831 --> 00:04:21,974
où effectivement on fait le lien entre un tableau

113
00:04:22,074 --> 00:04:24,204
et l'adressage dans ce tableau.

114
00:04:25,100 --> 00:04:28,166
En général, on pourrait utiliser des clés

115
00:04:28,266 --> 00:04:29,404
qui sont des nombres entiers,

116
00:04:29,504 --> 00:04:32,694
ça, c'est je dirais l'habitude

117
00:04:32,794 --> 00:04:34,646
que l'on a quand on est en algo avancée,

118
00:04:35,032 --> 00:04:36,603
mais du point de vue de l'introduction,

119
00:04:36,703 --> 00:04:38,181
je trouve beaucoup plus pertinent

120
00:04:38,516 --> 00:04:40,985
de dire qu'on va trier par exemple des lettres,

121
00:04:41,085 --> 00:04:42,435
des lettres de l'alphabet,

122
00:04:42,535 --> 00:04:45,145
tout le monde connaît l'ordre alphabétique,

123
00:04:45,553 --> 00:04:47,412
donc on va trier des lettres de l'alphabet

124
00:04:47,958 --> 00:04:50,734
et les entiers seront

125
00:04:51,161 --> 00:04:53,493
utilisés pour définir les indices

126
00:04:54,028 --> 00:04:56,294
c'est-à-dire les positions dans lesquelles seront ces lettres.

127
00:04:56,748 --> 00:04:58,305
Avec les étudiants,

128
00:04:58,405 --> 00:05:00,495
on a beaucoup de mal au démarrage

129
00:05:00,595 --> 00:05:03,161
à faire qu'ils distinguent bien

130
00:05:03,261 --> 00:05:04,895
une valeur d'indice

131
00:05:04,995 --> 00:05:05,903
dans un tableau

132
00:05:06,003 --> 00:05:07,412
et une valeur de clé

133
00:05:07,512 --> 00:05:09,369
qui est une valeur dans le tableau.

134
00:05:09,841 --> 00:05:13,013
Donc ça, c'est quelque chose auquel il faut faire attention

135
00:05:13,542 --> 00:05:16,529
quand on va faire effectivement de l'analyse des algorithmes

136
00:05:16,629 --> 00:05:19,072
et pour la compréhension de l'algorithme.

137
00:05:19,730 --> 00:05:21,480
Alors moi, je prends en général des lettres,

138
00:05:21,580 --> 00:05:23,633
on peut prendre des mots, on peut faire ce qu'on veut.

139
00:05:24,171 --> 00:05:26,407
L'opérateur de comparaison que j'utilise

140
00:05:26,507 --> 00:05:28,112
sera noté inférieur ou égal,

141
00:05:29,399 --> 00:05:29,959
ici.

142
00:05:30,357 --> 00:05:31,422
Évidemment,

143
00:05:33,282 --> 00:05:34,876
c'est tout à fait générique,

144
00:05:34,976 --> 00:05:37,218
quand vous comparez deux lettres,

145
00:05:37,318 --> 00:05:38,960
c'est l'ordre alphabétique qui va compter.

146
00:05:39,469 --> 00:05:41,783
Et souvent aussi,

147
00:05:42,132 --> 00:05:46,207
pour éviter de perturber la compréhension des algorithmes,

148
00:05:46,307 --> 00:05:49,061
on suppose que toutes les clés à trier sont distinctes.

149
00:05:49,426 --> 00:05:51,034
Alors ça se généralise très bien

150
00:05:51,134 --> 00:05:52,469
lorsque l'on a des cas d'égalité

151
00:05:52,569 --> 00:05:54,323
mais c'est pour éviter

152
00:05:54,423 --> 00:05:57,896
de mettre l'accent sur une propriété d'algorithme

153
00:05:58,764 --> 00:06:00,859
qui n'est pas intéressante à ce niveau-là.

154
00:06:00,959 --> 00:06:03,035
Ce qui nous, nous intéresse, c'est la structure

155
00:06:03,416 --> 00:06:05,672
et pas tant le détail qui est sur le côté

156
00:06:05,772 --> 00:06:06,590
pour l'instant.

157
00:06:07,350 --> 00:06:10,570
Alors, moi, j'aime bien des exemples de mots,

158
00:06:10,670 --> 00:06:13,169
en voilà un qui est tout à fait sympathique

159
00:06:13,269 --> 00:06:14,172
qui fait douze lettres,

160
00:06:14,272 --> 00:06:16,072
stylographie,

161
00:06:16,172 --> 00:06:18,960
et qui effectivement a toutes ses lettres qui sont différentes.

162
00:06:19,060 --> 00:06:22,190
Donc je l'utilise abusivement dans mes cours

163
00:06:22,290 --> 00:06:24,020
lorsque je dois faire des algorithmes de tri.

