1
00:00:00,835 --> 00:00:02,314
Nous allons passer maintenant

2
00:00:03,500 --> 00:00:05,174
à la recherche dans un tableau trié.

3
00:00:05,499 --> 00:00:07,447
L'idée qui est derrière,

4
00:00:07,547 --> 00:00:11,364
c'est, étant donné un tableau d'éléments T

5
00:00:11,644 --> 00:00:12,438
ordonné

6
00:00:13,092 --> 00:00:14,432
d'éléments comparables deux à deux,

7
00:00:14,844 --> 00:00:16,186
il s'agit de voir

8
00:00:16,286 --> 00:00:19,308
si votre élément est dans le tableau ou pas.

9
00:00:20,180 --> 00:00:21,250
On a une convention

10
00:00:21,350 --> 00:00:22,897
mais qui pourrait être un peu différente,

11
00:00:22,997 --> 00:00:24,381
qui consiste à dire que

12
00:00:24,694 --> 00:00:26,508
les indices vont aller de 0 à n -  1.

13
00:00:27,071 --> 00:00:28,917
Alors on pourrait dire aussi par convention

14
00:00:29,017 --> 00:00:30,782
-1 représente l'absence de x

15
00:00:31,153 --> 00:00:32,258
dans le tableau T.

16
00:00:33,359 --> 00:00:35,397
Pour simplifier l'analyse,

17
00:00:35,497 --> 00:00:36,948
et pour simplifier l'exposé,

18
00:00:37,048 --> 00:00:38,189
on pourra supposer que

19
00:00:38,289 --> 00:00:43,282
T contient des éléments qui sont tous distincts

20
00:00:43,382 --> 00:00:44,670
et que T est non vide.

21
00:00:48,869 --> 00:00:51,873
Alors, le principe qui est caché derrière

22
00:00:51,973 --> 00:00:54,009
la recherche dichotomique dans un tableau trié,

23
00:00:54,367 --> 00:00:55,663
c'est de dire on va

24
00:00:56,011 --> 00:00:58,134
diviser le tableau en deux parties,

25
00:00:58,483 --> 00:00:59,493
deux parties égales,

26
00:00:59,902 --> 00:01:02,768
et on va segmenter

27
00:01:02,868 --> 00:01:03,890
et essayer de dire si on est

28
00:01:03,990 --> 00:01:05,456
dans la partie gauche ou dans la partie droite.

29
00:01:05,742 --> 00:01:08,493
Ça dépend de la donnée que vous avez à votre disposition

30
00:01:08,791 --> 00:01:11,531
et donc on va aller à faire des branchements

31
00:01:11,631 --> 00:01:13,809
pour essayer de réduire

32
00:01:13,909 --> 00:01:15,230
l'espace de recherche.

33
00:01:15,330 --> 00:01:16,437
Donc ici, on est sur

34
00:01:16,537 --> 00:01:18,679
de la réduction de l'espace de recherche.

35
00:01:18,987 --> 00:01:21,108
Je vous donne un exemple très simple

36
00:01:21,351 --> 00:01:22,460
de recherche dichotomique,

37
00:01:22,804 --> 00:01:25,185
donc ici, j'ai n, la taille de T,

38
00:01:25,285 --> 00:01:30,232
et puis j'ai deux choses qui sont importantes,

39
00:01:31,008 --> 00:01:34,681
j'ai la partie gauche et la partie droite

40
00:01:34,947 --> 00:01:36,449
qui sont deux indices

41
00:01:36,549 --> 00:01:38,110
et je vais faire en sorte que

42
00:01:38,210 --> 00:01:40,264
l'élément que je recherche,

43
00:01:40,364 --> 00:01:44,371
l'élément de clé, ou l'élément x,

44
00:01:44,471 --> 00:01:46,211
se trouve systématiquement

45
00:01:46,810 --> 00:01:48,491
sur une case entre,

46
00:01:48,591 --> 00:01:50,681
alors, systématiquement s'il est dans le tableau,

47
00:01:50,781 --> 00:01:53,532
entre l'indice gauche et l'indice droit.

48
00:01:55,295 --> 00:01:57,362
Le schéma est relativement simple.

49
00:01:58,420 --> 00:02:00,996
Tant que, effectivement,

50
00:02:01,096 --> 00:02:02,681
je vais chercher à un endroit donné

51
00:02:03,193 --> 00:02:05,347
c'est-à-dire que j'ai effectivement un intervalle

52
00:02:05,726 --> 00:02:07,217
gauche plus petit que droit,

53
00:02:07,317 --> 00:02:09,048
dans ce cas-là, je vais pouvoir faire la recherche,

54
00:02:09,148 --> 00:02:11,224
donc je vais tester si le milieu

55
00:02:11,619 --> 00:02:13,349
est différent de x,

56
00:02:13,912 --> 00:02:16,863
donc ça, c'est la condition de test

57
00:02:17,259 --> 00:02:22,064
et puis, si le milieu est égal à x,

58
00:02:22,164 --> 00:02:22,863
je vais m'arrêter.

59
00:02:23,255 --> 00:02:26,268
Si la valeur de l'indice qui est au milieu

60
00:02:26,368 --> 00:02:27,208
est plus grande que x,

61
00:02:27,480 --> 00:02:30,815
alors je vais prendre l'intervalle de gauche,

62
00:02:31,241 --> 00:02:33,480
ça veut dire que mon élément x

63
00:02:33,580 --> 00:02:35,177
est sur le côté gauche du tableau,

64
00:02:35,560 --> 00:02:39,449
et si c'est, je dirais, au dessus de x,

65
00:02:39,549 --> 00:02:41,897
et bien je vais prendre la partie droite.

66
00:02:41,997 --> 00:02:43,453
Donc on aurait pu faire un dessin.

67
00:02:46,674 --> 00:02:48,405
J'ai pris le milieu

68
00:02:48,505 --> 00:02:50,093
donc ici,

69
00:02:50,703 --> 00:02:53,470
je vais avoir l'indice gauche.

70
00:02:54,609 --> 00:02:57,548
Ici, je vais avoir l'indice droit,

71
00:02:58,247 --> 00:03:02,361
je prends le milieu qui est ici,

72
00:03:05,055 --> 00:03:07,412
je vais regarder si T[m]

73
00:03:13,329 --> 00:03:15,495
est supérieur à x,

74
00:03:17,509 --> 00:03:20,418
ça sous-entend que mon élément x

75
00:03:20,518 --> 00:03:23,331
 va être dans cette partie-là,

76
00:03:24,119 --> 00:03:25,153
et sinon,

77
00:03:25,253 --> 00:03:29,104
l'élément x va être dans cette partie-là.

78
00:03:48,299 --> 00:03:50,830
Une fois que j'ai ça,

79
00:03:50,930 --> 00:03:53,732
je peux regarder des exemples.

80
00:03:54,307 --> 00:03:57,668
Ici, j'ai pris un tableau trié

81
00:03:57,958 --> 00:03:59,450
avec des lettres.

82
00:03:59,550 --> 00:04:01,878
L'idée, c'est quand même de prendre des lettres

83
00:04:02,424 --> 00:04:03,585
plutôt que des nombres

84
00:04:03,685 --> 00:04:05,666
pour éviter la confusion au niveau des élèves

85
00:04:06,105 --> 00:04:08,449
entre la valeur d'un indice

86
00:04:08,549 --> 00:04:10,584
et une valeur d'un élément.

87
00:04:11,497 --> 00:04:14,353
Donc ici, je me retrouve avec

88
00:04:14,656 --> 00:04:16,420
ce tableau.

89
00:04:16,661 --> 00:04:18,712
J'ai l'indice gauche,

90
00:04:19,143 --> 00:04:20,233
l'indice droit,

91
00:04:20,945 --> 00:04:23,627
le milieu va être égal à 3,

92
00:04:25,635 --> 00:04:27,164
ça, c'est le milieu,

93
00:04:27,549 --> 00:04:29,242
donc si je vais chercher m,

94
00:04:29,342 --> 00:04:35,191
je vais dire il est plus grand que h

95
00:04:35,291 --> 00:04:36,340
dans l'ordre alphabétique

96
00:04:36,661 --> 00:04:38,423
et donc je vais aller à l'étape suivante.

97
00:04:39,592 --> 00:04:41,340
Donc si je vais à l'étape suivante,

98
00:04:41,440 --> 00:04:44,072
je vais avoir comme indice gauche

99
00:04:44,434 --> 00:04:48,634
3 et 6,

100
00:04:48,734 --> 00:04:50,581
le milieu, c'est 4,

101
00:04:51,988 --> 00:04:52,975
je vais comparer,

102
00:04:53,281 --> 00:04:55,260
et quand je compare,

103
00:04:56,126 --> 00:05:01,432
je vais obtenir que i est plus petit que m

104
00:05:01,532 --> 00:05:04,700
et donc je me retrouve avec un gauche,

105
00:05:04,800 --> 00:05:06,100
un droit,

106
00:05:06,464 --> 00:05:08,250
et cætera, et cætera.

107
00:05:13,115 --> 00:05:14,466
Voilà, je continue,

108
00:05:14,566 --> 00:05:15,956
et à la fin, je trouve

109
00:05:16,378 --> 00:05:18,877
effectivement que m est en position 5.

110
00:05:21,388 --> 00:05:24,606
Maintenant, si j'ai envie d'aller un petit peu plus loin,

111
00:05:24,706 --> 00:05:25,878
qu'est-ce qui est derrière,

112
00:05:25,978 --> 00:05:27,297
derrière tout ça ?

113
00:05:28,129 --> 00:05:29,561
En fait, ce qui est derrière tout ça,

114
00:05:29,661 --> 00:05:32,308
et c'est caché, et ce n'est pas visible

115
00:05:32,408 --> 00:05:34,800
je dirais quand on parle comme ça,

116
00:05:34,900 --> 00:05:36,681
c'est qu'il y a une structure de décision

117
00:05:36,781 --> 00:05:38,159
et d'arbre de décision.

118
00:05:38,465 --> 00:05:39,860
Donc en fait, ce que je vais faire

119
00:05:39,960 --> 00:05:42,201
je vais représenter,

120
00:05:43,053 --> 00:05:44,337
je reprends mon tableau,

121
00:05:44,662 --> 00:05:46,407
ici, j'ai la représentation schématique,

122
00:05:46,769 --> 00:05:49,001
donc j'ai mon tableau et en fait,

123
00:05:49,311 --> 00:05:51,164
sous-jacent à ce tableau,

124
00:05:51,638 --> 00:05:52,783
j'ai un arbre

125
00:05:52,883 --> 00:05:55,113
qui correspond en fait aux décisions

126
00:05:55,213 --> 00:05:56,143
que je vais prendre.

127
00:05:56,243 --> 00:05:58,731
Ça, ça correspond aux milieux que je vais regarder

128
00:05:59,030 --> 00:06:00,082
au fur et à mesure.

129
00:06:00,182 --> 00:06:02,869
Première étape, j'avais comparé avec h,

130
00:06:03,164 --> 00:06:07,314
et donc j'avais dit je suis dans le sous-arbre droit.

131
00:06:07,623 --> 00:06:10,403
Je vais comparer avec m,

132
00:06:10,503 --> 00:06:12,376
je suis dans le sous-arbre droit, et cætera.

133
00:06:12,719 --> 00:06:15,081
Et donc quand j'ai fait mon opération,

134
00:06:15,443 --> 00:06:17,789
j'ai juste eu à décider

135
00:06:18,114 --> 00:06:20,236
dans quelle branche je partais.

136
00:06:21,734 --> 00:06:23,469
C'est vraiment important

137
00:06:23,569 --> 00:06:24,777
de se rendre compte que

138
00:06:24,877 --> 00:06:27,618
sous-jacent, c'est un test

139
00:06:27,718 --> 00:06:29,086
donc une réponse binaire

140
00:06:29,588 --> 00:06:31,570
donc on a derrière un arbre de décision.

141
00:06:32,620 --> 00:06:34,072
Donc ça, c'est le premier point.

142
00:06:34,494 --> 00:06:36,382
Deuxièmement, ça revient à dire

143
00:06:36,482 --> 00:06:38,960
que les indices du tableau,

144
00:06:39,060 --> 00:06:40,854
c'est-à-dire ces valeurs-là,

145
00:06:41,543 --> 00:06:43,786
correspondent à des nœuds dans un arbre

146
00:06:44,104 --> 00:06:45,900
et donc, ça correspond effectivement

147
00:06:46,000 --> 00:06:47,347
au fait que vous allez pouvoir

148
00:06:48,815 --> 00:06:51,197
vous basez sur cette structure implicite

149
00:06:51,473 --> 00:06:52,902
d'arbre à l'intérieur

150
00:06:53,002 --> 00:06:54,172
de votre tableau.

151
00:06:54,540 --> 00:06:56,607
Si on va un petit peu plus loin,

152
00:06:57,016 --> 00:06:58,456
ici, on a ce qu'on appelle

153
00:06:58,556 --> 00:06:59,791
un arbre binaire de recherche

154
00:06:59,891 --> 00:07:04,649
qui sera vu dans les cours suivants.

155
00:07:05,117 --> 00:07:07,366
Si je regarde maintenant mon algorithme,

156
00:07:08,600 --> 00:07:10,362
mon algorithme de recherche dichotomique,

157
00:07:10,462 --> 00:07:11,552
j'ai exactement ça.

158
00:07:11,652 --> 00:07:15,240
Ça teste si x est dans T.

159
00:07:15,340 --> 00:07:18,175
Alors voyez que pour faire la preuve,

160
00:07:18,275 --> 00:07:19,367
j'ai fait une première étape

161
00:07:19,467 --> 00:07:21,298
qui est une simplification abusive.

162
00:07:21,732 --> 00:07:23,308
C'est-à-dire que cette fois-ci,

163
00:07:23,408 --> 00:07:25,376
au lieu de retourner juste l'indice,

164
00:07:25,720 --> 00:07:27,026
je vais juste renvoyer

165
00:07:27,126 --> 00:07:28,081
vrai ou faux.

166
00:07:28,349 --> 00:07:29,447
Donc ça, ça va me permettre

167
00:07:29,547 --> 00:07:30,724
de juste regarder

168
00:07:31,109 --> 00:07:32,430
comment se comporte mon algorithme

169
00:07:32,530 --> 00:07:34,114
et comment se fait la condition d'arrêt.

170
00:07:34,989 --> 00:07:38,801
Ici, je n'ai plus l'arrêt en cours de route,

171
00:07:39,110 --> 00:07:42,344
donc je vais sans doute aller plus loin dans mon arbre,

172
00:07:42,655 --> 00:07:44,347
je vais juste m'arrêter quand gauche

173
00:07:44,447 --> 00:07:45,540
est égal à droit.

174
00:07:48,564 --> 00:07:51,661
Pour faire la preuve de cet algorithme,

175
00:07:52,050 --> 00:07:53,437
je vais regarder

176
00:07:53,702 --> 00:07:54,614
l'invariant,

177
00:07:54,714 --> 00:07:57,525
et donc un invariant ici assez simple,

178
00:07:57,625 --> 00:07:59,578
c'est de dire que x est dans T

179
00:07:59,678 --> 00:08:00,606
si et seulement si

180
00:08:00,920 --> 00:08:05,620
x est dans le sous-intervalle de T entre gauche et droit.

181
00:08:06,061 --> 00:08:07,532
Et donc, la précondition,

182
00:08:07,632 --> 00:08:08,757
comme j'ai gauche

183
00:08:09,225 --> 00:08:11,414
qui est égal à 0, droit qui est égal à n - 1,

184
00:08:11,514 --> 00:08:14,567
évidemment, la précondition est vérifiée.

185
00:08:18,796 --> 00:08:20,884
Si je regarde maintenant

186
00:08:20,984 --> 00:08:21,955
les différents cas,

187
00:08:22,556 --> 00:08:25,219
je vais avoir la propagation,

188
00:08:25,319 --> 00:08:26,496
donc comment ça

189
00:08:27,043 --> 00:08:28,904
vrai va se propager

190
00:08:29,194 --> 00:08:31,952
en ça vrai à ce niveau-là.

191
00:08:32,303 --> 00:08:34,419
Et bien, tout simplement, si x

192
00:08:34,789 --> 00:08:36,618
est plus grand que T du milieu,

193
00:08:36,718 --> 00:08:37,435
ça veut dire que

194
00:08:38,970 --> 00:08:41,413
vous avez x qui

195
00:08:41,713 --> 00:08:43,742
ne peut être que dans l'intervalle

196
00:08:43,842 --> 00:08:44,947
de droite,

197
00:08:45,273 --> 00:08:46,750
donc si vous avez

198
00:08:46,850 --> 00:08:47,885
le cas 1,

199
00:08:48,323 --> 00:08:50,331
le fait que vous ayez la condition

200
00:08:50,431 --> 00:08:52,860
T[m]

201
00:08:54,348 --> 00:08:55,803
inférieur à x,

202
00:08:55,903 --> 00:08:57,649
ça veut dire que

203
00:08:58,166 --> 00:09:00,417
donc ici, vous avez m,

204
00:09:01,282 --> 00:09:03,502
la condition T[m] strictement inférieur à x,

205
00:09:03,602 --> 00:09:04,822
ça va vous interdire

206
00:09:05,341 --> 00:09:08,455
pour x toute cette partie-là.

207
00:09:08,555 --> 00:09:11,901
Donc x ne peut pas être dans la partie gauche

208
00:09:12,001 --> 00:09:13,732
donc si x est dans le tableau,

209
00:09:13,832 --> 00:09:15,350
il est obligatoirement dans la partie droite.

210
00:09:15,758 --> 00:09:17,361
Et si vous regardez le cas 2,

211
00:09:17,836 --> 00:09:20,495
et bien vous avez exactement la même figure,

212
00:09:22,076 --> 00:09:23,336
dans ce cas-là,

213
00:09:23,719 --> 00:09:26,765
vous avez m qui est ici

214
00:09:27,558 --> 00:09:28,896
et vous savez que vous êtes

215
00:09:29,456 --> 00:09:31,605
dans cette partie-là, et celle-là

216
00:09:32,993 --> 00:09:35,706
est impossible pour x.

217
00:09:36,182 --> 00:09:40,348
Vous avez les deux cas qui sont relativement simples

218
00:09:40,715 --> 00:09:43,415
et donc vous avez la propagation de l'invariant,

219
00:09:43,515 --> 00:09:45,013
et donc en fin d'itération,

220
00:09:45,113 --> 00:09:46,582
l'invariant est également vrai.

221
00:09:46,997 --> 00:09:49,175
Il reste à montrer la post-condition

222
00:09:49,590 --> 00:09:51,840
c'est-à-dire en sortie de l'itération,

223
00:09:52,920 --> 00:09:55,669
je vais avoir gauche qui est égal à droit,

224
00:09:57,174 --> 00:09:59,775
gauche est inférieur ou égal à droit,

225
00:10:01,205 --> 00:10:03,038
j'ai une inégalité large,

226
00:10:03,138 --> 00:10:04,775
et donc lorsque je sors

227
00:10:04,875 --> 00:10:07,103
évidemment gauche n'est plus inférieur

228
00:10:07,203 --> 00:10:08,455
strictement à droit

229
00:10:08,555 --> 00:10:09,685
donc j'ai égalité

230
00:10:09,785 --> 00:10:10,854
donc gauche égale droit.

231
00:10:11,165 --> 00:10:13,919
Et x est dans l'intervalle

232
00:10:14,019 --> 00:10:15,541
gauche-droit

233
00:10:15,641 --> 00:10:17,470
qui est égal à gauche ici,

234
00:10:17,867 --> 00:10:19,850
donc si x est dans le tableau,

235
00:10:19,950 --> 00:10:21,157
ça ne peut être que

236
00:10:21,531 --> 00:10:24,492
l'élément d'indice gauche.

237
00:10:24,592 --> 00:10:29,771
Je fais donc un return T[gauche] == x,

238
00:10:29,871 --> 00:10:31,060
c'est-à-dire je teste si

239
00:10:31,160 --> 00:10:33,912
cet élément-là est l'élément x recherché.

240
00:10:34,267 --> 00:10:35,774
Si oui, et bien je l'ai trouvé,

241
00:10:35,874 --> 00:10:37,454
si non, il n'est pas dans le tableau.

242
00:10:39,491 --> 00:10:42,329
Donc l'invariant me permet de prouver

243
00:10:45,878 --> 00:10:47,835
la correction de mon algorithme.

244
00:10:49,017 --> 00:10:50,441
La terminaison,

245
00:10:50,541 --> 00:10:52,061
elle est relativement évidente.

246
00:10:52,396 --> 00:10:54,737
Il suffit de regarder le variant d'itération

247
00:10:54,837 --> 00:10:56,124
qui est droit - gauche

248
00:10:56,499 --> 00:10:58,445
qui est toujours supérieur ou égal à 0,

249
00:10:58,545 --> 00:10:59,416
ce qu'on a vu tout à l'heure,

250
00:10:59,699 --> 00:11:02,100
puisque gauche est toujours inférieur ou égal à droit,

251
00:11:02,496 --> 00:11:03,798
c'est une valeur entière,

252
00:11:03,898 --> 00:11:05,679
positive, strictement décroissante,

253
00:11:05,779 --> 00:11:07,021
et donc l'algorithme se termine

254
00:11:07,121 --> 00:11:09,491
en un nombre fini d'étapes.

255
00:11:11,032 --> 00:11:12,797
J'ai la correction partielle,

256
00:11:13,369 --> 00:11:14,611
j'ai la terminaison

257
00:11:14,711 --> 00:11:15,956
donc j'ai la correction totale

258
00:11:16,056 --> 00:11:18,270
donc mon algorithme est correct.

