1
00:00:04,761 --> 00:00:06,839
Au fait, c'est quoi un algorithme ?

2
00:00:06,939 --> 00:00:08,768
Si on représente un algorithme,

3
00:00:08,868 --> 00:00:11,155
et qu'on pose la question à nos voisins,

4
00:00:11,255 --> 00:00:12,662
à des gens qui nous entourent,

5
00:00:12,912 --> 00:00:14,446
on aura beaucoup de définitions,

6
00:00:14,546 --> 00:00:15,595
on aura beaucoup de mots

7
00:00:15,695 --> 00:00:18,102
et finalement, la notion n'est pas

8
00:00:18,202 --> 00:00:20,881
tout à fait bien saisie.

9
00:00:20,981 --> 00:00:22,461
On ne sait pas très bien expliquer.

10
00:00:22,561 --> 00:00:25,444
Donc l'objectif de toute cette première séquence

11
00:00:25,544 --> 00:00:27,869
sera d'essayer de définir

12
00:00:27,969 --> 00:00:28,734
proprement

13
00:00:29,322 --> 00:00:30,867
la notion d'algorithme,

14
00:00:30,967 --> 00:00:32,460
de voir ce qui s'y rattache,

15
00:00:32,560 --> 00:00:35,294
et surtout de la mettre dans un paysage

16
00:00:35,394 --> 00:00:36,678
qui est le paysage de l'informatique.

17
00:00:37,128 --> 00:00:39,605
Donc pour faire ça, nous allons regarder

18
00:00:40,048 --> 00:00:44,582
un certain nombre d'exemples.

19
00:00:46,850 --> 00:00:49,154
Ça, ce sont des exemples que vous connaissez tous,

20
00:00:49,254 --> 00:00:51,313
des exemples que vous avez vus en classe

21
00:00:51,750 --> 00:00:53,171
et dont vous avez entendu parler,

22
00:00:53,271 --> 00:00:55,073
que ce soit l'algorithme d'Euclide

23
00:00:56,052 --> 00:00:58,691
qui vous permet de faire du calcul sur les entiers,

24
00:00:59,300 --> 00:01:03,199
des algorithmes de résolution d'équations algébriques,

25
00:01:03,597 --> 00:01:06,900
qui ont été pour les premières posées par Al-Khwarizmi,

26
00:01:07,000 --> 00:01:09,413
dont le nom a donné par la suite algorithme.

27
00:01:09,812 --> 00:01:11,867
Et puis vous avez eu l'algorithme d'Euler

28
00:01:11,967 --> 00:01:13,786
pour faire des résolutions d'équations différentielles.

29
00:01:13,886 --> 00:01:15,447
Donc vous avez eu le terme algorithme

30
00:01:15,547 --> 00:01:17,896
qui a été utilisé abondamment dans les mathématiques,

31
00:01:18,135 --> 00:01:20,505
mais qu'on a utilisé également un peu ailleurs,

32
00:01:20,605 --> 00:01:23,070
donc on trouve ce mot algorithme

33
00:01:23,170 --> 00:01:24,135
un peu partout.

34
00:01:24,573 --> 00:01:27,638
Et donc ce mot algorithme s'est développé ces dernières années,

35
00:01:27,738 --> 00:01:29,892
et ça fait une quinzaine d'années

36
00:01:29,992 --> 00:01:31,474
que je note que dans les journaux,

37
00:01:31,574 --> 00:01:32,988
tout le monde parle des algorithmes,

38
00:01:33,474 --> 00:01:35,530
tout le monde dénonce les algorithmes,

39
00:01:35,630 --> 00:01:37,392
tout le monde se plaint de l'algorithme,

40
00:01:37,973 --> 00:01:39,900
tout le monde dit que l'algorithme est inhumain,

41
00:01:40,000 --> 00:01:41,349
enfin, on dit toutes sortes de choses,

42
00:01:41,834 --> 00:01:43,300
et c'est ça que je vais essayer d'explorer

43
00:01:43,400 --> 00:01:44,651
dans cette toute petite vidéo

44
00:01:44,841 --> 00:01:47,155
pour vous montrer que la situation,

45
00:01:47,255 --> 00:01:49,229
elle n'est pas facile,

46
00:01:49,329 --> 00:01:50,542
et nous, en tant qu'enseignants,

47
00:01:50,715 --> 00:01:53,083
on va avoir effectivement à expliquer des choses

48
00:01:53,183 --> 00:01:54,301
par rapport à ça.

49
00:01:55,196 --> 00:01:58,019
Donc si je reprends les journaux.

50
00:01:58,119 --> 00:01:59,600
Alors ici, j'ai fait des extraits de journaux,

51
00:01:59,700 --> 00:02:01,234
j'ai fait des extraits assez récents,

52
00:02:01,334 --> 00:02:01,995
de cette année.

53
00:02:02,333 --> 00:02:05,047
Donc ici, un algorithme

54
00:02:05,147 --> 00:02:07,643
qui est indiqué là,

55
00:02:08,045 --> 00:02:11,050
met 68 milliards de mélodies dans le domaine public,

56
00:02:11,424 --> 00:02:13,090
pour éviter de parler de plagiat,

57
00:02:13,343 --> 00:02:15,701
donc qui génère automatiquement toutes les mélodies.

58
00:02:15,859 --> 00:02:17,585
Alors, le mot algorithme,

59
00:02:18,128 --> 00:02:19,372
on ne sait pas très bien,

60
00:02:19,472 --> 00:02:20,700
nous, en tant qu'informaticiens,

61
00:02:20,800 --> 00:02:22,981
on sent bien que c'est quelque chose qui a généré des choses

62
00:02:23,081 --> 00:02:24,760
mais on ne sait pas très bien

63
00:02:24,860 --> 00:02:28,627
est-ce que c'est un algorithme qui peut être utilisé

64
00:02:28,727 --> 00:02:30,610
pour reconnaître qu'une mélodie existe ou pas ?

65
00:02:31,116 --> 00:02:34,417
Donc là, il y a des petites choses qui ne sont pas si simples,

66
00:02:34,517 --> 00:02:35,774
et en fait, on veut

67
00:02:36,100 --> 00:02:37,210
éviter le plagiat

68
00:02:37,310 --> 00:02:39,663
donc un algorithme pour éviter le plagiat,

69
00:02:39,763 --> 00:02:41,634
pour moi, ce n'est pas forcément

70
00:02:42,200 --> 00:02:44,632
la façon la plus simple de donner un algorithme.

71
00:02:46,546 --> 00:02:52,032
Parcoursup, l'orientation de nos jeunes ne peut reposer

72
00:02:53,267 --> 00:02:55,735
sur un unique algorithme

73
00:02:55,986 --> 00:02:58,619
Alors là, effectivement, on peut se dire

74
00:02:58,719 --> 00:03:01,318
mais comment est-ce qu'on comprend le terme algorithme ?

75
00:03:02,109 --> 00:03:04,019
Pour moi, ce n'est pas du tout clair,

76
00:03:04,927 --> 00:03:08,086
il semblerait que Parcoursup contienne plein de choses

77
00:03:09,075 --> 00:03:11,834
dont des algorithmes, et pas un seul algorithme.

78
00:03:13,048 --> 00:03:14,536
Est-ce qu'il y a un choix ? Enfin bon,

79
00:03:14,636 --> 00:03:15,906
Donc là aussi,

80
00:03:16,006 --> 00:03:17,311
je pense que le journaliste

81
00:03:17,411 --> 00:03:19,748
a voulu faire un titre un peu accrocheur

82
00:03:19,848 --> 00:03:21,033
mais ce n'était pas très clair

83
00:03:21,327 --> 00:03:22,484
au niveau du message.

84
00:03:23,585 --> 00:03:27,061
Après, vous avez l'utilisation de l'algorithme

85
00:03:27,161 --> 00:03:29,725
et d'une notion qui, moi, me paraît tout à fait drôle,

86
00:03:30,261 --> 00:03:32,655
ce sont des algorithmes qui sont ici

87
00:03:33,230 --> 00:03:36,040
devenus obsolètes.

88
00:03:36,709 --> 00:03:38,585
Et bien, comment je fais

89
00:03:38,886 --> 00:03:41,000
pour avoir un algorithme qui est obsolète ?

90
00:03:41,100 --> 00:03:43,014
Donc on a, on définit, on colle

91
00:03:43,254 --> 00:03:44,394
sur la notion d'algorithme

92
00:03:44,494 --> 00:03:45,613
une notion d'obsolescence,

93
00:03:45,713 --> 00:03:47,194
de durée de vie, enfin bon.

94
00:03:48,646 --> 00:03:49,987
Pour moi, ce n'est pas clair.

95
00:03:50,489 --> 00:03:52,311
Ce n'est toujours pas clair, la notion d'algorithme

96
00:03:52,411 --> 00:03:53,629
telle qu'elle est énoncée ici.

97
00:03:54,705 --> 00:03:58,344
Et puis, quand on rentre dans les réseaux sociaux,

98
00:03:59,478 --> 00:04:01,922
tout est cause,

99
00:04:03,395 --> 00:04:05,317
enfin, c'est à cause d'un algorithme

100
00:04:05,417 --> 00:04:07,061
si effectivement, on n'est pas visible,

101
00:04:07,161 --> 00:04:08,440
si effectivement, on est mal noté,

102
00:04:08,540 --> 00:04:09,560
si on n'a pas les likes,

103
00:04:09,660 --> 00:04:10,640
et cætera.

104
00:04:10,740 --> 00:04:14,100
Et à la fin, on a des phrases du genre

105
00:04:14,796 --> 00:04:17,017
"On ne plaisante pas avec l'algorithme."

106
00:04:17,446 --> 00:04:19,400
comme si c'était un objet avec lequel

107
00:04:19,500 --> 00:04:21,323
on aurait pu éventuellement plaisanter.

108
00:04:21,792 --> 00:04:24,676
Donc là encore, le sens du mot algorithme

109
00:04:25,232 --> 00:04:28,239
n'est pas forcément très clair.

110
00:04:28,627 --> 00:04:30,421
Alors ça, c'était les journaux nationaux,

111
00:04:30,521 --> 00:04:32,160
j'ai fait exactement la même expérience

112
00:04:32,260 --> 00:04:33,988
sur des journaux régionaux.

113
00:04:35,566 --> 00:04:39,216
En particulier là, c'était sur la loi sur la sécurité.

114
00:04:41,001 --> 00:04:43,136
"Le projet de loi pérennise

115
00:04:44,700 --> 00:04:46,076
le recours aux algorithmes."

116
00:04:46,714 --> 00:04:48,021
Ça veut dire qu'en gros

117
00:04:48,121 --> 00:04:49,913
les algorithmes sont une aide

118
00:04:51,004 --> 00:04:53,533
qu'on aurait le droit d'utiliser

119
00:04:53,633 --> 00:04:55,049
ou pas le droit d'utiliser.

120
00:04:55,488 --> 00:04:57,379
Ça rentre dans le droit,

121
00:04:57,479 --> 00:04:59,607
donc ça pénètre à l'intérieur d'une sphère

122
00:04:59,707 --> 00:05:00,991
qui était jusqu'à présent

123
00:05:01,996 --> 00:05:04,109
ignorée par la science informatique

124
00:05:04,369 --> 00:05:05,731
en ce sens-là.

125
00:05:05,831 --> 00:05:07,905
Ce n'est pas un objet de droit.

126
00:05:08,266 --> 00:05:13,330
Donc là encore, complexité de la définition.

127
00:05:13,801 --> 00:05:15,683
Je crois que j'ai encore une définition

128
00:05:15,783 --> 00:05:18,083
"L'algorithme plus fort que l'avocat"

129
00:05:18,553 --> 00:05:21,263
Ça, c'est encore dans Ouest France.

130
00:05:23,285 --> 00:05:25,927
La vérité ne peut pas s'écrire avec un algorithme

131
00:05:26,027 --> 00:05:27,954
donc là encore, les journalistes utilisent

132
00:05:28,054 --> 00:05:29,695
le terme algorithme dans un sens

133
00:05:30,876 --> 00:05:35,049
que je ne suis pas capable forcément de comprendre clairement.

134
00:05:35,494 --> 00:05:37,929
Donc en conclusion de cette toute petite vidéo,

135
00:05:39,600 --> 00:05:40,503
on pourrait se dire :

136
00:05:40,603 --> 00:05:42,256
on va regarder dans le dictionnaire.

137
00:05:43,244 --> 00:05:45,044
Donc on va regarder dans le dictionnaire

138
00:05:45,144 --> 00:05:46,445
et si on regarde dans le dictionnaire,

139
00:05:46,545 --> 00:05:48,712
donc ici, c'est la définition du Grand Robert,

140
00:05:48,812 --> 00:05:50,832
qui est sans doute une des références

141
00:05:52,108 --> 00:05:56,107
des définitions des mots de la langue française,

142
00:05:56,381 --> 00:05:57,642
il nous dit que,

143
00:05:57,742 --> 00:05:59,367
on va avoir le mot algorithme,

144
00:05:59,467 --> 00:06:03,466
c'est, je regarde, un ensemble de règles

145
00:06:03,566 --> 00:06:05,614
opératoires propres à un calcul.

146
00:06:05,714 --> 00:06:06,678
Ce  n'est pas très clair.

147
00:06:06,778 --> 00:06:07,439
Qu'est-ce que c'est qu'une règle ?

148
00:06:07,539 --> 00:06:08,338
Ce n'est pas très clair.

149
00:06:09,517 --> 00:06:11,065
Une suite de règles formelles.

150
00:06:11,165 --> 00:06:13,817
Alors là, ça devient vraiment très mathématique

151
00:06:14,436 --> 00:06:15,731
mais alors, quel est le lien

152
00:06:15,831 --> 00:06:18,408
avec ce qu'on a vu avant dans les journaux ?

153
00:06:19,688 --> 00:06:22,603
Donc, ce n'est pas évident de comprendre

154
00:06:23,643 --> 00:06:25,393
ce que c'est qu'un algorithme.

155
00:06:25,493 --> 00:06:27,644
Alors, intuitivement, un ensemble de règles

156
00:06:28,055 --> 00:06:31,509
qui permet de réaliser mécaniquement toute opération particulière

157
00:06:31,609 --> 00:06:33,389
correspondant à un type d'opération.

158
00:06:34,644 --> 00:06:39,580
Bon, pour moi, il y a des éléments clés,

159
00:06:39,680 --> 00:06:40,929
j'ai des mots clés qui apparaissent,

160
00:06:41,029 --> 00:06:42,238
qui effectivement ressortent,

161
00:06:42,700 --> 00:06:44,681
mais qui ne sont pas forcément avec du sens

162
00:06:44,781 --> 00:06:45,770
avec ce qu'on a vu avant.

163
00:06:46,203 --> 00:06:47,947
Alors on peut regarder plus loin,

164
00:06:48,147 --> 00:06:49,275
dans le Larousse,

165
00:06:49,600 --> 00:06:51,195
où on a une autre définition,

166
00:06:51,295 --> 00:06:52,860
c'est un ensemble de règles opératoires

167
00:06:53,513 --> 00:06:56,530
dont l'application permet de résoudre un problème.

168
00:06:56,886 --> 00:06:58,894
Alors ici, j'insiste sur le terme problème

169
00:07:00,092 --> 00:07:03,752
puisque le terme n'était pas dans la définition précédente du Robert,

170
00:07:04,096 --> 00:07:06,942
Et un algorithme peut être traduit,

171
00:07:07,166 --> 00:07:08,746
donc c'est cette notion de traduction

172
00:07:08,846 --> 00:07:10,086
qui va être importante aussi,

173
00:07:10,986 --> 00:07:13,763
dans un langage de programmation.

174
00:07:15,763 --> 00:07:17,878
Donc déjà, on a un certain nombre de termes

175
00:07:17,978 --> 00:07:20,277
qui commencent à apparaître dans les définitions sur les dictionnaires,

176
00:07:20,581 --> 00:07:23,144
mais qui sont pour l'instant encore très éloignées

177
00:07:23,244 --> 00:07:24,232
de ce qu'on a vu avant.

178
00:07:24,717 --> 00:07:26,936
Alors vous avez également, donc je vous le cite,

179
00:07:27,036 --> 00:07:32,903
le dictionnaire des trésors de la langue française,

180
00:07:33,485 --> 00:07:36,348
qui redonne encore une autre définition

181
00:07:36,448 --> 00:07:37,887
à la notion d'algorithme.

182
00:07:39,091 --> 00:07:41,122
Enfin, moi j'aurais envie de dire

183
00:07:41,222 --> 00:07:43,377
que un algorithme dans le langage courant,

184
00:07:43,477 --> 00:07:46,285
en fait, c'est un mot qui comprend un grand nombre de sens ;

185
00:07:46,859 --> 00:07:48,525
il sous-entend en général

186
00:07:48,625 --> 00:07:49,800
l'existence d'un ordinateur,

187
00:07:49,900 --> 00:07:52,315
c'est-à-dire d'une machine permettant le traitement de l'information,

188
00:07:52,720 --> 00:07:55,144
et souvent, sont confondus

189
00:07:55,244 --> 00:07:57,091
l'objectif, la méthode employée,

190
00:07:57,396 --> 00:07:59,497
le programme qui est exécuté,

191
00:07:59,597 --> 00:08:00,052
et cætera.

192
00:08:00,152 --> 00:08:02,741
Donc ça demande un petit peu de soin.

193
00:08:04,348 --> 00:08:05,900
La chose qu'il faut noter,

194
00:08:06,000 --> 00:08:08,114
c'est que ça a un impact social qui est très important,

195
00:08:08,646 --> 00:08:09,945
tout le monde en parle,

196
00:08:10,045 --> 00:08:11,481
tout le monde met des tas de choses derrière,

197
00:08:11,581 --> 00:08:13,156
on veut réguler les algorithmes,

198
00:08:13,556 --> 00:08:15,283
j'avais vu ça dans un autre article,

199
00:08:15,821 --> 00:08:18,258
donc mettre une loi pour réguler les algorithmes,

200
00:08:18,680 --> 00:08:21,924
et ça intéresse également beaucoup le public

201
00:08:22,024 --> 00:08:23,442
puisqu'il y a un grand nombre

202
00:08:24,445 --> 00:08:26,824
d'ouvrages scientifiques de vulgarisation

203
00:08:26,924 --> 00:08:28,672
qui portent sur les algorithmes.

204
00:08:29,734 --> 00:08:32,453
Mais pour nous, on voit qu'on sent bien 

205
00:08:32,553 --> 00:08:33,796
qu'on a un besoin de formalisation

206
00:08:34,449 --> 00:08:36,115
dans le contexte de l'informatique

207
00:08:36,215 --> 00:08:38,164
au sens de science informatique.

