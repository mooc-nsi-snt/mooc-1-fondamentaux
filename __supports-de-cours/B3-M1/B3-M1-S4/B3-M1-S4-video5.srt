1
00:00:01,336 --> 00:00:02,875
Comment on implémente

2
00:00:02,975 --> 00:00:04,110
un type abstrait de données ?

3
00:00:04,210 --> 00:00:06,639
Alors on choisit une représentation interne

4
00:00:06,739 --> 00:00:08,264
et puis on fournit les opérations

5
00:00:08,364 --> 00:00:09,634
associées à la spécification.

6
00:00:10,330 --> 00:00:12,090
Et donc, du point de vue de la preuve,

7
00:00:12,190 --> 00:00:13,669
on essaie de prouver tous les axiomes.

8
00:00:15,771 --> 00:00:17,198
Tout ça pour vous dire

9
00:00:17,298 --> 00:00:18,810
que si j'ai un type abstrait de données

10
00:00:18,910 --> 00:00:19,616
qui est bien fait,

11
00:00:21,669 --> 00:00:24,668
je ne peux pas me servir de son implémentation

12
00:00:24,768 --> 00:00:26,004
pour court-circuiter des choses,

13
00:00:26,303 --> 00:00:28,993
et donc je travaille avec l'objet en tant que tel

14
00:00:29,234 --> 00:00:32,009
sans savoir comment il est implémenté à l'intérieur.

15
00:00:32,392 --> 00:00:35,149
Si j'ai besoin de choses supplémentaires de l'intérieur,

16
00:00:35,249 --> 00:00:37,308
et bien il faut impérativement

17
00:00:37,629 --> 00:00:39,677
que je le spécifie et que je modifie

18
00:00:39,777 --> 00:00:41,333
ma structure d'objet, mon type abstrait.

19
00:00:42,375 --> 00:00:44,691
Mais une fois que mon type abstrait est défini,

20
00:00:44,791 --> 00:00:45,766
mon objet est défini,

21
00:00:45,866 --> 00:00:47,241
je ne touche pas

22
00:00:47,660 --> 00:00:49,566
à la structure interne de cet objet.

23
00:00:49,666 --> 00:00:50,824
Je n'y ai même pas accès,

24
00:00:50,924 --> 00:00:52,986
je ne sais pas du tout comment c'est fait à l'intérieur.

25
00:00:53,776 --> 00:00:55,801
Et ça, c'est vraiment un garde-fou important.

26
00:00:56,420 --> 00:00:59,413
Ce qui veut dire que pour un même type abstrait,

27
00:01:00,365 --> 00:01:02,307
on peut avoir plusieurs implémentations.

28
00:01:03,304 --> 00:01:06,986
Par exemple, si je veux implémenter une file dans un tableau,

29
00:01:07,543 --> 00:01:09,619
et bien je vais dire voilà,

30
00:01:09,719 --> 00:01:11,408
ma file, je vais la représenter

31
00:01:11,670 --> 00:01:12,635
sous cette forme-là,

32
00:01:13,024 --> 00:01:14,985
j'ai un nombre d'éléments dans la file,

33
00:01:15,514 --> 00:01:17,654
et je représente ça,

34
00:01:17,754 --> 00:01:21,157
la tête est ici,

35
00:01:24,604 --> 00:01:27,870
et la queue est ici.

36
00:01:29,571 --> 00:01:32,115
Après, je peux regarder toutes mes opérations.

37
00:01:32,635 --> 00:01:34,514
Je peux réécrire mes opérations.

38
00:01:40,268 --> 00:01:42,530
Quand je veux enfiler quelque chose,

39
00:01:42,630 --> 00:01:44,900
je suis obligé de tout décaler.

40
00:01:46,339 --> 00:01:48,392
Ça, c'est une première implémentation

41
00:01:48,492 --> 00:01:50,061
que l'on peut avoir d'une file.

42
00:01:51,699 --> 00:01:53,251
On pourrait représenter la file

43
00:01:53,351 --> 00:01:55,230
dans un tableau circulaire,

44
00:01:55,614 --> 00:01:57,088
c'est-à-dire que

45
00:01:59,250 --> 00:02:00,627
je vais mettre un élément,

46
00:02:00,727 --> 00:02:03,768
la queue de ma file, c'est là,

47
00:02:03,868 --> 00:02:05,389
et la tête est là,

48
00:02:05,617 --> 00:02:08,076
et donc, quand je veux enlever quelque chose en tête,

49
00:02:08,176 --> 00:02:10,257
je vais déplacer le curseur,

50
00:02:10,357 --> 00:02:11,429
donc je vais couper ça

51
00:02:11,529 --> 00:02:12,462
et je vais le mettre là,

52
00:02:12,840 --> 00:02:14,805
et quand je veux mettre un élément e ici,

53
00:02:14,905 --> 00:02:15,717
je vais le mettre là

54
00:02:16,018 --> 00:02:19,277
et je vais pointer sur la prochaine qui va être là.

55
00:02:20,081 --> 00:02:22,977
Donc je vais avoir deux indices,

56
00:02:23,279 --> 00:02:26,359
et puis je vais pouvoir travailler là-dessus.

57
00:02:26,459 --> 00:02:28,416
C'est ce qu'on appelle un tableau circulaire.

58
00:02:28,595 --> 00:02:30,244
Ici, vous avez un exemple à la fin

59
00:02:30,647 --> 00:02:34,183
dans lequel la queue de ma file est ici,

60
00:02:34,470 --> 00:02:36,407
et la tête de ma file est ici,

61
00:02:36,680 --> 00:02:37,988
je fais le tour du tableau.

62
00:02:38,230 --> 00:02:40,179
On peut le représenter sous la forme

63
00:02:43,147 --> 00:02:44,435
de quelque chose qui serait comme ça,

64
00:02:50,192 --> 00:02:51,807
où vous avez vos éléments,

65
00:02:51,907 --> 00:02:52,913
et votre file,

66
00:02:54,550 --> 00:02:56,320
elle représente quelque chose comme ça,

67
00:02:56,420 --> 00:02:57,091
et ça tourne.

68
00:02:57,191 --> 00:02:58,407
Évidemment, vous avez le problème

69
00:02:58,507 --> 00:03:00,679
de quand on dépasse la taille du tableau

70
00:03:00,779 --> 00:03:01,738
dans lequel est stockée la file,

71
00:03:02,457 --> 00:03:06,959
qui est un problème de programmation de l'objet

72
00:03:07,059 --> 00:03:10,550
et non un problème lié à l'utilisation de cet objet.

73
00:03:11,776 --> 00:03:14,046
On peut également représenter

74
00:03:14,365 --> 00:03:15,741
une file dans une liste chaînée,

75
00:03:16,037 --> 00:03:17,616
où la tête de la file

76
00:03:18,976 --> 00:03:20,699
est indiquée,

77
00:03:20,799 --> 00:03:22,045
donc c'est une liste chaînée à laquelle

78
00:03:23,953 --> 00:03:26,465
 j'ai mis une sémantique sur la liste

79
00:03:26,565 --> 00:03:28,445
et j'ai rajouté un pointeur sur la queue.

80
00:03:28,813 --> 00:03:30,984
Et puis, si je veux insérer un élément,

81
00:03:32,181 --> 00:03:34,945
je vais rajouter ici

82
00:03:36,546 --> 00:03:38,440
s, voilà.

83
00:03:38,806 --> 00:03:41,134
Ici, celui-là, je vais le faire pointer là,

84
00:03:41,456 --> 00:03:44,113
celui-là, il va là

85
00:03:44,213 --> 00:03:46,626
et celui-là va être là.

86
00:03:46,996 --> 00:03:52,503
Et si je veux supprimer un élément,

87
00:03:52,603 --> 00:03:54,152
c'est-à-dire extraire un élément de la file,

88
00:03:54,561 --> 00:03:55,500
je supprime

89
00:03:57,141 --> 00:03:59,517
et je vais sur cet élément-là.

90
00:03:59,903 --> 00:04:01,588
Donc on a effectivement

91
00:04:01,688 --> 00:04:02,942
une structure équivalente

92
00:04:03,260 --> 00:04:08,303
pour notre file dans une liste chaînée.

93
00:04:09,476 --> 00:04:11,350
Alors je peux reprendre les opérations

94
00:04:11,450 --> 00:04:12,795
tel que je vous ai montré.

95
00:04:13,131 --> 00:04:15,411
On a des avantages et des inconvénients

96
00:04:15,511 --> 00:04:16,804
à chacune des implémentations.

97
00:04:17,131 --> 00:04:19,282
Et vous voyez qu'on a séparé complètement

98
00:04:19,382 --> 00:04:20,688
l'implémentation de l'usage.

99
00:04:21,020 --> 00:04:22,875
Ça, c'est le travail d'un développeur

100
00:04:22,975 --> 00:04:25,074
qui est indépendant du travail du développeur

101
00:04:25,174 --> 00:04:27,213
qui va utiliser la file.

102
00:04:27,524 --> 00:04:29,803
Pour le tableau, on a des avantages,

103
00:04:29,903 --> 00:04:32,192
l'allocation unique, accès rapide.

104
00:04:33,338 --> 00:04:35,317
Les inconvénients, c'est que c'est borné,

105
00:04:35,828 --> 00:04:38,273
qu'on a de l'espace mémoire qui n'est pas utilisé

106
00:04:38,373 --> 00:04:40,076
si vous avez réservé 10M

107
00:04:40,176 --> 00:04:41,894
et que vous n'utilisez que quelques kilos,

108
00:04:42,277 --> 00:04:45,432
c'est évident que ce n'est pas intéressant.

109
00:04:46,119 --> 00:04:48,463
Si vous utilisez la liste chaînée,

110
00:04:48,563 --> 00:04:51,822
on a des avantages qui est l'optimisation de la mémoire,

111
00:04:51,922 --> 00:04:53,842
c'est-à-dire que vous pouvez avoir plusieurs files

112
00:04:54,186 --> 00:04:55,379
dans un même espace

113
00:04:55,479 --> 00:04:56,838
et puis vous pouvez les coder comme ça,

114
00:04:57,156 --> 00:04:58,279
par chaînage.

115
00:04:59,202 --> 00:05:01,231
Par contre, la gestion de mémoire est plus lourde.

116
00:05:01,612 --> 00:05:04,189
Vous avez à manipuler des choses qui sont un peu plus compliquées,

117
00:05:04,289 --> 00:05:05,075
des sources d'erreur.

118
00:05:05,977 --> 00:05:08,047
Voilà, il faut après choisir

119
00:05:08,895 --> 00:05:11,864
le bon format

120
00:05:12,174 --> 00:05:13,462
au niveau de l'implémentation

121
00:05:13,723 --> 00:05:14,869
et il est fréquent,

122
00:05:16,198 --> 00:05:18,734
lorsque vous allez utiliser des bibliothèques

123
00:05:19,159 --> 00:05:21,076
que vous ayez plusieurs implémentations

124
00:05:21,395 --> 00:05:23,875
pour le même objet

125
00:05:24,239 --> 00:05:27,544
et que, en fonction des primitives qui vont être utilisées

126
00:05:27,733 --> 00:05:29,434
pour travailler sur cet objet,

127
00:05:29,648 --> 00:05:33,982
une représentation sera plus efficace que d'autres.

128
00:05:34,230 --> 00:05:37,061
Par exemple, si vous voulez faire de la concaténation de files,

129
00:05:38,002 --> 00:05:39,453
c'est sans doute plus efficace

130
00:05:39,553 --> 00:05:42,673
d'avoir la notion de liste chaînée

131
00:05:42,773 --> 00:05:44,017
pour représenter la file.

132
00:05:46,473 --> 00:05:50,197
On attaque maintenant un tout petit problème,

133
00:05:50,297 --> 00:05:51,638
qui est un problème classique,

134
00:05:51,873 --> 00:05:54,311
dans lequel on montre comment on peut utiliser une pile

135
00:05:54,984 --> 00:05:57,371
pour faire ce travail.

136
00:05:57,940 --> 00:05:59,384
Donc je me donne un texte,

137
00:05:59,484 --> 00:06:01,110
par exemple une expression arithmétique,

138
00:06:01,210 --> 00:06:05,506
par exemple un texte de l'HTML ou des choses comme ça,

139
00:06:05,831 --> 00:06:08,086
et dans ce texte, j'ai des parenthèses ouvrantes

140
00:06:08,186 --> 00:06:09,163
et des parenthèses fermantes.

141
00:06:09,649 --> 00:06:11,927
Et il s'agit de vérifier que ma formule,

142
00:06:12,171 --> 00:06:13,533
que mon expression

143
00:06:13,633 --> 00:06:14,379
est bien formée.

144
00:06:14,684 --> 00:06:16,865
Ce sont des expressions parenthésées.

145
00:06:17,410 --> 00:06:18,657
C'est classique.

146
00:06:19,650 --> 00:06:21,553
Je dois avoir quelque chose qui est comme ça.

147
00:06:21,938 --> 00:06:24,746
Au passage, on a derrière une structure d'arbre.

148
00:06:26,526 --> 00:06:27,954
On reverra ça par la suite.

149
00:06:28,333 --> 00:06:31,432
Ici, j'ai des expressions qui sont mal parenthésées.

150
00:06:37,135 --> 00:06:39,365
On a effectivement des problèmes liés

151
00:06:39,465 --> 00:06:41,231
au nombre de parenthèses ouvrantes et fermantes,

152
00:06:41,331 --> 00:06:42,681
on a des problèmes qui peuvent être liés

153
00:06:42,781 --> 00:06:45,634
à la façon dont les parenthèses ouvrantes et fermantes sont imbriquées.

154
00:06:46,739 --> 00:06:48,398
L'objectif, c'est de vérifier que c'est correct.

155
00:06:50,154 --> 00:06:52,934
Après, je peux mettre des éléments à l'intérieur

156
00:06:53,034 --> 00:06:53,848
que j'appelle neutres.

157
00:06:54,444 --> 00:06:57,636
Et donc, typiquement vous sentez venir les expressions arithmétiques.

158
00:07:00,035 --> 00:07:01,354
Comment je vais vérifier

159
00:07:01,705 --> 00:07:03,526
qu'une expression est bien parenthésée ?

160
00:07:03,863 --> 00:07:06,350
Et bien tout simplement, je vais partir d'une pile vide,

161
00:07:06,925 --> 00:07:09,260
et puis, tant qu'il reste un caractère,

162
00:07:09,360 --> 00:07:10,513
je regarde le caractère.

163
00:07:10,613 --> 00:07:12,492
Si c'est une parenthèse ouvrante, je l'empile.

164
00:07:12,868 --> 00:07:14,735
Si c'est une parenthèse fermante,

165
00:07:15,089 --> 00:07:16,399
là, je vais tester.

166
00:07:16,499 --> 00:07:18,614
Si la pile est vide,

167
00:07:18,714 --> 00:07:20,206
j'ai une erreur de parenthésage

168
00:07:20,611 --> 00:07:22,233
sinon, je dépile.

169
00:07:22,728 --> 00:07:30,497
En fait, l'opération Empiler correspond à la parenthèse ouvrante,

170
00:07:30,772 --> 00:07:32,776
et Dépiler correspond à la parenthèse fermante.

171
00:07:33,881 --> 00:07:35,008
Et puis à la fin,

172
00:07:35,108 --> 00:07:38,803
une fois que j'ai traité toute ma chaîne de caractères,

173
00:07:38,903 --> 00:07:43,688
je retourne la valeur de EstVide.

174
00:07:43,788 --> 00:07:46,713
En gros, si la pile est vide à la fin,

175
00:07:46,813 --> 00:07:48,861
ça veut dire que mon parenthésage est correct.

176
00:07:49,250 --> 00:07:54,188
Au passage ici, j'ai détecté une erreur de parenthésage

177
00:07:55,025 --> 00:07:56,165
en cours de route,

178
00:07:56,265 --> 00:07:57,590
ce n'est pas la peine que j'analyse

179
00:07:57,747 --> 00:07:59,331
ma structure jusqu'au bout.

180
00:08:02,164 --> 00:08:06,386
Donc ici, en fait, j'aurais très bien pu prendre un entier.

181
00:08:08,272 --> 00:08:10,123
C'est-à-dire que j'aurais pu dire

182
00:08:10,223 --> 00:08:12,255
je compte le nombre de parenthèses ouvrantes,

183
00:08:13,146 --> 00:08:16,275
et à chaque fois, dès que j'ai une parenthèse fermante,

184
00:08:16,375 --> 00:08:18,520
je soustrais 1 à mon compteur.

185
00:08:18,897 --> 00:08:20,496
Donc il suffit d'un entier.

186
00:08:21,449 --> 00:08:23,660
Mais en fait, si vous regardez bien,

187
00:08:23,760 --> 00:08:26,184
même en Python, même dans tous les langages de programmation,

188
00:08:26,608 --> 00:08:27,943
vous n'avez pas que des parenthèses.

189
00:08:28,309 --> 00:08:29,710
Vous avez des environnements

190
00:08:29,810 --> 00:08:31,391
et ces environnements sont délimités

191
00:08:31,491 --> 00:08:32,736
par des symboles,

192
00:08:32,987 --> 00:08:34,254
les parenthèses,

193
00:08:34,428 --> 00:08:36,123
les accolades,

194
00:08:36,387 --> 00:08:38,357
des begin et des end,

195
00:08:39,324 --> 00:08:42,030
des indentations, des débuts et fins d'indentation,

196
00:08:42,517 --> 00:08:45,754
ça, c'est si je prends des langages de programmation classiques,

197
00:08:46,052 --> 00:08:48,085
si vous prenez des langages à base de balises,

198
00:08:48,185 --> 00:08:51,589
comme HTML, XML, et cætera,

199
00:08:51,689 --> 00:08:54,669
vous avez une balise d'ouverture, une balise de fermeture.

200
00:08:55,025 --> 00:08:56,977
Donc une très jolie généralisation

201
00:08:57,077 --> 00:08:58,883
de l'algorithme des parenthèses, c'est de dire

202
00:08:59,162 --> 00:09:01,019
vérifier qu'un texte html

203
00:09:01,245 --> 00:09:02,086
est bien formé,

204
00:09:02,186 --> 00:09:04,680
c'est-à-dire que les balises sont bien placées

205
00:09:05,178 --> 00:09:06,568
et sont bien imbriquées

206
00:09:06,668 --> 00:09:09,492
pour que le texte entier soit cohérent.

207
00:09:10,298 --> 00:09:14,127
Par exemple, ça, c'est quelque chose qui est correct,

208
00:09:14,623 --> 00:09:15,735
cette expression-là

209
00:09:16,158 --> 00:09:17,577
qui est donnée ici,

210
00:09:17,677 --> 00:09:19,736
est correcte si je ne tiens pas compte

211
00:09:20,110 --> 00:09:21,436
du type des parenthèses,

212
00:09:21,536 --> 00:09:22,601
mais qui est incorrecte

213
00:09:22,701 --> 00:09:25,097
si je regarde les deux types de parenthèses à la fois.

214
00:09:26,001 --> 00:09:27,774
Alors, comment est-ce qu'on va faire ?

215
00:09:28,000 --> 00:09:34,269
Et bien, je vais réutiliser exactement la même structure,

216
00:09:34,588 --> 00:09:36,154
sauf que là, les objets que j'empile

217
00:09:36,254 --> 00:09:39,179
vont être des objets qui vont être des balises d'ouverture.

218
00:09:41,647 --> 00:09:45,621
Et je vois qu'ici, j'ai un principe algorithmique important

219
00:09:45,721 --> 00:09:47,890
au niveau de la façon de décrire des algorithmes,

220
00:09:48,198 --> 00:09:50,661
je vais passer d'un algorithme qui était simple

221
00:09:51,184 --> 00:09:52,636
et je vais le raffiner

222
00:09:52,942 --> 00:09:55,524
pour le transformer en quelque chose qui va me traiter

223
00:09:55,848 --> 00:09:57,405
des choses plus compliquées.

224
00:09:58,018 --> 00:10:00,846
Donc ici, si c'est une parenthèse ouvrante, je l'empile.

225
00:10:01,233 --> 00:10:02,762
Si c'est une parenthèse fermante,

226
00:10:02,862 --> 00:10:04,157
je vais tester

227
00:10:04,360 --> 00:10:09,699
si la parenthèse fermante correspond

228
00:10:09,799 --> 00:10:11,752
à celle que j'ai envie de dépiler.

229
00:10:12,129 --> 00:10:14,445
Dans ce cas-là, si c'est bon,

230
00:10:14,758 --> 00:10:15,745
je vais dépiler.

231
00:10:15,845 --> 00:10:17,717
C'est ici, cette partie-là.

232
00:10:18,013 --> 00:10:18,987
Si ce n'est pas bon,

233
00:10:19,087 --> 00:10:21,603
ça veut dire que j'ai une situation du genre

234
00:10:22,149 --> 00:10:23,813
j'ai ça, suivi de ça,

235
00:10:24,899 --> 00:10:26,656
ce n'est pas bon et donc je dis

236
00:10:26,896 --> 00:10:30,513
je peux faire remonter une erreur de parenthésage.

237
00:10:30,995 --> 00:10:33,270
Évidemment, les conditions de terminaison

238
00:10:33,571 --> 00:10:35,080
sont les mêmes que tout à l'heure.

239
00:10:35,798 --> 00:10:37,819
À la fin de l'exécution,

240
00:10:37,919 --> 00:10:41,663
je dois avoir toutes mes parenthèses bien refermées

241
00:10:41,986 --> 00:10:43,149
et donc que ma pile est vide.

242
00:10:43,875 --> 00:10:46,080
Donc ça, ça vous permet effectivement

243
00:10:46,462 --> 00:10:49,754
de traiter n'importe quel langage à base de balises.

244
00:10:50,448 --> 00:10:54,775
On est sur les premiers, je dirais, interpréteurs de langage

245
00:10:54,875 --> 00:10:57,524
qui vont vous permettre de dire ça, c'est une bonne expression.

246
00:11:02,805 --> 00:11:05,613
On peut avoir également des parenthèses non orientées

247
00:11:05,713 --> 00:11:07,805
comme les guillemets,

248
00:11:07,905 --> 00:11:10,025
et là, ça devient encore un peu plus compliqué.

249
00:11:10,415 --> 00:11:11,697
Parce que selon les cas,

250
00:11:11,797 --> 00:11:12,978
vous devez soit empiler,

251
00:11:13,850 --> 00:11:14,848
soit dépiler

252
00:11:14,948 --> 00:11:16,549
si par exemple vous avez des guillemets

253
00:11:16,649 --> 00:11:18,658
ou des quotes par exemple en Python.

254
00:11:21,497 --> 00:11:27,285
Si je regarde ce type de processus,

255
00:11:27,385 --> 00:11:29,532
en fait, j'ai des niveaux d'imbrication,

256
00:11:29,632 --> 00:11:31,921
c'est-à-dire en un point du texte

257
00:11:32,021 --> 00:11:33,468
c'est le nombre de parenthèses ouvrantes

258
00:11:33,568 --> 00:11:35,985
qu'il y a en amont de ce point-là.

259
00:11:36,346 --> 00:11:38,789
Et le contexte, c'est la partie du texte

260
00:11:39,031 --> 00:11:40,679
qui est comprise entre deux parenthèses

261
00:11:41,697 --> 00:11:43,859
qui sont à un même niveau d'imbrication.

262
00:11:44,623 --> 00:11:47,498
Et donc ici, on peut poser la question

263
00:11:47,598 --> 00:11:49,245
par exemple quel est le niveau d'imbrication

264
00:11:50,395 --> 00:11:52,380
maximal de ce texte-là.

265
00:11:54,978 --> 00:11:56,578
Si je regarde bien,

266
00:11:56,678 --> 00:11:58,158
c'est les lettres d et e

267
00:11:58,448 --> 00:12:01,854
qui sont ici et ici

268
00:12:02,824 --> 00:12:05,468
qui sont à un niveau d'imbrication de 3.

269
00:12:07,376 --> 00:12:09,117
Vous avez des contextes différents

270
00:12:11,685 --> 00:12:13,386
qui sont empilés les uns sur les autres.

271
00:12:14,509 --> 00:12:17,227
Alors question : est-ce qu'on est capable de

272
00:12:17,327 --> 00:12:20,653
se poser des questions sur ceci ?

273
00:12:20,753 --> 00:12:23,599
Est-ce que je peux calculer le niveau d'imbrication maximal ?

274
00:12:24,183 --> 00:12:29,059
Je peux réutiliser mon algorithme de tout à l'heure

275
00:12:29,159 --> 00:12:32,579
pour en plus maintenant calculer un niveau d'imbrication.

276
00:12:32,679 --> 00:12:35,197
Est-ce que je peux apparier les parenthèses correspondantes

277
00:12:35,297 --> 00:12:36,706
en donnant leurs positions dans le texte ?

278
00:12:37,134 --> 00:12:39,353
De la même façon, est-ce que je peux calculer

279
00:12:39,699 --> 00:12:41,353
les longueurs des différents contextes ?

280
00:12:42,022 --> 00:12:44,638
Alors, pour le niveau d'imbrication maximal, oui.

281
00:12:44,738 --> 00:12:47,962
Il suffit d'actualiser la hauteur courante de la pile

282
00:12:48,062 --> 00:12:49,119
et la hauteur maximale.

283
00:12:49,219 --> 00:12:50,231
Donc ça, ça marche bien.

284
00:12:50,608 --> 00:12:54,889
Si je veux apparier les parenthèses correspondantes,

285
00:12:54,989 --> 00:13:00,311
je vais empiler avec la parenthèse la position dans le texte.

286
00:13:00,411 --> 00:13:01,808
Donc ça, ça ne pose pas de problème.

287
00:13:02,105 --> 00:13:04,788
Calculer la longueur du contexte,

288
00:13:04,888 --> 00:13:07,880
je maintiens à jour une longueur de contexte courant.

289
00:13:08,218 --> 00:13:10,376
Lorsqu'on ouvre une nouvelle parenthèse, on l'empile

290
00:13:10,689 --> 00:13:13,684
et on empile la longueur du contexte supérieur,

291
00:13:13,784 --> 00:13:15,717
et on part de 0 pour le contexte supérieur.

292
00:13:15,992 --> 00:13:17,791
Et lorsque je ferme, je fais l'addition

293
00:13:17,891 --> 00:13:19,169
avec ce que j'ai observé avant.

294
00:13:20,486 --> 00:13:22,723
Voilà, donc là, on a vu comment,

295
00:13:22,951 --> 00:13:24,491
juste sur un exemple simple,

296
00:13:24,591 --> 00:13:26,112
on va raffiner un algorithme

297
00:13:26,212 --> 00:13:28,333
pour obtenir un algorithme

298
00:13:28,433 --> 00:13:30,419
qui va être de test

299
00:13:30,519 --> 00:13:33,930
je dirais d'un programme qui soit bien formé.

