1
00:00:00,992 --> 00:00:02,684
Alors beaucoup d'algorithmes

2
00:00:02,784 --> 00:00:04,854
en fait regardent

3
00:00:05,087 --> 00:00:07,539
l'ensemble des nœuds d'un arbre

4
00:00:07,639 --> 00:00:10,135
et donc un des premiers problèmes

5
00:00:10,439 --> 00:00:11,810
qui peut se poser, c'est

6
00:00:12,172 --> 00:00:13,793
essayer de regarder

7
00:00:13,893 --> 00:00:15,608
tout ce qui est contenu à l'intérieur d'un arbre.

8
00:00:15,940 --> 00:00:18,141
Voyez, pour observer un arbre,

9
00:00:18,494 --> 00:00:19,521
pour l'étudier,

10
00:00:19,621 --> 00:00:21,967
je vais être obligé de rentrer dans la structure,

11
00:00:22,067 --> 00:00:24,170
c'est-à-dire de regarder et d'examiner des nœuds

12
00:00:24,513 --> 00:00:26,797
et puis d'examiner les sous-arbres

13
00:00:26,897 --> 00:00:28,427
qui sont rattachés à ce nœud-là.

14
00:00:28,950 --> 00:00:30,333
Et donc en fait,

15
00:00:30,433 --> 00:00:33,619
les idées qu'on va avoir pour faire ça,

16
00:00:33,719 --> 00:00:35,316
c'est de parcourir

17
00:00:35,416 --> 00:00:37,391
et essayer de faire un parcours entre guillemets

18
00:00:37,806 --> 00:00:39,177
de façon récursive,

19
00:00:39,277 --> 00:00:40,421
c'est-à-dire que quand j'examine

20
00:00:40,721 --> 00:00:41,871
à un moment donné un nœud,

21
00:00:42,032 --> 00:00:43,123
je vais regarder

22
00:00:43,529 --> 00:00:45,748
les sous-arbres qui dépendent de ce nœud.

23
00:00:49,054 --> 00:00:52,065
Pour éviter, je dirais, tous les petits problèmes,

24
00:00:52,165 --> 00:00:53,808
on va utiliser une opération Traiter,

25
00:00:54,177 --> 00:00:56,259
alors qui peut être de stocker dans une structure,

26
00:00:56,359 --> 00:00:58,903
qui peut être de calculer quelque chose dessus, et cætera,

27
00:00:59,003 --> 00:01:00,797
et donc on fait une petite abstraction

28
00:01:00,897 --> 00:01:02,914
pour dire : quand je vais passer sur un nœud,

29
00:01:03,014 --> 00:01:03,839
je vais le traiter.

30
00:01:07,148 --> 00:01:09,098
L'idée quand même derrière, c'est

31
00:01:09,198 --> 00:01:10,873
on a une structure qui va être récursive,

32
00:01:10,973 --> 00:01:11,949
la structure de données,

33
00:01:12,259 --> 00:01:13,255
et par contre,

34
00:01:16,184 --> 00:01:18,647
on peut utiliser d'autres façons

35
00:01:18,747 --> 00:01:20,257
de parcourir cette structure

36
00:01:20,760 --> 00:01:22,377
que juste une forme récursive.

37
00:01:23,303 --> 00:01:24,634
Alors si je prends

38
00:01:25,494 --> 00:01:29,397
un arbre, là, que j'ai coloré en noir,

39
00:01:29,497 --> 00:01:30,742
et j'ai fait un parcours en rouge,

40
00:01:31,054 --> 00:01:32,872
qui va regarder chacun des nœuds.

41
00:01:32,972 --> 00:01:34,676
Comme je sais que je dois passer

42
00:01:34,776 --> 00:01:36,625
et explorer les sous-arbres,

43
00:01:36,725 --> 00:01:39,475
je vais avoir un ordre d'exploration de ces sous-arbres.

44
00:01:39,982 --> 00:01:41,473
Et donc ici, ce que j'ai fait,

45
00:01:41,573 --> 00:01:42,947
j'ai fait un parcours dans lequel

46
00:01:43,399 --> 00:01:45,391
je vais regarder,

47
00:01:47,240 --> 00:01:49,111
je vais explorer chacun

48
00:01:49,843 --> 00:01:52,036
des nœuds avec une certaine règle.

49
00:01:53,120 --> 00:01:55,165
Alors, pour faire ça,

50
00:01:55,998 --> 00:01:57,435
je vais me poser la question

51
00:01:58,370 --> 00:01:59,469
dans quel ordre

52
00:01:59,746 --> 00:02:01,480
je vais traiter les nœuds ?

53
00:02:01,580 --> 00:02:03,315
C'est-à-dire est-ce que je vais

54
00:02:03,764 --> 00:02:05,994
évaluer quelque chose quand je vais être

55
00:02:06,750 --> 00:02:08,929
sur le nœud la première fois que je le vois ?

56
00:02:09,234 --> 00:02:10,271
Est-ce que je vais

57
00:02:10,791 --> 00:02:13,362
évaluer quand je reviens à la fin

58
00:02:13,462 --> 00:02:16,468
ou est-ce que je regarde quand je suis au milieu ?

59
00:02:16,568 --> 00:02:18,916
Alors on peut voir là-dessus

60
00:02:19,450 --> 00:02:21,165
ici, je prends ce nœud-là,

61
00:02:24,234 --> 00:02:26,920
je peux le voir en descendant,

62
00:02:28,417 --> 00:02:29,783
je peux le voir

63
00:02:30,174 --> 00:02:31,145
ou le traiter

64
00:02:31,354 --> 00:02:32,717
en repassant par là,

65
00:02:33,171 --> 00:02:34,233
ou je peux le traiter

66
00:02:34,691 --> 00:02:37,198
en ayant fini d'explorer tous les sous-arbres

67
00:02:37,298 --> 00:02:38,815
et ça vous donne trois types de parcours

68
00:02:39,251 --> 00:02:40,997
qui sont des parcours intéressants

69
00:02:41,334 --> 00:02:43,887
parce qu'ils correspondront à différentes

70
00:02:44,321 --> 00:02:46,395
opérations qu'on a envie de mener sur l'arbre.

71
00:02:47,542 --> 00:02:50,139
Alors, le parcours en profondeur,

72
00:02:50,406 --> 00:02:52,731
va se dérouler de la façon suivante.

73
00:02:53,135 --> 00:02:55,115
Ici, je le décris de façon récursive,

74
00:02:55,215 --> 00:02:56,400
on peut l'avoir de façon itérative

75
00:02:56,500 --> 00:02:58,048
mais c'est, je trouve, plus élégant

76
00:02:58,148 --> 00:03:00,076
de le voir de façon récursive.

77
00:03:00,462 --> 00:03:02,317
On a une condition d'arrêt :

78
00:03:04,490 --> 00:03:05,959
est-ce que mon arbre est vide ?

79
00:03:06,059 --> 00:03:07,143
Si mon arbre est vide,

80
00:03:07,243 --> 00:03:08,306
et bien je ne vais rien faire.

81
00:03:08,766 --> 00:03:10,388
Si mon arbre n'est pas vide,

82
00:03:10,959 --> 00:03:12,946
dans ce cas-là, je vais avoir à faire

83
00:03:13,046 --> 00:03:14,659
mes deux explorations récursives,

84
00:03:14,759 --> 00:03:16,226
ça, c'est mes deux appels récursifs,

85
00:03:25,724 --> 00:03:27,705
et puis je vais regarder

86
00:03:27,805 --> 00:03:29,706
à quel moment je fais le traitement du nœud.

87
00:03:29,875 --> 00:03:31,628
Si je fais le traitement du nœud

88
00:03:31,728 --> 00:03:32,581
au tout début,

89
00:03:32,681 --> 00:03:35,472
c'est-à-dire dès que j'observe mon arbre,

90
00:03:35,572 --> 00:03:36,865
ça veut dire que

91
00:03:37,232 --> 00:03:39,520
je vais traiter la racine d'abord,

92
00:03:39,684 --> 00:03:41,999
et puis après, je vais explorer les fils.

93
00:03:43,307 --> 00:03:44,349
C'est ce que l'on appelle

94
00:03:44,584 --> 00:03:46,913
le parcours en profondeur préfixé.

95
00:03:47,519 --> 00:03:48,957
Alors en profondeur,

96
00:03:49,374 --> 00:03:50,988
parce que l'on va explorer

97
00:03:51,398 --> 00:03:54,145
les nœuds par niveau croissant,

98
00:03:54,245 --> 00:03:55,484
par profondeur croissante.

99
00:03:56,500 --> 00:03:57,560
Deuxième façon de faire,

100
00:03:57,660 --> 00:04:00,431
Alors, si je reprends cet arbre-là,

101
00:04:00,866 --> 00:04:03,269
l'ordre alphabétique correspond à l'ordre de traitement.

102
00:04:03,894 --> 00:04:05,766
Donc je traite d'abord A,

103
00:04:06,322 --> 00:04:07,293
puis B,

104
00:04:07,574 --> 00:04:08,679
puis C,

105
00:04:08,779 --> 00:04:09,694
puis D,

106
00:04:10,197 --> 00:04:11,152
puis E,

107
00:04:12,171 --> 00:04:13,142
je remonte,

108
00:04:13,242 --> 00:04:14,216
puis F,

109
00:04:14,615 --> 00:04:15,872
je remonte,

110
00:04:16,298 --> 00:04:19,123
puis G,

111
00:04:19,342 --> 00:04:20,648
puis H, et cætera.

112
00:04:21,000 --> 00:04:22,927
Donc je vais vraiment traiter

113
00:04:23,027 --> 00:04:24,183
dès que je vois un nœud

114
00:04:24,283 --> 00:04:25,084
je traite ce nœud.

115
00:04:25,184 --> 00:04:26,614
Par exemple, je l'imprime,

116
00:04:26,714 --> 00:04:28,603
par exemple, je le mets dans une autre structure.

117
00:04:29,236 --> 00:04:32,032
Ça, c'est ce qu'on appelle le parcours en profondeur préfixé.

118
00:04:32,452 --> 00:04:34,291
Maintenant, je peux

119
00:04:35,316 --> 00:04:37,058
aussi traiter au moment

120
00:04:37,158 --> 00:04:40,251
où j'ai traité le sous-arbre gauche.

121
00:04:41,603 --> 00:04:43,638
Donc j'ai exploré le sous-arbre gauche.

122
00:04:43,738 --> 00:04:45,309
Quand je suis ici,

123
00:04:45,854 --> 00:04:50,017
je ne vais pas traiter le nœud F,

124
00:04:50,316 --> 00:04:52,253
je vais descendre.

125
00:04:52,603 --> 00:04:54,878
Je ne vais pas traiter le nœud D parce qu'il a des fils.

126
00:04:55,227 --> 00:04:56,492
Je redescends.

127
00:04:57,540 --> 00:04:58,474
Je redescends.

128
00:04:58,574 --> 00:05:01,319
Ici, j'arrive effectivement

129
00:05:01,655 --> 00:05:04,340
à une feuille, donc je la traite,

130
00:05:05,738 --> 00:05:07,389
puisque l'arbre est vide.

131
00:05:07,631 --> 00:05:08,942
Et en fait je remonte.

132
00:05:09,042 --> 00:05:10,084
Et à ce moment-là,

133
00:05:10,602 --> 00:05:11,619
je traite B.

134
00:05:11,719 --> 00:05:12,920
Puis je traite C.

135
00:05:15,184 --> 00:05:16,076
Là, je remonte.

136
00:05:16,176 --> 00:05:17,008
Je traite D.

137
00:05:18,024 --> 00:05:20,112
Je redescends, je traite E.

138
00:05:20,748 --> 00:05:21,850
Et cætera.

139
00:05:22,548 --> 00:05:23,521
Dans ce cas-là,

140
00:05:23,621 --> 00:05:26,692
si vous traitez le nœud

141
00:05:26,792 --> 00:05:28,975
au moment où vous arrivez

142
00:05:29,520 --> 00:05:31,329
après avoir traité le sous-arbre gauche,

143
00:05:31,787 --> 00:05:34,308
vous avez ce qu'on appelle un parcours infixé.

144
00:05:34,561 --> 00:05:36,195
Évidemment, le dernier,

145
00:05:36,295 --> 00:05:37,534
c'est le parcours suffixé,

146
00:05:37,968 --> 00:05:39,850
où vous ne traitez les opérations

147
00:05:39,950 --> 00:05:40,946
que sur la fin

148
00:05:41,252 --> 00:05:43,095
c'est-à-dire quand vous avez exploré

149
00:05:43,195 --> 00:05:44,152
tout ce qu'il y avait en dessous.

150
00:05:44,610 --> 00:05:47,006
Quand par exemple, vous recherchez

151
00:05:47,452 --> 00:05:48,437
quelque chose,

152
00:05:48,740 --> 00:05:50,902
par exemple, dans une pièce, vous avez des armoires,

153
00:05:51,002 --> 00:05:52,220
vous avez des boîtes, et cætera,

154
00:05:52,502 --> 00:05:54,540
et bien, vous n'allez

155
00:05:54,640 --> 00:05:56,333
dire que la boîte a été fouillée

156
00:05:56,433 --> 00:05:58,608
que quand toutes les sous-boîtes

157
00:05:58,708 --> 00:06:00,266
ont été vérifiées.

158
00:06:00,731 --> 00:06:02,392
Et ceci de façon récursive donc.

159
00:06:02,492 --> 00:06:04,309
Par exemple, vous avez cette expression-là,

160
00:06:04,409 --> 00:06:06,752
à la fin, vous faites votre calcul.

161
00:06:07,109 --> 00:06:09,403
Quand par exemple, vous cherchez un objet,

162
00:06:09,717 --> 00:06:11,721
et que l'objet est stocké dans l'arbre,

163
00:06:11,821 --> 00:06:12,997
vous parcourez l'arbre

164
00:06:13,097 --> 00:06:15,806
et dès que vous avez un objet qui a une propriété,

165
00:06:15,906 --> 00:06:16,839
vous pouvez vous arrêter

166
00:06:17,221 --> 00:06:18,240
et dans ce cas-là,

167
00:06:18,340 --> 00:06:20,256
vous pouvez remonter l'information.

168
00:06:20,627 --> 00:06:21,913
Ou vous pouvez la traiter.

169
00:06:22,013 --> 00:06:23,780
Donc en fait, en fonction des situations

170
00:06:23,880 --> 00:06:24,773
dans lesquelles vous allez être,

171
00:06:24,873 --> 00:06:26,247
vous allez avoir à choisir

172
00:06:26,611 --> 00:06:29,507
le type de parcours de votre arbre que vous allez faire.

173
00:06:29,915 --> 00:06:32,619
Alors, si par exemple, vous faites

174
00:06:35,604 --> 00:06:37,903
une exploration d'un graphe

175
00:06:38,003 --> 00:06:40,007
avec rechercher un plus court chemin,

176
00:06:40,292 --> 00:06:41,394
ce que vous allez faire,

177
00:06:41,494 --> 00:06:42,331
vous allez regarder

178
00:06:42,646 --> 00:06:44,587
un parcours dans lequel vous allez

179
00:06:44,687 --> 00:06:46,448
traiter le nœud avant

180
00:06:46,548 --> 00:06:47,557
de traiter les sous-arbres,

181
00:06:47,657 --> 00:06:49,065
parce que si vous regardez un chemin

182
00:06:49,165 --> 00:06:50,484
qui passe par un nœud

183
00:06:50,584 --> 00:06:52,589
et que ce nœud a des fils,

184
00:06:52,689 --> 00:06:53,660
des sous-arbres,

185
00:06:53,822 --> 00:06:57,470
et bien, si votre chemin est déjà trop long,

186
00:06:57,570 --> 00:06:59,410
ce n'est pas la peine d'aller explorer les sous-arbres

187
00:06:59,510 --> 00:07:00,621
et donc typiquement, on ne va pas

188
00:07:00,721 --> 00:07:02,003
pousser l'exploration.

189
00:07:02,226 --> 00:07:05,067
Donc on va avoir des façons d'explorer

190
00:07:05,167 --> 00:07:06,393
en regardant d'abord le nœud

191
00:07:06,493 --> 00:07:08,022
avant de regarder la suite.

192
00:07:08,372 --> 00:07:09,890
À l'opposé, on verra,

193
00:07:09,990 --> 00:07:11,559
le parcours infixé correspond

194
00:07:11,659 --> 00:07:13,103
au tri par ABR,

195
00:07:13,203 --> 00:07:16,146
ça, c'est la façon d'utiliser

196
00:07:16,246 --> 00:07:17,896
les arbres binaires de recherche.

197
00:07:18,185 --> 00:07:19,937
Et le parcours suffixé,

198
00:07:20,037 --> 00:07:20,748
ce que je vous ai dit avant,

199
00:07:20,848 --> 00:07:22,549
par exemple, pour les expressions arithmétiques,

200
00:07:22,975 --> 00:07:24,858
on ne va évaluer l'expression

201
00:07:24,958 --> 00:07:25,962
que quand on aura évalué

202
00:07:26,238 --> 00:07:27,364
toutes les sous-expressions

203
00:07:27,464 --> 00:07:29,662
et on aura la valeur de toutes les sous-expressions

204
00:07:29,989 --> 00:07:31,281
avant de calculer le résultat.

205
00:07:32,261 --> 00:07:34,824
Si je regarde maintenant mon arbre,

206
00:07:34,924 --> 00:07:37,650
là, j'ai essayé de partir en profondeur d'abord.

207
00:07:37,750 --> 00:07:39,732
C'est-à-dire tant que j'ai des fils,

208
00:07:40,056 --> 00:07:40,782
je descends.

209
00:07:41,127 --> 00:07:42,893
On pourrait très bien dire

210
00:07:43,229 --> 00:07:45,145
je vais parcourir mon arbre

211
00:07:45,245 --> 00:07:47,347
et le parcourir en largeur d'abord,

212
00:07:47,447 --> 00:07:49,144
c'est-à-dire que je vais l'explorer

213
00:07:49,457 --> 00:07:50,497
niveau par niveau.

214
00:07:51,137 --> 00:07:53,819
Donc ici, ça consisterait à faire la chose suivante.

215
00:07:55,294 --> 00:07:56,461
Je pars de A.

216
00:07:59,072 --> 00:08:00,726
Et je regarde

217
00:08:02,768 --> 00:08:04,550
les différents nœuds

218
00:08:05,683 --> 00:08:07,837
au fur et à mesure que je les rencontre.

219
00:08:08,843 --> 00:08:10,826
 Donc c'est vraiment du balayage

220
00:08:11,648 --> 00:08:12,644
ligne par ligne

221
00:08:13,120 --> 00:08:14,940
de mon arbre.

222
00:08:15,512 --> 00:08:16,902
Ce type de parcours

223
00:08:17,002 --> 00:08:18,848
s'appelle parcours en largeur d'abord.

224
00:08:19,461 --> 00:08:22,082
Alors attention, il n'est pas forcément récursif.

225
00:08:22,358 --> 00:08:25,117
Ce n'est pas forcément adapté

226
00:08:25,217 --> 00:08:27,654
d'avoir une vision récursive de ce parcours d'arbre

227
00:08:28,000 --> 00:08:29,173
parce que vous n'allez pas

228
00:08:29,982 --> 00:08:32,244
tirer parti de la structure récursive

229
00:08:32,917 --> 00:08:34,314
de vos données.

230
00:08:36,035 --> 00:08:37,706
On n'utilise pas la récursivité,

231
00:08:37,806 --> 00:08:39,159
donc qu'est-ce qu'on peut utiliser

232
00:08:39,471 --> 00:08:40,249
à la place ?

233
00:08:40,349 --> 00:08:42,487
Ou qu'est-ce qui est la structure sous-jacente

234
00:08:42,934 --> 00:08:44,577
pour traiter ce genre de chose ?

235
00:08:44,677 --> 00:08:46,799
Et bien, c'est ce qu'on appelle la file,

236
00:08:47,144 --> 00:08:48,420
dans laquelle on va dire

237
00:08:48,520 --> 00:08:49,750
je vais enfiler.

238
00:08:53,719 --> 00:08:55,646
Je vais avoir ma file,

239
00:08:57,197 --> 00:08:58,357
je vais enfiler

240
00:09:00,917 --> 00:09:03,262
l'élément A,

241
00:09:04,699 --> 00:09:05,821
puis je vais le traiter.

242
00:09:05,921 --> 00:09:06,755
Alors pour le traiter,

243
00:09:06,855 --> 00:09:09,036
je le traite, donc je l'enlève,

244
00:09:09,287 --> 00:09:11,878
et je vais enfiler les fils.

245
00:09:11,978 --> 00:09:14,281
Donc j'enfile B,

246
00:09:14,381 --> 00:09:16,328
j'enfile C.

247
00:09:16,744 --> 00:09:18,234
Je vais traiter

248
00:09:18,606 --> 00:09:19,375
le suivant,

249
00:09:19,475 --> 00:09:21,225
c'est-à-dire B,

250
00:09:22,292 --> 00:09:25,296
et en traitant B, je vois qu'il a deux sous-arbres,

251
00:09:25,481 --> 00:09:27,022
celui qui est étiqueté avec D,

252
00:09:27,122 --> 00:09:28,447
celui qui est étiqueté avec E.

253
00:09:28,547 --> 00:09:29,605
Donc je vais enfiler

254
00:09:30,258 --> 00:09:32,566
D et E.

255
00:09:33,296 --> 00:09:35,245
Je vais traiter C.

256
00:09:36,337 --> 00:09:41,727
Donc j'enfile F et G.

257
00:09:43,830 --> 00:09:44,714
Et cætera.

258
00:09:47,043 --> 00:09:49,470
Donc la bonne façon d'avoir

259
00:09:49,570 --> 00:09:51,079
un parcours d'arbre en largeur d'abord,

260
00:09:51,551 --> 00:09:53,951
c'est d'utiliser une structure auxiliaire

261
00:09:54,051 --> 00:09:56,651
qui est la structure de file.

262
00:09:56,751 --> 00:09:59,579
Ça, c'est exactement l'algorithme que je vous ai donné,

263
00:09:59,960 --> 00:10:02,802
sauf qu'il est écrit à l'aide

264
00:10:03,260 --> 00:10:04,062
d'une file

265
00:10:04,342 --> 00:10:06,207
et tant que la file n'est pas vide,

266
00:10:06,307 --> 00:10:07,741
et bien je vais prendre la tête,

267
00:10:08,041 --> 00:10:09,062
je vais défiler,

268
00:10:09,162 --> 00:10:13,015
et puis je vais traiter les sous-arbres gauche et droit.

269
00:10:15,210 --> 00:10:17,135
Et là vous voyez qu'on utilise

270
00:10:17,678 --> 00:10:21,664
le fait qu'on peut avoir des arbres vides

271
00:10:22,946 --> 00:10:25,418
qui sont des éléments.

272
00:10:26,507 --> 00:10:28,484
Alors on aurait pu utiliser

273
00:10:28,584 --> 00:10:30,270
le même genre d'outil

274
00:10:30,591 --> 00:10:32,103
pour faire le parcours de l'arbre

275
00:10:32,492 --> 00:10:34,259
en profondeur d'abord.

276
00:10:34,721 --> 00:10:36,550
C'est-à-dire utiliser une structure de pile

277
00:10:36,650 --> 00:10:37,930
et empiler au fur et à mesure.

278
00:10:38,565 --> 00:10:39,993
Ce qui est d'ailleurs une des façons

279
00:10:40,093 --> 00:10:42,037
qui est donnée pour,

280
00:10:43,157 --> 00:10:46,203
je dirais, décrire le parcours en profondeur d'abord

281
00:10:46,469 --> 00:10:48,506
de façon assez élémentaire.

282
00:10:48,976 --> 00:10:50,996
J'ai préféré avoir une présentation

283
00:10:51,295 --> 00:10:54,841
sous la forme de parcours récursif

284
00:10:55,124 --> 00:10:57,179
pour la simple raison que

285
00:10:57,479 --> 00:10:59,208
vous avez une structure de données

286
00:10:59,308 --> 00:11:01,251
qui est définie de façon récursive

287
00:11:01,573 --> 00:11:04,355
et donc on tire parti de cette définition récursive

288
00:11:04,455 --> 00:11:07,565
pour pouvoir l'utiliser et faire des parcours récursifs.

289
00:11:07,838 --> 00:11:11,039
C'est vraiment l'idée derrière

290
00:11:11,139 --> 00:11:16,255
de faire un parcours le plus simple possible

291
00:11:16,355 --> 00:11:17,626
et le plus adapté à la structure.

292
00:11:20,197 --> 00:11:22,240
Voilà. Donc ça, c'est pour les parcours,

293
00:11:22,464 --> 00:11:24,435
ils sont à maîtriser complètement.

