1
00:00:01,119 --> 00:00:03,075
Comme je le disais tout au début,

2
00:00:03,175 --> 00:00:05,423
un algorithme transforme de l'information.

3
00:00:05,523 --> 00:00:06,989
Il transforme de l'information,

4
00:00:07,089 --> 00:00:09,317
ça veut dire qu'il va considérer de l'information

5
00:00:09,417 --> 00:00:11,278
qui est en général structurée

6
00:00:11,378 --> 00:00:13,954
c'est-à-dire que vous avez des objets,

7
00:00:14,054 --> 00:00:17,183
vous avez des entiers, des réels,

8
00:00:17,283 --> 00:00:18,085
tout ce que vous voulez,

9
00:00:19,199 --> 00:00:21,713
et il va transformer cette information-là

10
00:00:21,813 --> 00:00:23,015
en une autre information.

11
00:00:24,847 --> 00:00:27,139
 Ce qui est important, c'est que le programme

12
00:00:27,239 --> 00:00:28,577
et la machine ne savent pas

13
00:00:28,677 --> 00:00:30,401
quelle est la nature de l'information

14
00:00:30,501 --> 00:00:31,589
et le contenu de l'information.

15
00:00:32,114 --> 00:00:36,134
C'est à la personne qui va coder le programme

16
00:00:36,234 --> 00:00:39,302
que revient la tâche

17
00:00:39,402 --> 00:00:41,091
de dire exactement

18
00:00:41,269 --> 00:00:43,090
quel est le sens que l'on peut donner

19
00:00:43,190 --> 00:00:43,909
à cette information,

20
00:00:44,009 --> 00:00:45,156
c'est-à-dire quelle interprétation

21
00:00:45,583 --> 00:00:47,237
on peut donner à cette information.

22
00:00:49,552 --> 00:00:50,921
Cette information-là

23
00:00:52,472 --> 00:00:54,037
possède donc une sémantique

24
00:00:54,588 --> 00:00:55,694
et cette sémantique,

25
00:00:55,983 --> 00:00:57,763
on va être capable de l'analyser

26
00:00:58,176 --> 00:00:58,993
et de regarder.

27
00:00:59,093 --> 00:01:00,537
Alors on a déjà des types simples

28
00:01:00,637 --> 00:01:02,702
qui sont implémentés dans un langage de programmation.

29
00:01:03,054 --> 00:01:04,870
Par exemple, vous avez les booléens

30
00:01:04,970 --> 00:01:06,803
avec des opérations qui correspondent,

31
00:01:07,214 --> 00:01:09,721
et, ou, et cætera,

32
00:01:09,821 --> 00:01:11,164
toutes les opérations classiques.

33
00:01:11,515 --> 00:01:13,175
Vous avez les types numériques.

34
00:01:13,275 --> 00:01:15,102
Vous avez les entiers.

35
00:01:15,202 --> 00:01:17,265
Vous avez les nombres réels

36
00:01:17,365 --> 00:01:18,408
qui sont représentés

37
00:01:18,895 --> 00:01:21,751
par des flottants, donc ce n'est pas tout à fait des nombres réels,

38
00:01:21,851 --> 00:01:25,244
c'est des nombres réels avec un certain nombre de contraintes.

39
00:01:25,567 --> 00:01:28,327
Vous avez éventuellement, dans certaines librairies Python,

40
00:01:28,427 --> 00:01:29,501
des nombres complexes.

41
00:01:29,795 --> 00:01:32,539
Et vous avez derrière toutes les opérations qui vont bien.

42
00:01:32,984 --> 00:01:37,520
On peut également avoir des types simples

43
00:01:37,620 --> 00:01:38,762
comme les chaînes de caractères

44
00:01:40,831 --> 00:01:42,714
et les opérations qui vont aller dessus,

45
00:01:42,814 --> 00:01:46,021
la concaténation, la suppression, l'extraction, et cætera.

46
00:01:46,760 --> 00:01:48,484
Et cætera, et cætera.

47
00:01:48,584 --> 00:01:50,201
Donc dans tous les langages de programmation

48
00:01:50,301 --> 00:01:51,991
et en algorithmique de façon générale,

49
00:01:52,287 --> 00:01:53,777
on a un certain nombre de types simples

50
00:01:53,877 --> 00:01:57,434
qu'on est capable de manipuler.

51
00:01:58,426 --> 00:02:01,860
Alors, si je vais un peu plus loin,

52
00:02:01,960 --> 00:02:03,027
pour synthétiser,

53
00:02:03,351 --> 00:02:04,757
qu'est-ce que c'est qu'un type simple ?

54
00:02:04,857 --> 00:02:06,097
En fait, c'est un élément,

55
00:02:06,197 --> 00:02:08,003
alors je me place du point de vue algorithmique,

56
00:02:08,103 --> 00:02:11,348
c'est un élément ayant une interprétation sans ambiguïté.

57
00:02:11,448 --> 00:02:12,432
Quand on parle d'un booléen,

58
00:02:12,532 --> 00:02:15,120
on sait que ça va prendre deux valeurs, vrai ou faux

59
00:02:15,220 --> 00:02:18,938
et on va l'interpréter comme un objet de logique.

60
00:02:19,363 --> 00:02:20,770
Et d'autre part,

61
00:02:20,870 --> 00:02:22,268
quand on a des types simples,

62
00:02:22,595 --> 00:02:24,670
en fait, c'est des types qui nous sont fournis

63
00:02:24,770 --> 00:02:25,929
par le langage de programmation

64
00:02:26,029 --> 00:02:27,900
qui est visé par l'algorithme.

65
00:02:29,358 --> 00:02:32,503
Alors, effectivement, travailler sur un élément,

66
00:02:32,603 --> 00:02:33,363
c'est intéressant

67
00:02:33,463 --> 00:02:35,656
mais ce qui va être plus intéressant,

68
00:02:35,756 --> 00:02:37,415
c'est de travailler sur une collection d'éléments.

69
00:02:39,008 --> 00:02:41,198
Quand on veut travailler sur une collection d'éléments,

70
00:02:41,680 --> 00:02:44,745
on va devoir organiser cette collection.

71
00:02:45,676 --> 00:02:48,061
Donc on va avoir besoin de constructeurs

72
00:02:48,161 --> 00:02:51,586
qui vont nous permettre d'insérer des éléments dans la collection

73
00:02:51,686 --> 00:02:54,875
ou de supprimer des éléments de la collection.

74
00:02:55,325 --> 00:02:57,267
Et puis on aura des méthodes d'accès

75
00:02:57,367 --> 00:02:58,850
aux éléments de la collection.

76
00:02:59,685 --> 00:03:01,588
Quels sont les opérateurs principaux

77
00:03:01,688 --> 00:03:03,780
qu'on va avoir sur une collection ?

78
00:03:03,880 --> 00:03:05,329
C'est la recherche d'un élément,

79
00:03:05,537 --> 00:03:06,749
le parcours,

80
00:03:06,849 --> 00:03:08,300
trouver tous les éléments de la collection,

81
00:03:08,669 --> 00:03:11,349
et puis des opérations de manipulation,

82
00:03:11,449 --> 00:03:12,393
de transformation.

83
00:03:12,493 --> 00:03:15,728
Ça, c'est si on a quelque chose de dynamique.

84
00:03:17,346 --> 00:03:20,563
Ça, c'est je dirais plutôt statique,

85
00:03:22,003 --> 00:03:24,516
et ici, c'est un aspect dynamique

86
00:03:26,271 --> 00:03:29,366
c'est-à-dire que là, on va être capable de dire

87
00:03:29,683 --> 00:03:32,548
ma collection va évoluer au cours du temps

88
00:03:32,648 --> 00:03:36,613
au fur et à mesure que je fais des opérations dessus,

89
00:03:36,713 --> 00:03:37,999
que j'insère des éléments dedans.

90
00:03:40,523 --> 00:03:42,276
Pour représenter ces collections,

91
00:03:42,376 --> 00:03:44,350
on a un certain nombre de façons de faire.

92
00:03:45,010 --> 00:03:47,909
On construit ce qu'on appelle des structures séquentielles.

93
00:03:48,479 --> 00:03:50,357
Alors la plus simple que vous connaissez tous,

94
00:03:50,457 --> 00:03:51,033
c'est le tableau.

95
00:03:51,249 --> 00:03:53,047
Et puis on peut aussi avoir des listes,

96
00:03:53,147 --> 00:03:54,089
des listes chaînées,

97
00:03:54,189 --> 00:03:56,390
et puis après, on peut aussi avoir

98
00:03:56,490 --> 00:03:58,932
des modes spécifiques de collections d'éléments

99
00:03:59,424 --> 00:04:01,361
en particulier pour les chaînes de caractères,

100
00:04:01,461 --> 00:04:04,419
une chaîne de caractères, c'est une collection de caractères

101
00:04:04,519 --> 00:04:06,145
qui est organisée d'une certaine manière.

102
00:04:07,710 --> 00:04:11,312
Il y a également des structures qu'on appelle non-séquentielles,

103
00:04:11,749 --> 00:04:16,695
c'est-à-dire qui vont avoir une notion différente

104
00:04:16,795 --> 00:04:18,640
que l'ordre que l'on met en place

105
00:04:18,740 --> 00:04:20,253
dans une structure séquentielle.

106
00:04:21,895 --> 00:04:23,889
Par exemple, le tableau associatif,

107
00:04:23,989 --> 00:04:25,200
donc c'est le dictionnaire.

108
00:04:25,300 --> 00:04:28,406
Ça, c'est une structure dans laquelle on accède directement à l'objet

109
00:04:28,506 --> 00:04:31,021
en fonction d'un paramètre de l'objet.

110
00:04:31,477 --> 00:04:33,924
Derrière, c'est des structures de tables de hachage.

111
00:04:34,273 --> 00:04:37,099
Vous avez des structurations sous forme d'arbres,

112
00:04:37,199 --> 00:04:38,595
d'arbres binaires en particulier.

113
00:04:38,949 --> 00:04:40,901
Et lorsqu'on va regarder les arbres,

114
00:04:41,001 --> 00:04:44,241
on va regarder en particulier les arbres binaires de recherche.

115
00:04:44,788 --> 00:04:46,686
On peut également structurer l'information

116
00:04:46,786 --> 00:04:48,021
sous forme de graphes,

117
00:04:48,445 --> 00:04:50,589
qui sont une généralisation des arbres,

118
00:04:50,930 --> 00:04:54,775
qui vont nous permettre je dirais de représenter

119
00:04:54,875 --> 00:04:58,609
des liens qui existent entre les objets de notre collection.

120
00:04:59,118 --> 00:05:00,850
Vous pourrez regarder par exemple

121
00:05:00,950 --> 00:05:02,838
la collection des villes de France

122
00:05:02,938 --> 00:05:04,064
qui sont connectées par des routes,

123
00:05:04,164 --> 00:05:05,804
c'est structuré,

124
00:05:05,904 --> 00:05:07,337
vous avez des moyens d'accès

125
00:05:07,437 --> 00:05:09,283
à toutes les villes avec un parcours du graphe,

126
00:05:09,625 --> 00:05:12,680
et vous avez des moyens de trouver des chemins à l'intérieur,

127
00:05:12,780 --> 00:05:14,136
donc des relations un petit peu plus compliquées.

128
00:05:16,151 --> 00:05:18,852
Ce travail sur les collections d'éléments

129
00:05:19,967 --> 00:05:22,645
va être en fait indépendant de l'implémentation.

130
00:05:22,995 --> 00:05:24,821
On va dire je travaille sur un ensemble,

131
00:05:26,689 --> 00:05:28,332
je ne vais pas me soucier

132
00:05:28,432 --> 00:05:30,378
de la façon dont c'est implémenté

133
00:05:30,478 --> 00:05:33,461
à l'intérieur de mon programme.

134
00:05:33,818 --> 00:05:36,485
Donc je vais m'affranchir

135
00:05:36,881 --> 00:05:39,833
de la façon dont les objets vont être codés

136
00:05:39,933 --> 00:05:42,096
à l'intérieur de mon programme.

137
00:05:42,655 --> 00:05:45,716
Néanmoins, ça, c'est la vue très abstraite

138
00:05:45,816 --> 00:05:47,091
de celui qui est au-dessus,

139
00:05:47,382 --> 00:05:48,715
néanmoins,

140
00:05:49,808 --> 00:05:51,673
quand je vais écrire un algorithme,

141
00:05:51,773 --> 00:05:53,052
j'ai quand même en tête

142
00:05:53,395 --> 00:05:55,271
le langage de programmation

143
00:05:55,371 --> 00:05:57,893
que je vais utiliser pour implémenter

144
00:05:58,069 --> 00:06:00,371
ces objets et ces structures.

145
00:06:00,756 --> 00:06:02,893
Donc en fait, je me base

146
00:06:02,993 --> 00:06:06,523
sur les types qui existent déjà dans le langage de programmation

147
00:06:06,917 --> 00:06:08,448
et sur les constructeurs de types

148
00:06:08,548 --> 00:06:11,065
qui sont à l'intérieur de mon langage de programmation.

149
00:06:11,446 --> 00:06:13,917
Ce qui est fait quand, par exemple, vous faites des objets,

150
00:06:14,017 --> 00:06:16,239
vous avez quand vous définissez un objet

151
00:06:16,339 --> 00:06:17,384
d'un point de vue algorithmie,

152
00:06:17,550 --> 00:06:19,597
par exemple, je prends un point du plan,

153
00:06:19,697 --> 00:06:20,653
je vais dire c'est un objet,

154
00:06:20,993 --> 00:06:23,003
j'ai dans la tête

155
00:06:23,103 --> 00:06:25,355
que je vais être capable de représenter cet objet

156
00:06:25,692 --> 00:06:27,110
dans mon langage de programmation

157
00:06:27,486 --> 00:06:29,209
par exemple, sous la forme d'un tuple

158
00:06:29,309 --> 00:06:31,045
avec deux coordonnées, x et y

159
00:06:31,145 --> 00:06:33,823
ou, dans une représentation polaire,

160
00:06:33,923 --> 00:06:36,723
avec un angle et avec une distance au centre.

161
00:06:39,129 --> 00:06:41,543
Les deux ne sont jamais découplés,

162
00:06:41,643 --> 00:06:42,612
c'est-à-dire qu'on ne travaille jamais

163
00:06:42,712 --> 00:06:43,992
sur un objet complètement abstrait.

164
00:06:44,300 --> 00:06:45,365
On a toujours dans la tête

165
00:06:45,465 --> 00:06:47,777
que l'algorithme doit être opérationnel

166
00:06:47,877 --> 00:06:49,448
et donc que l'on va devoir

167
00:06:50,602 --> 00:06:52,406
mettre en œuvre cet algorithme-là.

168
00:06:55,207 --> 00:06:58,155
Lorsqu'on va juste regarder une collection,

169
00:06:58,436 --> 00:07:00,278
et qu'elle est complètement statique,

170
00:07:00,378 --> 00:07:02,364
et qu'on ne va pas la modifier,

171
00:07:03,866 --> 00:07:06,253
et qu'on va avoir besoin d'accéder aux éléments,

172
00:07:06,692 --> 00:07:09,976
typiquement, on va utiliser

173
00:07:10,076 --> 00:07:11,424
je dirais la composition,

174
00:07:11,524 --> 00:07:15,798
donc les objets de type struct qu'on a par exemple en C,

175
00:07:15,898 --> 00:07:18,667
et les tuples qu'on a par exemple en Python.

176
00:07:19,823 --> 00:07:23,403
Je vais lier toutes les caractéristiques de ma collection entre elles,

177
00:07:23,503 --> 00:07:24,618
c'est figé,

178
00:07:24,999 --> 00:07:31,334
et j'ai un aspect atomique de la collection,

179
00:07:31,434 --> 00:07:32,560
c'est-à-dire que je ne peux pas

180
00:07:32,924 --> 00:07:35,730
toucher à un élément de la collection et le modifier.

181
00:07:36,092 --> 00:07:38,407
Donc ça, c'est l'objet tuple en Python.

182
00:07:38,748 --> 00:07:42,309
Et puis on a la structure linéaire qui est la plus simple,

183
00:07:42,915 --> 00:07:46,580
chaque élément est placé à une position dans la structure

184
00:07:46,680 --> 00:07:48,623
mais je vais pouvoir définir

185
00:07:48,723 --> 00:07:50,546
des éléments avant, des éléments après,

186
00:07:51,043 --> 00:07:52,381
je n'ai pas de hiérarchie

187
00:07:52,481 --> 00:07:54,239
et je vais pouvoir, éventuellement,

188
00:07:54,785 --> 00:07:57,169
travailler de façon dynamique sur cette structure.

189
00:07:57,491 --> 00:08:00,372
Je peux insérer et extraire des éléments.

190
00:08:00,818 --> 00:08:03,450
Je peux déterminer une position.

191
00:08:03,845 --> 00:08:04,613
Et cætera.

192
00:08:04,901 --> 00:08:06,866
Donc ça, c'est l'objet liste en Python.

193
00:08:06,966 --> 00:08:08,433
Le plus simple.

194
00:08:08,758 --> 00:08:11,740
Alors il est impératif de maîtriser cet objet-là

195
00:08:12,138 --> 00:08:14,613
dans tous les programmes de première et terminale.

196
00:08:15,146 --> 00:08:18,924
C'est vraiment une des bases

197
00:08:19,394 --> 00:08:21,600
de toute l'algorithmique et de la programmation

198
00:08:21,700 --> 00:08:22,783
de première et terminale.

199
00:08:23,908 --> 00:08:26,436
Alors pour implémenter ces objets,

200
00:08:26,709 --> 00:08:28,136
ces structures séquentielles,

201
00:08:28,393 --> 00:08:29,912
en fait, au niveau algorithmique,

202
00:08:30,012 --> 00:08:31,485
on va faire des petites distinctions

203
00:08:31,585 --> 00:08:34,470
selon la manière dont on va utiliser les objets

204
00:08:34,570 --> 00:08:36,346
qui sont les objets Python.

205
00:08:36,839 --> 00:08:38,407
Alors, le premier, c'est,

206
00:08:38,646 --> 00:08:41,212
voilà, j'ai donné un exemple,

207
00:08:41,312 --> 00:08:42,220
c'est le tableau d'éléments.

208
00:08:42,878 --> 00:08:44,580
Alors, le tableau d'éléments, c'est quoi ?

209
00:08:44,680 --> 00:08:46,813
C'est en fait une structure de données

210
00:08:46,913 --> 00:08:48,077
dans laquelle vous avez

211
00:08:48,792 --> 00:08:50,880
un certain nombre de cases contiguës

212
00:08:50,980 --> 00:08:52,416
que l'on représente de cette façon,

213
00:08:52,516 --> 00:08:55,383
ça ne veut pas dire que de façon interne dans votre machine,

214
00:08:55,954 --> 00:08:57,388
ça sera représenté comme ça,

215
00:08:57,747 --> 00:09:01,022
mais vous avez une taille globale qui est fixe,

216
00:09:01,361 --> 00:09:02,521
vous l'indicez.

217
00:09:02,827 --> 00:09:05,262
Alors là, on l'indice de 0 à n - 1,

218
00:09:06,007 --> 00:09:08,239
dans certains ouvrages d'algorithmique,

219
00:09:08,339 --> 00:09:09,422
on indice de 1 à n.

220
00:09:09,694 --> 00:09:12,399
Il suffit juste de se mettre d'accord sur la convention.

221
00:09:13,592 --> 00:09:15,038
Pour accéder à un élément,

222
00:09:15,138 --> 00:09:18,271
on y accède directement lorsqu'on connaît sa position.

223
00:09:19,016 --> 00:09:20,494
On peut parcourir le tableau,

224
00:09:20,594 --> 00:09:21,606
rechercher un élément,

225
00:09:21,706 --> 00:09:23,774
modifier une valeur, et cætera.

226
00:09:24,141 --> 00:09:28,204
Cette structure-là est fixe,

227
00:09:28,304 --> 00:09:30,284
c'est-à-dire la structure est figée,

228
00:09:30,664 --> 00:09:32,840
par contre, le contenu peut être modifié.

229
00:09:34,196 --> 00:09:36,004
Voilà, ça, c'est l'idée du tableau.

230
00:09:37,041 --> 00:09:38,371
Quand on travaille avec un tableau,

231
00:09:38,471 --> 00:09:40,960
en gros, on ne se pose pas la question

232
00:09:41,060 --> 00:09:43,139
de la taille du tableau

233
00:09:43,239 --> 00:09:44,833
et de faire évoluer sa taille.

234
00:09:45,662 --> 00:09:48,573
On peut améliorer ce tableau

235
00:09:48,673 --> 00:09:52,019
en disant, s'il était trié, est-ce que ce serait plus efficace ?

236
00:09:52,119 --> 00:09:53,560
Est-ce que ce serait plus intéressant ?

237
00:09:53,929 --> 00:09:54,861
Alors la réponse est oui,

238
00:09:54,961 --> 00:09:56,519
en particulier si vous cherchez

239
00:09:57,079 --> 00:09:59,314
un élément en fonction de sa valeur

240
00:09:59,414 --> 00:10:00,322
à l'intérieur du tableau.

241
00:10:01,127 --> 00:10:04,268
Par exemple, vous cherchez quel est l'indice,

242
00:10:05,341 --> 00:10:07,321
le repère, l'index dans la collection

243
00:10:07,421 --> 00:10:10,472
de l'élément, par exemple, 'c'.

244
00:10:10,832 --> 00:10:13,757
Si vous êtes sur le tableau d'éléments initial,

245
00:10:14,169 --> 00:10:16,408
ici, vous allez parcourir

246
00:10:16,909 --> 00:10:18,435
jusqu'à trouver 'c',

247
00:10:18,535 --> 00:10:20,862
et là, vous êtes super contents.

248
00:10:21,977 --> 00:10:23,428
Si vous avez un tableau trié,

249
00:10:23,782 --> 00:10:25,633
vous connaissez la musique.

250
00:10:26,175 --> 00:10:29,183
On va prendre le milieu,

251
00:10:29,283 --> 00:10:30,893
on va voir si l'élément qu'on recherche

252
00:10:30,993 --> 00:10:32,005
est à gauche ou à droite,

253
00:10:32,338 --> 00:10:33,487
et puis on divise

254
00:10:34,054 --> 00:10:35,689
et puis, ici,

255
00:10:36,102 --> 00:10:37,433
on l'a trouvé.

256
00:10:37,954 --> 00:10:41,661
Donc, ça, ce sont des outils

257
00:10:41,761 --> 00:10:43,670
dans lesquels on a pensé

258
00:10:44,039 --> 00:10:45,086
auparavant

259
00:10:45,186 --> 00:10:47,791
qu'il fallait faire la recherche

260
00:10:48,330 --> 00:10:50,908
et que c'était la recherche qui était importante

261
00:10:51,008 --> 00:10:54,386
donc on va rendre la structure efficace

262
00:10:54,692 --> 00:10:56,053
pour l'opérateur de recherche.

263
00:10:56,881 --> 00:10:59,369
Alors, un troisième type de structure

264
00:10:59,469 --> 00:11:01,295
peut également être utilisé,

265
00:11:01,395 --> 00:11:02,525
c'est ce qu'on appelle la liste.

266
00:11:03,015 --> 00:11:05,670
Alors, la liste, ce n'est pas au sens Python,

267
00:11:05,770 --> 00:11:09,057
ici, c'est la liste au sens algorithmique du terme,

268
00:11:09,565 --> 00:11:10,857
c'est-à-dire qu'on va avoir

269
00:11:10,957 --> 00:11:14,601
deux types d'objets dans une liste,

270
00:11:14,843 --> 00:11:16,874
on va avoir la liste en elle-même

271
00:11:16,974 --> 00:11:17,924
qui est ici

272
00:11:19,503 --> 00:11:21,603
et puis la notion de cellule

273
00:11:22,926 --> 00:11:24,248
qui est définie ici.

274
00:11:25,513 --> 00:11:26,884
Qu'est-ce que c'est qu'une cellule ?

275
00:11:26,984 --> 00:11:30,373
Une cellule, c'est tout simplement une valeur d'élément

276
00:11:30,764 --> 00:11:36,298
suivie, je dirais, d'un pointeur

277
00:11:37,361 --> 00:11:38,993
qui va vous permettre d'accéder

278
00:11:39,093 --> 00:11:40,565
à l'élément suivant dans la collection.

279
00:11:41,274 --> 00:11:42,746
Ça, c'est quelque chose

280
00:11:42,846 --> 00:11:44,507
de tout à fait utile en informatique.

281
00:11:45,374 --> 00:11:46,997
La représentation qui est ici,

282
00:11:47,097 --> 00:11:48,786
c'est une représentation abstraite

283
00:11:49,125 --> 00:11:51,105
ça ne veut pas dire que c'est implémenté

284
00:11:51,205 --> 00:11:52,213
à l'intérieur de votre programme

285
00:11:52,313 --> 00:11:53,165
de cette façon-là.

286
00:11:53,404 --> 00:11:56,175
Ici, c'est juste une façon de voir les choses.

287
00:11:56,588 --> 00:11:58,457
Si on avait envie de la représenter

288
00:11:58,557 --> 00:11:59,640
à l'intérieur d'une machine,

289
00:11:59,929 --> 00:12:01,137
on pourrait avoir par exemple

290
00:12:01,237 --> 00:12:02,217
un très grand tableau,

291
00:12:02,502 --> 00:12:05,078
dans lequel on a des éléments qui ont deux champs,

292
00:12:05,269 --> 00:12:07,575
un champ qui va être l'élément

293
00:12:07,675 --> 00:12:09,478
et un champ qui correspondra au suivant.

294
00:12:09,776 --> 00:12:12,411
Donc on peut faire un certain nombre d'opérations dessus,

295
00:12:12,789 --> 00:12:14,583
c'est ce qu'on appellera le chaînage,

296
00:12:14,942 --> 00:12:17,690
et là aussi, ce sont des outils qu'il faut connaître et maîtriser.

297
00:12:18,009 --> 00:12:20,348
Ce n'est pas tout à fait la liste en Python.

298
00:12:20,691 --> 00:12:22,373
Ici, si vous regardez bien,

299
00:12:22,473 --> 00:12:25,143
en fait, ça, ça serait une implémentation

300
00:12:25,383 --> 00:12:26,563
de la liste en Python.

301
00:12:27,202 --> 00:12:28,388
Alors pas tout à fait

302
00:12:28,488 --> 00:12:29,913
parce qu'elle est plus complexe

303
00:12:30,013 --> 00:12:32,418
mais ça représenterait une telle implémentation.

304
00:12:33,739 --> 00:12:37,052
On accède donc à l'élément suivant

305
00:12:37,152 --> 00:12:39,775
que par une fonction suivant sur l'élément.

306
00:12:40,146 --> 00:12:42,700
Ça, ça veut dire que vous aurez des coûts

307
00:12:42,800 --> 00:12:45,298
qui seront linéaires quand vous allez rechercher un élément.

308
00:12:45,659 --> 00:12:47,584
Vous allez parcourir votre liste

309
00:12:47,684 --> 00:12:49,095
de suivant en suivant.

310
00:12:50,337 --> 00:12:52,318
Par contre, c'est une structure dynamique

311
00:12:52,418 --> 00:12:54,189
dans laquelle vous allez pouvoir insérer,

312
00:12:54,289 --> 00:12:56,583
supprimer, en début, en fin,

313
00:12:56,683 --> 00:12:59,066
et puis après, dans une position donnée.

314
00:13:00,045 --> 00:13:01,770
Alors, si vous voulez insérer dans la liste,

315
00:13:01,870 --> 00:13:03,194
et bien on peut insérer au début,

316
00:13:03,647 --> 00:13:05,775
insérer en fin ou au milieu,

317
00:13:05,875 --> 00:13:06,842
par exemple, vous pouvez créer

318
00:13:06,942 --> 00:13:09,853
un nouvel élément ici.

319
00:13:12,311 --> 00:13:15,494
Vous allez casser ça,

320
00:13:15,710 --> 00:13:17,339
mettre un pointeur

321
00:13:17,439 --> 00:13:18,491
vers là,

322
00:13:19,261 --> 00:13:20,501
et un pointeur

323
00:13:22,389 --> 00:13:23,388
vers là.

324
00:13:24,798 --> 00:13:26,820
L'insertion ne pose aucun problème

325
00:13:28,031 --> 00:13:30,442
et la gestion de la mémoire est assurée

326
00:13:30,773 --> 00:13:32,116
par le support qui est derrière.

327
00:13:32,516 --> 00:13:35,693
Donc on peut aussi travailler sur des listes chaînées

328
00:13:36,015 --> 00:13:37,011
de cette façon-là.

329
00:13:37,373 --> 00:13:40,166
Ici, j'ai proposé une liste chaînée simple,

330
00:13:41,216 --> 00:13:43,451
où vous n'avez un chaînage que dans un sens,

331
00:13:43,736 --> 00:13:46,305
on peut imaginer des listes chaînées doubles

332
00:13:46,557 --> 00:13:48,477
c'est-à-dire avoir également

333
00:13:49,171 --> 00:13:51,050
une autre case ici

334
00:13:52,936 --> 00:13:55,048
dans laquelle on va vous dire

335
00:13:55,148 --> 00:13:56,127
vous pouvez aller là.

336
00:13:56,505 --> 00:13:57,643
Et cætera.

337
00:13:57,743 --> 00:13:59,563
Donc ça permet de retourner en arrière.

338
00:13:59,963 --> 00:14:01,972
C'est un outil extrêmement utile

339
00:14:02,400 --> 00:14:05,165
et qu'on utilise constamment en informatique,

340
00:14:05,265 --> 00:14:06,012
en système,

341
00:14:06,314 --> 00:14:08,628
en particulier, pour effectivement gérer

342
00:14:08,947 --> 00:14:12,425
la mémoire et des collections dynamiques d'objets.

