1
00:00:00,362 --> 00:00:01,966
À peu près tous les langages impératifs

2
00:00:02,066 --> 00:00:03,636
et tous les langages orientés objets

3
00:00:03,736 --> 00:00:05,393
contiennent, sous une forme ou une autre,

4
00:00:05,493 --> 00:00:06,816
la notion de fonction.

5
00:00:07,247 --> 00:00:08,793
Nous utilisons le terme fonction

6
00:00:08,893 --> 00:00:11,705
pour désigner toutes les constructions des langages

7
00:00:11,805 --> 00:00:13,356
permettant la découpe du code

8
00:00:13,456 --> 00:00:16,126
en blocs de code appelables dans différents contextes.

9
00:00:16,452 --> 00:00:18,144
Regardons cette notion d'un peu plus près.

10
00:00:18,519 --> 00:00:21,061
D'abord, ce concept se décline sous différentes formes.

11
00:00:21,161 --> 00:00:22,836
Cela peut être une sous-routine,

12
00:00:22,936 --> 00:00:24,608
encore appelée simplement routine,

13
00:00:24,708 --> 00:00:26,522
sous-programme ou procédure,

14
00:00:26,622 --> 00:00:28,238
qui est un bloc de code appelable

15
00:00:28,338 --> 00:00:30,355
avec retour à l'instruction qui suit l'appel,

16
00:00:30,455 --> 00:00:32,803
échangeant ses informations via paramètres

17
00:00:32,903 --> 00:00:33,889
et variables globales.

18
00:00:34,524 --> 00:00:36,651
Cela peut être une fonction stricto sensu,

19
00:00:36,751 --> 00:00:38,263
extension du cas précédent

20
00:00:38,363 --> 00:00:40,283
qui évalue en outre une valeur de retour

21
00:00:40,383 --> 00:00:42,231
renvoyée dans le contexte de l'appel

22
00:00:42,331 --> 00:00:44,038
nécessairement au sein d'une expression,

23
00:00:44,497 --> 00:00:46,427
une procédure étant parfois présentée

24
00:00:46,527 --> 00:00:47,720
comme une fonction à valeur vide

25
00:00:47,820 --> 00:00:49,109
ou sans valeur.

26
00:00:49,551 --> 00:00:52,002
Une méthode, simple fonction

27
00:00:52,102 --> 00:00:54,513
ou procédure définie au sein d'une classe

28
00:00:54,613 --> 00:00:56,189
et s'appliquant donc implicitement

29
00:00:56,289 --> 00:00:57,289
à un objet de cette classe,

30
00:00:57,389 --> 00:01:00,182
parfois à l'objet classe lui-même

31
00:01:00,282 --> 00:01:01,445
pour les méthodes statiques.

32
00:01:01,725 --> 00:01:02,725
Un opérateur,

33
00:01:02,853 --> 00:01:04,920
cas particulier de fonction ou de méthode,

34
00:01:05,020 --> 00:01:06,943
dont le nom n'est pas un identificateur

35
00:01:07,043 --> 00:01:08,732
mais un symbole, également appelé opérateur,

36
00:01:08,919 --> 00:01:10,883
et dont la syntaxe d'appel

37
00:01:10,983 --> 00:01:12,837
s'apparente à celle d'une expression algébrique.

38
00:01:13,242 --> 00:01:14,098
Une co-routine,

39
00:01:14,198 --> 00:01:15,371
forme généralisée de routine

40
00:01:15,471 --> 00:01:16,832
qui, lors d'un appel,

41
00:01:16,932 --> 00:01:18,389
reprend l'exécution de son code

42
00:01:18,489 --> 00:01:20,397
à l'endroit de retour précédent,

43
00:01:20,497 --> 00:01:22,585
plutôt appelé yield que return d'ailleurs,

44
00:01:22,685 --> 00:01:24,284
et doit par conséquent

45
00:01:24,384 --> 00:01:25,393
maintenir son état local

46
00:01:25,493 --> 00:01:26,581
d'un appel à l'autre.

47
00:01:26,897 --> 00:01:28,969
Un thread, ou fil d'exécution,

48
00:01:29,069 --> 00:01:31,979
routine qui s'exécute en parallèle du bloc appelant

49
00:01:32,079 --> 00:01:34,167
qui, lui, poursuit donc son exécution

50
00:01:34,267 --> 00:01:35,942
sans attendre le retour de la routine

51
00:01:36,042 --> 00:01:38,355
sauf au point explicite de synchronisation,

52
00:01:38,455 --> 00:01:40,414
mécanisme de rendez-vous et de sémaphore.

53
00:01:40,716 --> 00:01:43,270
Nous utilisons ici le terme générique de fonction

54
00:01:43,370 --> 00:01:45,288
pour désigner ces différents cas.

55
00:01:45,636 --> 00:01:47,500
Les notions d'en-tête,

56
00:01:47,600 --> 00:01:48,432
de corps,

57
00:01:48,532 --> 00:01:49,931
de paramètre formel,

58
00:01:50,031 --> 00:01:51,250
de paramètre effectif,

59
00:01:51,350 --> 00:01:52,526
d'arité, de signature,

60
00:01:52,626 --> 00:01:53,743
de prototype,

61
00:01:53,843 --> 00:01:55,190
de type d'une fonction,

62
00:01:55,290 --> 00:01:56,425
de surcharge,

63
00:01:56,525 --> 00:01:57,610
de récursivité,

64
00:01:57,710 --> 00:01:59,399
de fonction variadique,

65
00:01:59,499 --> 00:02:01,519
qui admet un nombre variable et indéterminé

66
00:02:01,619 --> 00:02:02,593
de paramètres effectifs,

67
00:02:02,839 --> 00:02:04,397
de mode de transmission,

68
00:02:04,497 --> 00:02:06,103
de valeur par défaut

69
00:02:06,203 --> 00:02:08,586
sont présentes pour parler de fonction.

70
00:02:09,636 --> 00:02:11,089
Je vous laisse le soin de regarder

71
00:02:11,189 --> 00:02:12,449
la richesse qu'offre Python

72
00:02:12,549 --> 00:02:14,451
pour définir des fonctions variadiques

73
00:02:14,551 --> 00:02:16,365
ainsi que les paramètres positionnels

74
00:02:16,465 --> 00:02:17,370
ou nommés

75
00:02:17,470 --> 00:02:18,687
et les valeurs par défaut.

76
00:02:19,017 --> 00:02:21,468
Notons les différents modes de transmission

77
00:02:21,568 --> 00:02:23,012
en fonction du langage utilisé.

78
00:02:23,305 --> 00:02:26,775
On parle de transmission par valeur,

79
00:02:27,497 --> 00:02:28,935
par référence,

80
00:02:29,279 --> 00:02:30,441
par copies,

81
00:02:30,541 --> 00:02:31,847
par partage,

82
00:02:31,947 --> 00:02:33,270
par résultat,

83
00:02:33,370 --> 00:02:34,264
par nom,

84
00:02:34,364 --> 00:02:35,377
à l'usage,

85
00:02:35,873 --> 00:02:36,888
comme constante.

86
00:02:37,521 --> 00:02:38,642
Je vous invite à regarder

87
00:02:38,742 --> 00:02:41,100
à quel mécanisme de passage de paramètres

88
00:02:41,200 --> 00:02:43,332
correspond chacun de ces modes de transmission.

89
00:02:43,637 --> 00:02:46,775
Il existe évidemment d'autres modes exotiques

90
00:02:46,875 --> 00:02:47,688
encore plus rares.

