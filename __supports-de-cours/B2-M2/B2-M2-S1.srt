1
00:00:05,595 --> 00:00:07,737
Vu le grand nombre de langages existants,

2
00:00:07,837 --> 00:00:09,608
une classification s'est fait jour.

3
00:00:09,885 --> 00:00:12,500
On regroupe en général les langages en familles

4
00:00:12,600 --> 00:00:14,712
soit selon le paradigme de programmation

5
00:00:14,812 --> 00:00:16,631
auquel ils sont le mieux adapté,

6
00:00:16,731 --> 00:00:19,274
nous verrons un peu plus loin la notion de paradigme,

7
00:00:19,374 --> 00:00:20,912
soit selon leur généalogie,

8
00:00:21,012 --> 00:00:23,358
en effet, la majorité des langages de programmation

9
00:00:23,458 --> 00:00:25,337
ont été crées comme évolutions, variantes

10
00:00:25,437 --> 00:00:27,740
d'un ou de plusieurs langages qui leur préexistaient.

11
00:00:27,895 --> 00:00:30,070
Ou encore selon différents critères,

12
00:00:30,361 --> 00:00:32,134
dont le plus utilisé est le fait que

13
00:00:32,234 --> 00:00:36,227
le langage est compilé, comme C, C++, Fortan, Cobol, Java, Ocaml,

14
00:00:36,327 --> 00:00:40,774
ou interprété, comme Python, Basic, Ruby, PHP, JavaScript.

15
00:00:41,106 --> 00:00:43,437
Nous parlerons de compilateur et d'interpréteur

16
00:00:43,537 --> 00:00:45,250
et de leur fonctionnement plus loin.

17
00:00:45,350 --> 00:00:46,886
Commençons par brièvement voir

18
00:00:46,986 --> 00:00:48,903
la notion de paradigme de programmation,

19
00:00:49,003 --> 00:00:51,810
qui est un concept assez difficile à définir précisément.

20
00:00:52,188 --> 00:00:54,368
Wikipedia nous dit que c'est une façon

21
00:00:54,468 --> 00:00:56,786
d'approcher la programmation informatique

22
00:00:56,886 --> 00:00:58,608
et de traiter les solutions aux problèmes

23
00:00:58,708 --> 00:01:01,972
et leur formulation dans un langage de programmation approprié.

24
00:01:02,822 --> 00:01:04,354
C'est assez flou comme définition.

25
00:01:04,884 --> 00:01:07,779
On parle donc de façon de formuler la solution

26
00:01:07,879 --> 00:01:09,055
à un problème par opposition

27
00:01:09,155 --> 00:01:10,533
à la méthode pour le résoudre.

28
00:01:10,633 --> 00:01:13,129
La notion de paradigme devient généralement plus claire

29
00:01:13,229 --> 00:01:16,167
grâce aux instances de langage de programmation qui l'utilisent.

30
00:01:16,543 --> 00:01:18,418
Deux grandes familles de langage de programmation

31
00:01:18,518 --> 00:01:20,097
occupent une place prépondérante

32
00:01:20,197 --> 00:01:21,191
dans ce paysage.

33
00:01:21,291 --> 00:01:22,682
Les langages impératifs

34
00:01:22,782 --> 00:01:24,821
dont sont issus les langages orientés objet

35
00:01:24,921 --> 00:01:27,668
même s'ils sont étudiés de façon spécifique,

36
00:01:28,196 --> 00:01:29,904
et les langages déclaratifs

37
00:01:30,004 --> 00:01:31,984
dont font partie les langages fonctionnels

38
00:01:32,084 --> 00:01:33,586
que nous aborderons brièvement.

39
00:01:34,009 --> 00:01:35,245
Dans un premier temps,

40
00:01:35,345 --> 00:01:37,479
nous regarderons les langages impératifs

41
00:01:37,579 --> 00:01:40,377
qui définissent le comportement de base

42
00:01:40,477 --> 00:01:44,175
de beaucoup de langages comme Python, C, C++, Fortran, Cobol, Java,

43
00:01:44,275 --> 00:01:45,790
même si certains d'entre eux

44
00:01:45,890 --> 00:01:47,549
incluent dans leur conception

45
00:01:48,051 --> 00:01:49,939
le paradigme orienté objet

46
00:01:50,039 --> 00:01:52,588
comme bien évidemment Python, C++ et Java.

47
00:01:52,910 --> 00:01:54,605
Dans ces langages,

48
00:01:54,705 --> 00:01:55,786
il est généralement admis

49
00:01:55,886 --> 00:01:57,455
que l'on travaille sur des modèles d'ordinateurs

50
00:01:57,555 --> 00:01:59,562
qui suivent l'architecture de Von Neumann.

51
00:01:59,931 --> 00:02:01,598
En bref, l'architecture de Von Neumann

52
00:02:01,698 --> 00:02:04,326
décompose l'ordinateur en quatre parties distinctes.

53
00:02:04,426 --> 00:02:07,289
L'unité arithmétique et logique,

54
00:02:07,389 --> 00:02:09,142
ou unité de traitement dont le rôle

55
00:02:09,242 --> 00:02:11,073
est d'effectuer des opérations de base,

56
00:02:11,173 --> 00:02:12,807
l'unité de contrôle

57
00:02:12,907 --> 00:02:14,928
chargée du séquençage des opérations,

58
00:02:15,028 --> 00:02:17,322
la mémoire qui contient à la fois les données

59
00:02:17,422 --> 00:02:20,128
et le programme qui indiquera à l'unité de contrôle

60
00:02:20,228 --> 00:02:22,151
quels sont les calculs à faire sur ces données,

61
00:02:22,251 --> 00:02:25,949
et le dispositif, ou les dispositifs, d'entrée/sortie

62
00:02:26,049 --> 00:02:27,831
qui permettent de communiquer avec le monde extérieur.

63
00:02:28,168 --> 00:02:30,276
Avant de parler plus des langages impératifs,

64
00:02:30,789 --> 00:02:32,660
nous allons d'abord brièvement nous arrêter

65
00:02:32,760 --> 00:02:35,648
sur la notion de compilateur et d'interpréteur.

