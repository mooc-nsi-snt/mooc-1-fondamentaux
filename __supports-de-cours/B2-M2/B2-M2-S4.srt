1
00:00:00,000 --> 00:00:01,651
Les langages de programmation

2
00:00:01,751 --> 00:00:03,641
sont, entre autres, caractérisés

3
00:00:03,741 --> 00:00:05,642
par le fait qu'ils soient interprétés

4
00:00:05,742 --> 00:00:06,766
ou compilés,

5
00:00:07,120 --> 00:00:08,243
le typage des données

6
00:00:08,343 --> 00:00:09,955
et la déclaration des variables,

7
00:00:10,055 --> 00:00:12,050
le runtime et la possibilité d'effectuer

8
00:00:12,150 --> 00:00:13,729
de la réflexion sur le code,

9
00:00:13,829 --> 00:00:15,871
et d'autres fonctionnalités avancées,

10
00:00:15,971 --> 00:00:17,900
comme l'utilisation de ramasse-miettes,

11
00:00:18,000 --> 00:00:19,275
garbage collector en anglais,

12
00:00:19,375 --> 00:00:21,259
la manipulation d'exceptions,

13
00:00:21,359 --> 00:00:23,762
d'événements, de threads, et cætera.

14
00:00:24,271 --> 00:00:27,901
Nous avons déjà parlé de langages compilés et interprétés,

15
00:00:28,216 --> 00:00:29,805
abordons brièvement

16
00:00:29,905 --> 00:00:31,019
les notions de typage.

17
00:00:31,316 --> 00:00:32,411
Un type de donnée

18
00:00:32,511 --> 00:00:34,649
est un ensemble de valeurs

19
00:00:34,749 --> 00:00:35,772
que peut prendre la donnée,

20
00:00:36,200 --> 00:00:38,271
un ensemble fini d'opérations

21
00:00:38,371 --> 00:00:39,823
qui peuvent lui être appliquées,

22
00:00:39,923 --> 00:00:41,887
et aussi un codage des valeurs

23
00:00:41,987 --> 00:00:42,943
sous forme binaire.

24
00:00:43,332 --> 00:00:44,877
Chaque langage offre

25
00:00:44,977 --> 00:00:47,825
un certain nombre de types élémentaires prédéfinis,

26
00:00:48,063 --> 00:00:49,474
à partir desquels

27
00:00:49,574 --> 00:00:52,906
l'on peut définir de nouveaux types composés ou dérivés.

28
00:00:53,006 --> 00:00:54,150
Selon le langage,

29
00:00:54,250 --> 00:00:56,156
divers types composés sont prédéfinis.

30
00:00:56,420 --> 00:00:58,672
Notons que pointeurs et références

31
00:00:58,772 --> 00:01:00,830
sont considérés comme des types composés.

32
00:01:01,170 --> 00:01:04,230
Pointeurs et références donnent des accès à une donnée,

33
00:01:04,330 --> 00:01:06,743
les pointeurs manipulent explicitement

34
00:01:06,843 --> 00:01:08,161
les adresses des données,

35
00:01:08,261 --> 00:01:10,510
avec les références, l'accès aux données

36
00:01:10,610 --> 00:01:12,794
ce qu'on appelle le déréférencement,

37
00:01:12,894 --> 00:01:13,791
est automatique.

38
00:01:14,207 --> 00:01:17,116
Les pointeurs définis par exemple en C ou en C++

39
00:01:17,216 --> 00:01:18,576
ne sont pas définis en Python.

40
00:01:18,977 --> 00:01:22,251
Les références sont définies ou utilisées en C++,

41
00:01:22,451 --> 00:01:24,142
en Java et en Python.

42
00:01:24,242 --> 00:01:25,256
Plus précisément,

43
00:01:25,356 --> 00:01:27,093
toute variable Python est un nom

44
00:01:27,193 --> 00:01:29,272
implémenté sous forme d'une référence

45
00:01:29,372 --> 00:01:30,897
qui donne accès à un objet.

46
00:01:31,585 --> 00:01:33,535
Les règles déterminant la manière dont

47
00:01:33,635 --> 00:01:35,832
les types sont attribués aux entités,

48
00:01:35,932 --> 00:01:37,979
variables, constantes, objets, fonctions,

49
00:01:38,079 --> 00:01:39,687
constituent un système de typage.

50
00:01:40,235 --> 00:01:43,251
Le langage réalise un typage statique ou dynamique.

51
00:01:43,351 --> 00:01:45,863
Si la détermination du type

52
00:01:45,963 --> 00:01:48,492
et la vérification de la faisabilité de l'opération

53
00:01:48,592 --> 00:01:50,218
sont effectuées dès la compilation,

54
00:01:50,318 --> 00:01:51,903
on parle de typage statique.

55
00:01:52,003 --> 00:01:53,783
Si elles sont faites lors de l'exécution,

56
00:01:53,883 --> 00:01:56,031
on parle de typage dynamique.

57
00:01:57,100 --> 00:01:59,832
Par ailleurs, le langage réalise un typage fort ou faible.

58
00:02:00,164 --> 00:02:03,463
Le sens de cette distinction n'est pas universellement fixé.

59
00:02:03,972 --> 00:02:07,080
La majorité des auteurs qualifie de typage fort

60
00:02:07,180 --> 00:02:08,882
un système suffisamment strict et riche

61
00:02:08,982 --> 00:02:10,885
pour procurer une vérification sûre

62
00:02:10,985 --> 00:02:12,815
de la cohérence du programme écrit

63
00:02:12,915 --> 00:02:15,874
et l'insertion non ambiguë de conversions efficaces

64
00:02:15,974 --> 00:02:17,728
ce qu'on appelle type-safety.

65
00:02:18,075 --> 00:02:20,949
Les langages comme Java, Pascal, Ada, Lisp

66
00:02:21,049 --> 00:02:22,793
et même Cobol sont fortement typés,

67
00:02:22,893 --> 00:02:25,495
par contre, Fortran et C ne le sont pas.

68
00:02:25,909 --> 00:02:27,067
Quant à Python,

69
00:02:27,774 --> 00:02:29,907
on pourrait dire qu'il est fortement typé

70
00:02:30,007 --> 00:02:32,270
mais la possibilité d'agir de manière imprévue

71
00:02:32,370 --> 00:02:33,674
sur le type d'un objet,

72
00:02:33,774 --> 00:02:36,303
une simple assignation à un nouvel attribut

73
00:02:36,403 --> 00:02:38,366
inexistant initialement,

74
00:02:39,065 --> 00:02:40,979
fait que ce langage

75
00:02:41,079 --> 00:02:44,931
ne peut pas être qualifié de complètement fortement typé.

76
00:02:45,256 --> 00:02:48,399
On parle aussi de typage explicite ou implicite.

77
00:02:48,754 --> 00:02:51,021
Avec un typage explicite,

78
00:02:51,121 --> 00:02:53,123
c'est à l'utilisateur d'indiquer lui-même

79
00:02:53,223 --> 00:02:54,459
les types qu'il utilise,

80
00:02:54,559 --> 00:02:57,028
par exemple lors des déclarations de variables

81
00:02:57,128 --> 00:02:57,835
ou de fonctions.

82
00:02:57,935 --> 00:02:58,819
Au contraire,

83
00:02:58,919 --> 00:03:01,185
avec un système de typage implicite,

84
00:03:01,285 --> 00:03:04,180
le développeur laisse au compilateur et au runtime

85
00:03:04,280 --> 00:03:06,030
le soin de déterminer tout seul

86
00:03:06,130 --> 00:03:07,252
les types des données

87
00:03:07,352 --> 00:03:09,828
utilisées par exemple par inférence.

88
00:03:09,928 --> 00:03:11,863
Python a donc un typage dynamique,

89
00:03:11,963 --> 00:03:13,340
fort et implicite

90
00:03:13,440 --> 00:03:15,004
puisque, par exemple,

91
00:03:15,104 --> 00:03:17,073
les variables ne sont pas déclarées.

