1
00:00:00,100 --> 00:00:02,439
Parlons maintenant des langages déclaratifs.

2
00:00:02,539 --> 00:00:04,372
Contrairement aux langages impératifs

3
00:00:04,472 --> 00:00:05,776
qui décrivent comment faire

4
00:00:05,876 --> 00:00:07,052
pour calculer un résultat,

5
00:00:07,152 --> 00:00:08,816
les langages déclaratifs décrivent

6
00:00:08,916 --> 00:00:10,340
ce qu'il faut calculer.

7
00:00:10,733 --> 00:00:12,065
Les langages déclaratifs

8
00:00:12,165 --> 00:00:13,951
ont donc une syntaxe très différente

9
00:00:14,051 --> 00:00:15,941
de celle des langages impératifs.

10
00:00:16,239 --> 00:00:18,946
Finis les boucles, les tests, les call-returns

11
00:00:19,046 --> 00:00:20,394
et vive les fonctions mathématiques,

12
00:00:20,494 --> 00:00:22,115
les variables formelles, et cætera.

13
00:00:22,507 --> 00:00:24,227
Cette grande et très ancienne famille

14
00:00:24,327 --> 00:00:27,204
puisque Lisp date de 1958

15
00:00:27,304 --> 00:00:29,617
peut être divisée en langages fonctionnels

16
00:00:29,717 --> 00:00:31,183
et en langages logiques.

17
00:00:31,283 --> 00:00:33,116
Les premiers langages déclaratifs,

18
00:00:33,216 --> 00:00:35,106
tant en nombre qu'historiquement,

19
00:00:35,206 --> 00:00:36,190
sont dits fonctionnels.

20
00:00:36,833 --> 00:00:39,489
Ils sont issus des travaux de John McCarthy

21
00:00:39,589 --> 00:00:41,629
qui, dès 1960,

22
00:00:41,729 --> 00:00:43,979
sur la base des premiers développements de Lisp,

23
00:00:44,079 --> 00:00:45,836
proposait une définition de langages

24
00:00:45,936 --> 00:00:48,205
fondée sur la notation du lambda calcul

25
00:00:48,305 --> 00:00:51,490
inventé vers 1930 par Alonzo Church.

26
00:00:52,262 --> 00:00:53,595
Un langage fonctionnel

27
00:00:53,695 --> 00:00:55,148
est un langage dans lequel

28
00:00:55,248 --> 00:00:56,268
un programme est décrit

29
00:00:56,368 --> 00:00:57,900
par composition de fonctions.

30
00:00:58,381 --> 00:01:00,405
Le langage Lisp a été dès le départ

31
00:01:00,505 --> 00:01:01,900
un sujet d'étude propre

32
00:01:02,000 --> 00:01:04,574
et une inspiration riche et fertile pour d'autres.

33
00:01:04,674 --> 00:01:06,096
Puis cette famille de langages

34
00:01:06,196 --> 00:01:07,987
est tombée partiellement en désuétude

35
00:01:08,087 --> 00:01:10,294
en partie pour des raisons d'efficacité

36
00:01:10,394 --> 00:01:11,652
d'exécution de ses programmes.

37
00:01:11,946 --> 00:01:15,151
Mais en 1978, John Backus

38
00:01:15,251 --> 00:01:17,488
pourtant connu comme co-créateur de Fortran,

39
00:01:17,588 --> 00:01:19,607
et pour ses travaux précurseurs

40
00:01:19,707 --> 00:01:22,808
sur la définition et la compilation des langages impératifs,

41
00:01:22,908 --> 00:01:24,229
publie une longue communication,

42
00:01:24,329 --> 00:01:28,013
Can programming be liberated from the Von Neumann style?

43
00:01:28,113 --> 00:01:31,619
A functional style and its algebra of programs,

44
00:01:31,719 --> 00:01:33,522
qui lui valut un prix Turing

45
00:01:33,622 --> 00:01:35,231
et relança véritablement l'intérêt

46
00:01:35,331 --> 00:01:36,737
pour cette famille de langages

47
00:01:36,837 --> 00:01:38,914
et le développement d'outils plus performants.

48
00:01:39,173 --> 00:01:41,077
Aujourd'hui, il existe effectivement

49
00:01:41,177 --> 00:01:42,787
de nombreux langages fonctionnels

50
00:01:42,887 --> 00:01:44,803
qui peuvent, dans certaines circonstances,

51
00:01:44,903 --> 00:01:46,601
rivaliser en efficacité

52
00:01:46,701 --> 00:01:48,070
avec les langages impératifs.

53
00:01:48,170 --> 00:01:51,456
Lisp a été développé initialement

54
00:01:51,556 --> 00:01:54,109
comme un simple outil de manipulation de listes généralisées,

55
00:01:54,209 --> 00:01:56,441
sa syntaxe y reste toujours attachée,

56
00:01:56,541 --> 00:01:58,135
tout y est une liste simple,

57
00:01:58,235 --> 00:01:59,691
d'atomes ou de sous-listes.

58
00:01:59,791 --> 00:02:01,623
Une brève explication de Lisp

59
00:02:01,723 --> 00:02:03,336
accompagnée de quelques exemples

60
00:02:03,436 --> 00:02:05,166
est disponible dans le support écrit.

61
00:02:06,289 --> 00:02:08,809
D'autres langages fonctionnels ont bien sûr été créés,

62
00:02:08,909 --> 00:02:11,288
notamment ML en 1973,

63
00:02:11,493 --> 00:02:14,525
un langage fortement et statiquement typé,

64
00:02:14,625 --> 00:02:17,122
surtout connu par son dialecte Caml,

65
00:02:17,222 --> 00:02:20,278
et son lointain descendant orienté objet, Ocaml,

66
00:02:20,378 --> 00:02:23,398
apparu en 1996.

67
00:02:23,675 --> 00:02:28,039
Mais également Scheme, Erlang, Haskell et cætera.

