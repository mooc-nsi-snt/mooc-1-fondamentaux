1
00:00:01,223 --> 00:00:03,679
Quand nous achetons un ordinateur ou un smartphone,

2
00:00:03,779 --> 00:00:06,099
ses caractéristiques techniques contiennent, entre autres,

3
00:00:06,199 --> 00:00:07,576
la quantité d'information

4
00:00:07,676 --> 00:00:09,254
que l'on peut stocker dans sa mémoire.

5
00:00:09,354 --> 00:00:11,272
Celle-ci est normalement exprimée

6
00:00:11,372 --> 00:00:12,416
en gigaoctets

7
00:00:12,516 --> 00:00:14,635
qu'on écrit Go.

8
00:00:15,174 --> 00:00:16,749
Le gigaoctet est donc une unité

9
00:00:16,849 --> 00:00:19,194
qui permet de parler de la quantité d'information.

10
00:00:19,294 --> 00:00:20,795
C'est le sujet de cette vidéo.

11
00:00:20,895 --> 00:00:23,126
Nous allons voir comment mesurer l'information

12
00:00:23,226 --> 00:00:25,234
quand elle est exprimée en binaire.

13
00:00:25,764 --> 00:00:27,970
Chaque chiffre, 0 ou 1,

14
00:00:28,070 --> 00:00:29,302
d'une représentation binaire

15
00:00:29,402 --> 00:00:30,659
est appelé un bit.

16
00:00:30,759 --> 00:00:33,070
Ce nom vient de l'anglais binary digit

17
00:00:33,170 --> 00:00:34,896
qui signifie chiffre binaire.

18
00:00:35,076 --> 00:00:36,865
Quand on a un nombre de 8 bits,

19
00:00:36,965 --> 00:00:38,463
on parle d'octet en français

20
00:00:38,563 --> 00:00:39,911
ou de byte en anglais.

21
00:00:40,031 --> 00:00:41,508
Voici un exemple

22
00:00:41,608 --> 00:00:43,150
où l'on a bien huit chiffres.

23
00:00:43,530 --> 00:00:45,579
Les symboles associés à ces unités

24
00:00:45,679 --> 00:00:47,676
sont le b minuscule pour le bit,

25
00:00:47,776 --> 00:00:49,521
le o minuscule pour l'octet

26
00:00:49,621 --> 00:00:51,568
et le B majuscule pour le byte.

27
00:00:52,045 --> 00:00:54,008
Naturellement, les quantités d'information

28
00:00:54,108 --> 00:00:55,778
que l'on manipule dans la vie de tous les jours

29
00:00:55,878 --> 00:00:57,117
sont beaucoup plus importantes.

30
00:00:57,217 --> 00:00:58,550
On va donc pouvoir parler

31
00:00:58,650 --> 00:00:59,799
de milliers d'octets,

32
00:00:59,899 --> 00:01:00,885
de millions d'octets

33
00:01:00,985 --> 00:01:01,661
et ainsi de suite.

34
00:01:02,227 --> 00:01:04,435
Pour ce faire, on trouve en informatique

35
00:01:04,535 --> 00:01:06,036
deux types de préfixe.

36
00:01:06,527 --> 00:01:08,620
On peut utiliser les préfixes habituels

37
00:01:08,720 --> 00:01:10,355
kilo, méga et ainsi de suite

38
00:01:10,455 --> 00:01:11,330
du système SI.

39
00:01:11,430 --> 00:01:13,753
Ainsi, un kilooctet s'écrit ko

40
00:01:13,853 --> 00:01:14,709
et vaut mille octets.

41
00:01:14,809 --> 00:01:17,315
Un mégaoctet s'écrit Mo

42
00:01:17,415 --> 00:01:18,950
et vaut un million d'octets.

43
00:01:19,050 --> 00:01:19,785
Et cætera.

44
00:01:20,178 --> 00:01:21,667
On peut aussi utiliser des préfixes

45
00:01:21,767 --> 00:01:23,109
propres à l'informatique

46
00:01:23,209 --> 00:01:24,736
qui sont basés sur des puissances de 2.

47
00:01:24,898 --> 00:01:26,695
On parle alors de kibioctets

48
00:01:26,795 --> 00:01:28,177
pour 2 exposant 10 octets

49
00:01:28,277 --> 00:01:29,937
ce qui se note kio.

50
00:01:30,037 --> 00:01:31,145
Un mébioctet

51
00:01:31,245 --> 00:01:32,762
vaut 2 exposant 20 octets

52
00:01:32,862 --> 00:01:35,791
ce qui se note Mio, et ainsi de suite.

53
00:01:36,157 --> 00:01:37,969
En pratique, ces deux familles d'unités

54
00:01:38,069 --> 00:01:39,007
sont suffisamment proches

55
00:01:39,107 --> 00:01:40,430
qu'on se permet souvent de les confondre.

56
00:01:40,530 --> 00:01:42,975
Néanmoins, des procès retentissants ont eu lieu

57
00:01:43,075 --> 00:01:44,270
au début des années 2000

58
00:01:44,370 --> 00:01:45,411
autour de cette confusion

59
00:01:45,511 --> 00:01:47,115
car des consommateurs pensaient avoir acheté

60
00:01:47,215 --> 00:01:49,088
un disque dur d'une capacité équivalente

61
00:01:49,188 --> 00:01:50,587
à 80 gibioctets

62
00:01:50,687 --> 00:01:52,114
alors qu'il en contenait en réalité

63
00:01:52,214 --> 00:01:54,021
que 80 gigaoctets.

