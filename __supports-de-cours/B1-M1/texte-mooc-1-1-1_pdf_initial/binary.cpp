#include <iostream>

int main() {
    int x ; // x est un entier signé, on utilise le complément à deux
    unsigned int y ; // y est un entier non négatif
    char z ; // z est un caractère
    
    // Toutes ces variables tiennent sur 32 bits
    
    // On place la même valeur de 32 bits dans les 3 variables
    // avec la bit de poids fort à 1
    // 1000 0000  0000 0000  0000 0000  0010 1111
    
    x = 0b10000000000000000000000000101111 ;
    y = 0b10000000000000000000000000101111 ;
    z = 0b10000000000000000000000000101111 ;

    // L'affichage des trois valeurs est différent
    // L'affichage tient compte du type: ce sont les
    // instructions qui donnent leur sémantique aux
    // valeurs représentées en binaire.
    
    std::cout << x << std::endl ;
   
    std::cout << y << std::endl ;
    
    std::cout << z << std::endl ;
    
}
