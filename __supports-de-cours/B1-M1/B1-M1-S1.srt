1
00:00:04,765 --> 00:00:07,706
Un ordinateur est une machine qui traite de l'information.

2
00:00:07,971 --> 00:00:09,502
Pour comprendre comment,

3
00:00:09,602 --> 00:00:10,719
nous devons d'abord expliquer

4
00:00:10,819 --> 00:00:12,866
comment l'information est représentée.

5
00:00:12,966 --> 00:00:13,603
En effet,

6
00:00:13,703 --> 00:00:15,222
il faut bien faire la différence

7
00:00:15,322 --> 00:00:17,782
entre l'information et sa représentation.

8
00:00:18,219 --> 00:00:20,180
Cela peut sembler étrange, comme question.

9
00:00:20,280 --> 00:00:22,555
Par exemple, si on considère la valeur 17

10
00:00:22,655 --> 00:00:24,786
tout le monde pensera à représenter cette valeur

11
00:00:24,886 --> 00:00:26,883
par les chiffres 1 et 7.

12
00:00:26,983 --> 00:00:29,630
Mais on peut imaginer plein d'autres possibilités,

13
00:00:29,730 --> 00:00:31,205
par exemple les chiffres romains,

14
00:00:31,305 --> 00:00:32,515
les marques de dénombrement

15
00:00:32,615 --> 00:00:34,077
ou encore, le binaire.

16
00:00:34,413 --> 00:00:36,234
Et on pourrait encore en imaginer d'autres.

17
00:00:36,776 --> 00:00:38,230
La représentation binaire

18
00:00:38,330 --> 00:00:40,869
est justement celle que nous allons utiliser.

19
00:00:41,128 --> 00:00:43,172
Mais qu'entend-on par binaire ?

20
00:00:43,449 --> 00:00:44,882
Cela signifie tout simplement

21
00:00:44,982 --> 00:00:46,219
que toute l'information

22
00:00:46,319 --> 00:00:47,532
est représentée à l'aide

23
00:00:47,632 --> 00:00:49,556
de deux symboles uniquement.

24
00:00:49,870 --> 00:00:51,537
Ceux-ci sont en général appelés

25
00:00:51,637 --> 00:00:53,306
0 et 1,

26
00:00:53,406 --> 00:00:54,768
mais cela pourrait être aussi

27
00:00:54,868 --> 00:00:56,300
Faux et Vrai.

28
00:00:56,733 --> 00:00:58,637
Pourquoi avoir adopté le binaire

29
00:00:58,737 --> 00:01:01,338
comme représentation universelle en informatique ?

30
00:01:01,438 --> 00:01:02,933
Pour des raisons technologiques,

31
00:01:03,033 --> 00:01:04,522
on peut facilement représenter

32
00:01:04,622 --> 00:01:05,606
deux valeurs différentes

33
00:01:05,706 --> 00:01:07,220
avec des voltages différents.

34
00:01:07,320 --> 00:01:10,328
Par exemple, entre 0 et 1,4 V,

35
00:01:10,428 --> 00:01:11,490
on aura un 0 

36
00:01:11,590 --> 00:01:13,498
et entre 2,4 et 5 V,

37
00:01:13,598 --> 00:01:14,703
on aura un 1.

38
00:01:14,878 --> 00:01:16,994
Ces voltages peuvent alors être manipulés

39
00:01:17,094 --> 00:01:18,979
par les composants de base de l'ordinateur

40
00:01:19,079 --> 00:01:20,934
que sont les transistors.

41
00:01:21,470 --> 00:01:23,060
Dans la suite de cette section,

42
00:01:23,160 --> 00:01:24,251
nous allons voir comment on peut

43
00:01:24,351 --> 00:01:25,589
représenter des nombres,

44
00:01:25,689 --> 00:01:26,573
du texte

45
00:01:26,673 --> 00:01:27,890
ou des images

46
00:01:27,990 --> 00:01:28,988
de façon binaire.

