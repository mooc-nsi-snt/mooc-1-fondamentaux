1
00:00:00,750 --> 00:00:02,180
Dans les vidéos précédentes,

2
00:00:02,280 --> 00:00:04,280
nous avons vu qu'on peut représenter un nombre

3
00:00:04,380 --> 00:00:05,130
en base 2.

4
00:00:05,523 --> 00:00:07,629
Si ce nombre est un nombre entier positif,

5
00:00:07,729 --> 00:00:09,723
nous obtenons une représentation binaire

6
00:00:09,823 --> 00:00:11,694
car on n'utilise que les symboles 0 et 1.

7
00:00:12,044 --> 00:00:13,869
Si le nombre est négatif par contre,

8
00:00:13,969 --> 00:00:16,183
il faut trouver un moyen de se défaire du signe -

9
00:00:16,283 --> 00:00:17,762
pour avoir une représentation binaire.

10
00:00:18,096 --> 00:00:19,284
Dans cette vidéo,

11
00:00:19,384 --> 00:00:21,736
nous allons étudier le principe du complément à 2

12
00:00:21,836 --> 00:00:24,375
qui nous servira à représenter des nombres négatifs.

13
00:00:24,661 --> 00:00:26,243
Voici le principe général

14
00:00:26,343 --> 00:00:27,335
pour les nombres entiers.

15
00:00:27,924 --> 00:00:29,910
Si nous avons un nombre positif,

16
00:00:30,010 --> 00:00:31,385
nous le représentons en base 2.

17
00:00:31,548 --> 00:00:33,705
Si nous avons un nombre négatif,

18
00:00:33,805 --> 00:00:36,176
nous commençons par calculer son opposé.

19
00:00:36,276 --> 00:00:38,037
On laisse donc tomber le signe -

20
00:00:38,137 --> 00:00:40,843
puis nous exprimons cet opposé en binaire

21
00:00:40,943 --> 00:00:43,073
puis nous calculons le complément à 2

22
00:00:43,173 --> 00:00:44,531
de cette représentation binaire.

23
00:00:45,325 --> 00:00:47,844
On voit donc que le calcul du complément à 2

24
00:00:47,944 --> 00:00:49,269
que nous allons bientôt expliquer

25
00:00:49,369 --> 00:00:51,544
remplace l'ajout d'un signe -

26
00:00:51,644 --> 00:00:53,175
pour indiquer que le nombre est négatif.

27
00:00:53,515 --> 00:00:55,052
Le complément à 2, quant à lui,

28
00:00:55,152 --> 00:00:56,535
est simple à calculer.

29
00:00:56,635 --> 00:00:59,204
On inverse tous les bits de la représentation binaire,

30
00:00:59,304 --> 00:01:01,020
on remplace donc les 0 par des 1

31
00:01:01,120 --> 00:01:02,300
et les 1 par des 0,

32
00:01:02,400 --> 00:01:03,972
et puis, on ajoute 1.

33
00:01:04,275 --> 00:01:06,691
Considérons maintenant un exemple.

34
00:01:06,791 --> 00:01:08,253
Supposons qu'on veuille représenter

35
00:01:08,353 --> 00:01:11,356
les nombres 42 et -42 sur 8 bits.

36
00:01:11,572 --> 00:01:13,198
Pour le nombre 42 qui est positif,

37
00:01:13,298 --> 00:01:16,031
on utilise la représentation en base 2 habituelle

38
00:01:16,131 --> 00:01:18,992
0010 1010.

39
00:01:19,287 --> 00:01:21,306
Pour -42 qui est négatif,

40
00:01:21,406 --> 00:01:23,947
on part de la représentation en base 2 de 42,

41
00:01:24,047 --> 00:01:25,692
on en inverse tous les bits,

42
00:01:25,792 --> 00:01:27,232
et on y ajoute 1.

43
00:01:27,332 --> 00:01:31,237
Et on obtient 1101 0110.

44
00:01:31,763 --> 00:01:32,848
Sur cet exemple,

45
00:01:32,948 --> 00:01:34,672
on peut voir une propriété importante

46
00:01:34,772 --> 00:01:35,706
de cette représentation.

47
00:01:35,937 --> 00:01:38,884
Le bit de poids fort vaut 0 quand le nombre est positif,

48
00:01:38,984 --> 00:01:40,357
et 1 quand il est négatif.

49
00:01:40,470 --> 00:01:43,539
On dit qu'il se comporte comme un bit de signe.

50
00:01:44,023 --> 00:01:46,764
Voyons maintenant un autre avantage de cette représentation.

51
00:01:46,900 --> 00:01:49,457
On peut continuer à effectuer des additions

52
00:01:49,557 --> 00:01:50,643
de manière habituelle

53
00:01:50,743 --> 00:01:52,376
et le résultat restera correct.

54
00:01:52,513 --> 00:01:55,266
Calculons par exemple 14 - 42,

55
00:01:55,366 --> 00:01:56,546
ce qui revient à faire la somme

56
00:01:56,646 --> 00:01:58,444
de 14 et de -42.

57
00:01:58,831 --> 00:02:00,786
On procède colonne par colonne comme d'habitude

58
00:02:00,886 --> 00:02:04,074
et on obtient 1110 0100.

59
00:02:04,457 --> 00:02:06,239
Comme le bit de poids fort est à 1,

60
00:02:06,339 --> 00:02:07,484
le résultat est négatif.

61
00:02:07,887 --> 00:02:09,496
Inversons maintenant le complément à 2

62
00:02:09,596 --> 00:02:10,718
pour savoir de quel nombre il s'agit.

63
00:02:10,818 --> 00:02:12,642
On commence par soustraire 1

64
00:02:12,742 --> 00:02:14,192
puis on inverse tous les bits

65
00:02:14,292 --> 00:02:17,471
et on obtient 0001 1100,

66
00:02:17,571 --> 00:02:18,747
qui est la représentation de 28.

67
00:02:19,124 --> 00:02:21,914
Le résultat est donc bien le complément à 2 de 28,

68
00:02:22,014 --> 00:02:23,641
c'est-à-dire -28.

69
00:02:24,159 --> 00:02:26,099
En utilisant le complément à 2

70
00:02:26,199 --> 00:02:27,466
pour les nombres négatifs,

71
00:02:27,566 --> 00:02:28,274
on a associé

72
00:02:28,374 --> 00:02:29,775
toute une série de représentations binaires

73
00:02:29,875 --> 00:02:30,796
à des nombres négatifs

74
00:02:31,000 --> 00:02:32,509
alors qu'on les utilisait avant

75
00:02:32,609 --> 00:02:33,902
pour des nombres positifs.

76
00:02:34,002 --> 00:02:34,849
Voyons cela en détail.

77
00:02:35,600 --> 00:02:37,232
Si on a des nombres sur 8 bits,

78
00:02:37,332 --> 00:02:38,344
on a 2 exposant 8

79
00:02:38,444 --> 00:02:40,114
ce qui fait 256 possibilités.

80
00:02:40,326 --> 00:02:42,360
Si on décide de ne manipuler que des nombres positifs,

81
00:02:42,460 --> 00:02:45,526
on représente donc tous les nombres de 0 à 255

82
00:02:45,626 --> 00:02:46,452
et on parle dans ce cas

83
00:02:46,552 --> 00:02:48,168
d'une représentation non signée.

84
00:02:48,268 --> 00:02:50,967
Avec une représentation signée en complément à 2,

85
00:02:51,067 --> 00:02:52,916
tous les nombres dont le bit de poids fort vaut 1

86
00:02:53,016 --> 00:02:54,225
sont maintenant des nombres négatifs.

87
00:02:54,325 --> 00:02:57,124
On peut donc représenter tous les nombres de 0 à 127

88
00:02:57,224 --> 00:02:59,842
mais aussi ceux de -128 à -1.

89
00:03:00,093 --> 00:03:02,370
Au total, on a toujours 256 valeurs différentes

90
00:03:02,470 --> 00:03:03,324
qui sont représentées.

91
00:03:03,723 --> 00:03:04,897
On peut alors se demander

92
00:03:04,997 --> 00:03:06,756
comment savoir si une représentation binaire

93
00:03:06,856 --> 00:03:07,916
qui commence par un 1

94
00:03:08,016 --> 00:03:10,337
doit être interprétée par un grand nombre positif

95
00:03:10,437 --> 00:03:12,108
ou comme un nombre négatif.

96
00:03:12,368 --> 00:03:13,948
Malheureusement, il n'y a rien

97
00:03:14,048 --> 00:03:15,134
dans la représentation binaire

98
00:03:15,234 --> 00:03:16,261
qui nous permette de le savoir.

99
00:03:16,361 --> 00:03:18,445
C'est le programme qui a la responsabilité

100
00:03:18,545 --> 00:03:20,114
d'interpréter les données correctement.

101
00:03:20,305 --> 00:03:22,156
Certains langages de programmation

102
00:03:22,256 --> 00:03:24,256
comme le C ou le Java

103
00:03:24,356 --> 00:03:25,741
ont d'ailleurs des types spéciaux

104
00:03:25,841 --> 00:03:27,657
pour indiquer si les données sont signées ou pas.

105
00:03:27,757 --> 00:03:29,758
En Python, c'est automatique.

