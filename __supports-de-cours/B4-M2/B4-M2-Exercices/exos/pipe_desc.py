#!/usr/bin/env python3
import os, sys

BUFSIZE = 10
BUFOUT = "Bonjour!"
BUFIN = ""

def main():
    # descripteurs pour lecture, écriture
    r, w = os.pipe()
    
    pid = os.fork()
    if pid==0:  # FILS
        os.close(w)
        # reçu en binaire
        BUFIN=os.read(r,BUFSIZE).decode("utf-8")
        print('Le fils lit : ',BUFIN )
        #rio.close()
        os.close(r)
        print('Le fils se termine')
        os._exit(0)
    else: # PERE
        os.close(r)
        print('Le père écrit...')
        # écrire en binaire
        os.write(w,BUFOUT.encode())
        os.close(w)
        os.waitpid(pid,0)
        print('Le père se termine')
        os._exit(0)
   
main()
