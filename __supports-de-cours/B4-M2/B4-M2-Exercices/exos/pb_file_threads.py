#!/usr/bin/env python3
import threading

# WRITER THREAD ---------------------------------------------
class WriterThread (threading.Thread):

    def __init__(self, nb, phrase):
        threading.Thread.__init__(self)
        self.nb = nb
        self.phrase = phrase
         
    def run(self):
        file = open("test.txt","w")
        file.write('%3d %15s\n' % (self.nb, self.phrase))

# READER THREAD ---------------------------------------------        
class ReaderThread (threading.Thread):

    def __init__(self, nb):
        threading.Thread.__init__(self)
        self.nb = nb
         
    def run(self):
        file = open("test.txt","r")
        print("Reader thread...")
        print(file.readline())            

# main function ---------------------------------------------
def main():
    # Create new threads
    thread1 = WriterThread(1, "Bonjour DIU!" )
    thread2 = WriterThread(2, "Il fait beau" )

    thread3 = ReaderThread(3)

    threads = []
    threads.append(thread1)
    threads.append(thread2)
    threads.append(thread3)

    # Start new Threads
    for t in threads:
        t.start()

    # Wait for the end of the threads    
    for t in threads:
        t.join()

    print("Exiting the main thread")


# MAIN -----------------------------------
print("Starting the main thread!!!")
main()
