#!/usr/bin/env python3
import threading
import time


class JauneDabordThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        
    def run(self):
        global jaune, bleu
        print("JauneDabord : je suis parti!")
        jaune.acquire()
        print("JauneDabord : JAUNE OK!")
        time.sleep(5)
        bleu.acquire()
        print("JauneDabord : BLEU OK!")
        print("JauneDabord : je fabrique du vert!!!")
        time.sleep()
        bleu.release()
        jaune.release()
        
class BleuDabordThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        
    def run(self):
        global jaune, bleu
        print("BleuDabord : je suis parti!")
        bleu.acquire()
        print("BleuDabord : BLEU OK!")
        time.sleep(5)
        jaune.acquire()
        print("BleuDabord : JAUNE OK!")
        print("BleuDabord : je fabrique du vert!!!")
        time.sleep()
        bleu.release()
        jaune.release()
     
# main function ------------------------------------
def main():
    global lx
    NB_THREADS = 10
    
    # Create new threads
    threads = []
    thread = JauneDabordThread()
    threads.append(thread)

    thread = BleuDabordThread()
    threads.append(thread)

    # Start new threads
    for t in threads:
        t.start()

    # Wait for threads
    for t in threads:
        t.join()
    
    print("Exiting the main thread with lx = \n", lx)

# MAIN -----------------------------------------
print("Starting the main thread!!!")

#two shared lock
jaune = threading.Lock()
bleu = threading.Lock()
    
main()
