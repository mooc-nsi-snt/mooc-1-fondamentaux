#!/usr/bin/env python3

import os
import time
import signal

def handler_sigint(sig,f):
    print("Je suis %d et j'ai reçu SIGINT" % os.getpid())
    os._exit(0)

def main():
    print('Je me lance et je suis', os.getpid())
    signal.signal(signal.SIGINT,handler_sigint)
    signal.pause()
    os._exit(0)

main()

