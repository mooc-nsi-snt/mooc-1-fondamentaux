#!/usr/bin/env python3

import os
import time
import signal

def main():
    print('[père] mon pid est ', os.getpid())
    gpid = os.getpgid(os.getpid())
    newpid = os.fork()
    if newpid == 0:
        print('[fils] mon pid est ', os.getpid())
        for i in range(10):
            time.sleep(1)
            print('[fils] itération ', i)
    else:
        print('[père] je tue mon fils', newpid)
        time.sleep(3)
        os.kill(newpid, signal.SIGKILL)
        res = os.waitpid(newpid, 0)
        status = res[1]
        if os.WIFEXITED(status):
            print(  '[père] mon fils', res[0],
                    'a terminé normalement avec valeur :', os.WEXITSTATUS(status))
        else:
            print('[père] mon fils', res[0], 'a termine anormalement')
        
    print('j\'ai terminé', os.getpid())
    os._exit(0)

main()

