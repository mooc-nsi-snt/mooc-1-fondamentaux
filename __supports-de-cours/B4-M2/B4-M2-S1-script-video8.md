# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 4.2.1.8 : Interface d'un système d'exploitation

**[00:00:00]**

Avant de plonger dans le vif du sujet et parler de processus, nous allons expliciter la notion d'interface d'un système d'exploitation. Nous allons expliciter ce qu'est l'interface système. Rappelons tout d'abord que le système d'exploitation est une couche logicielle qui nous permet de travailler facilement avec une machine. Ici, sur les schémas, nous trouvons la couche basse qui est le matériel qui compose notre machine, et sur la partie haute, nous avons des exemples d'applications que nous lançons sur cette machine. Ici nous avons un exemple de jeu, nous avons un environnement de programmation, des applications pour accéder aux réseaux sociaux, des éditeurs de texte, etc. Et donc, pour que ces différentes applications s'exécutent de manière correcte et optimale sur le matériel de nos machines, nous avons besoin de différents outils, mais surtout (donc) de cette couche qui est le système d'exploitation. 

Les trois fonctionnalités principales du système d'exploitation sont donc tout d’abord l'abstraction qui nous permet de ne pas nous préoccuper de quel type exactement de matériel nous disposons dans la machine. Le deuxième rôle d'un système d'exploitation est de permettre une utilisation efficace des ressources pour qu'on puisse effectuer le plus de traitements possible et exécuter le plus d'applications possible. Le troisième point, très important, c'est la protection, qui nous permet d'exécuter les applications comme elles sont censées et de ne pas corrompre les données des différentes applications et des usagers. 

Alors, le système d'exploitation fournit une interface qui consiste tout simplement en un ensemble de fonctions qui sont proposées aux usagers et aux applications. Donc c'est cette interface qui cache l'hétérogénéité du matériel et qui nous permet donc de travailler avec le matériel, même s’il peut y avoir différents types de processeurs, différents types de mémoire, différents types de dispositifs d'entrées/sorties, différents claviers, différents moniteurs…

**[00:02:20]**

Donc, le système d'exploitation nous permet de cacher tout ça et nous fournit un ensemble de fonctions qui sont plus accessibles et plus faciles à utiliser par les usagers et par les applications. Cette interface nous permet aussi d'avoir une vision logique de notre machine et au lieu de travailler avec les ressources matérielles qui sont donc ici le processeur, la mémoire, le clavier, moniteur, disque, qui sont les dispositifs d'entrée sortie standard, le système d'exploitation introduit des notions de ressources virtuelles et à la place, de processeurs, nous allons parler de processus. Donc, nous allons voir dans les séquences vidéo suivantes qu'un processus est tout simplement un programme qui s'exécute sur une machine. A la place de mémoire physique, nous parlons de mémoire virtuelle et de la mémoire d'un processus. A la place, donc des dispositifs d'entrée/sortie, nous avons, au niveau du système d'exploitation, la notion de flux d'entrées/sorties et enfin, quand nous parlons de disque, qui est le dispositif de stockage persistant, au niveau du système d'exploitation, nous travaillons avec des fichiers.

Que nous permet donc l'interface système? L'interface système nous permet de travailler et de gérer ces différentes ressources virtuelles. Chaque fonction de l'interface système est ainsi définie de manière très déterministe et claire. Elle est définie par son format, c'est à dire que nous avons le nom, le type des paramètres et le type de retour, et sa spécification qui est l'explication de ce qu’est censée faire la fonction.

**[00:04:15]**

Ici, nous avons une représentation schématique des différents types de fonctions que nous pouvons trouver dans un système d'exploitation. Nous allons donc avoir la gestion des fichiers, la gestion des processus, la gestion des entrées/sorties, la gestion de la protection et au niveau de l'interface nous allons avoir un ensemble de fonctions qui nous permettront de travailler avec ces différents aspects. 

Quand on parle d'interface système, en fait, elle se présente sous différentes formes. Nous pouvons avoir l'interface graphique, nous pouvons avoir l'interface en ligne de commande, où nous travaillons avec des terminaux, ou alors nous pouvons avoir l'interface programmatique, qui consiste en un ensemble de fonctions que nous pouvons utiliser en développant des applications. Ici vous voyez un extrait de la documentation que nous pouvons trouver sur n'importe quelle machine, ici je suis dans un terminal, et j'ai cherché de l'aide sur une fonction système qui a la fonction “fork” que nous allons voir plus tard dans les séquences suivantes. 

Voyons maintenant les différents types d'interface. 

L'interface graphique, comme son nom l'indique, est une interface qui nous permet d'interagir avec le système d'exploitation en utilisant l'interface homme-machine. Donc, c'est une interface qui est élevée, donc éloignée de tout ce qui est matériel et gestion bas niveau du matériel. Typiquement, nous interagissons en cliquant sur l'écran. Et quand on fait ça, c'est de la manière la plus intuitive et donc, l'utilisateur n'a pas besoin de savoir comment fonctionne exactement le système d'exploitation et ce qu'il fait exactement.

**[00:06:04]**

Le deuxième type d'interface, c'est l'interface en ligne de commande, où nous travaillons sur une fenêtre spéciale qui est le terminal, et quand on lance un terminal, nous interagissons avec un programme qui est lancé en premier, qui est l'interpréteur de commandes. Ou on utilise le mot anglais qui est le shell. Ainsi, quand nous sommes sur un terminal, quand nous tapons quelque chose, donc nous tapons une ligne de commande, c'est l'interprète des commandes qui va lire ce que nous avons tapé et qui va décider si c'est une commande qui peut être exécutée, et dans ce cas là, il va se charger à l'exécuter pour nous, ou alors nous avons tapé quelque chose qui n'a pas de sens et dans ce cas là, il va nous dire qu'il y a une erreur. Alors, il existe de nombreux interpréteurs de commandes. Ici donc moi  je travaille avec “tcsh”, mais il y a le bash, le csh, le ksh, etc. Et quand on travaille de cette manière là, nous travaillons avec les lignes de commande. 

Ici, je vous montre différents types de terminaux que nous trouvons de manière classique, (bafouillage) sur les différents systèmes d'exploitation. ici nous voyons à un terminal que nous trouvons sur un Linux Ubuntu, ici, il y a le terminal de base de Windows et ici, c'est le terminal que nous trouvons dans les Windows modernes qui ressemble donc, c’est le PowerShell, qui ressemble énormément à un terminal que nous trouvons sous Linux. Et donc quand on est sur un terminal, nous pouvons taper de nombreuses commandes.


**[00:07:52]**

Sous Windows typiquement, on peut faire dir pour voir quel est le contenu du répertoire. On peut changer de répertoire courant et de la même manière sous Linux avec les commandes correspondantes. 

Alors, le dernier type d'interface, l'interface programmatique ou alors appelée API, par de son appellation anglaise qui veut dire Application Programming Interface, est l'interface qui définit l'ensemble des fonctions que nous pouvons utiliser quand nous écrivons de code. Donc, quand nous écrivons du code et nous programmons des applications, nous pouvons, à travers certaines fonctions, faire appel direct au système d'exploitation. Donc, ici, vous avez un exemple, un extrait de fonctions fournies par le système pour le langage C et à gauche, vous avez l'exemple pour le langage Python. Ici, j'ai pris des fonctions que vous connaissez, qui sont liées à l'ouverture/ fermeture et de manière générale, au travail avec des fichiers. C'est les quatre premières fonctions. Après, nous avons des fonctions que nous allons voir plus tard, qui sont liées à l'utilisation des processus. Et les trois derniers ici sont des fonctions liées à la gestion de la mémoire en C, quelque chose que nous ne voyons pas dans le langage Python. 

Pour résumer, dans cette séquence, nous avons vu qu'un système d'exploitation est une couche logicielle qui nous permet de travailler avec les machines de manière facilitée et optimale. Le système d'exploitation nous fournit une interface pour ce faire et cette interface se présente sous différentes manières, donc graphique, en ligne de commande ou programmatique.

