# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 4.3.7.3 : Les protocoles SSH SSL

**[00:00:01]**

Nous présentons dans cette vidéo les protocoles standardisés SSH et SSL qui sont grandement utilisés dans le monde d'aujourd'hui et qui sont basés sur des méthodes de chiffrement.
SSH (Secure Shell) est un protocole réseau qui utilise le chiffrement pour sécuriser la connexion entre deux machines. Ainsi, il permet d'avoir des sessions interactives entre machines interconnectées, des transferts de fichiers et des exécutions de commandes à distance avec authentification forte et un chiffrement des données transmises.
Il est utilisé pour l'administration à distance du serveur, par exemple.
SSH fait également référence à l'ensemble d'outils implémentant ce protocole.
Le protocole fonctionne selon le modèle client serveur, alors le serveur écoute les connexions sur un port désigné.
Il est responsable de la négociation, de la connexion sécurisée, de l'authentification de la partie qui se connecte et du lancement de l'environnement correct si les informations d'identification sont acceptées. 
Le client, lui, est chargé d'initier la connexion avec le serveur, de négocier la connexion sécurisée, de vérifier que l'identité du serveur correspond aux informations précédemment enregistrées et de fournir les informations d'identification pour s'authentifier. 

Une session SSH est établie en deux étapes distinctes. La première consiste à convenir et à établir un chiffrement pour protéger les communications futures. L'algorithme asymétrique est RSA est couramment utilisé. La deuxième étape consiste à authentifier l'utilisateur et à découvrir si l'accès au serveur doit être accordé. 

Étape 1 Lorsqu'une connexion est initiée par un client, le serveur répond en indiquant les versions de protocole qu'il prend en charge. Si le client peut correspondre à l'une des versions de protocole acceptables, la connexion se poursuit.

**[00:02:06]**

Le client et le serveur s'accordent également sur le meilleur algorithme de chiffrement supporté par les deux qu'ils utiliseront pour la suite. En effet, SSH supporte plusieurs algorithmes de chiffrement symétrique tel que AES, Blowfish, 3DES, CAST128 ou Arcfour.
En étape 2, le serveur fournit également sa clé publique d'hôte, que le client peut utiliser pour vérifier s'il s'agit de l'hôte prévu. Le client stocke les clés publiques d'hôte auxquelles il s'est connecté auparavant dans le fichier known_host. 
Au point 3 les deux parties négocient la clé de session en utilisant une variante de l'algorithme Diffie-Hellman. Cet algorithme et ses variantes permettent à chaque partie de combiner ses propres données privées avec les données publiques de l'autre système pour obtenir une clé de session secrète identique. 
Le secret généré est une clé symétrique qui sera utilisée dans le chiffrement du reste de la communication. Ainsi, toute communication ultérieure sera enveloppée dans un tunnel chiffré qui ne peut être déchiffré par des personnes extérieures. 

Une fois le chiffrement de la session établi, l'étape d'authentification de l'utilisateur commence. Cela correspond à l'étape 4 sur la figure à droite. 
Il existe quelques méthodes différentes qui peuvent être utilisées pour l'authentification en fonction de ce que le serveur accepte. La plus simple est l'authentification par mot de passe, dans laquelle le serveur demande simplement au client le mot de passe du compte avec lequel il tente de se connecter. Le mot de passe est envoyé par le biais du chiffrement négocié, de telle sorte qu'il est protégé des parties extérieures. 

**[00:03:57]**

Même si le mot de passe est chiffré, cette méthode n'est généralement pas recommandée en raison des limites imposées à la complexité du mot de passe. Les scripts automatisés peuvent casser les mots de passe de longueur normale très facilement par rapport aux autres méthodes d'authentification. L'alternative la plus populaire et la plus recommandée est l'utilisation de paires de clés SSH. La procédure se déroule comme suit :
Le client commence par envoyer au serveur un id pour la paire de clés avec laquelle il souhaite s'authentifier. 
Le serveur vérifie l’id de la clé dans le fichier authorized_keys du compte auquel le client tente de se connecter. Si une clé publique avec l'id correspondant est trouvée dans le fichier, le serveur génère un nombre aléatoire et utilise la clé publique pour chiffrer le nombre. 
Le serveur envoie au client ce message chiffré. 
Si le client possède effectivement la clé privée associée, il pourra déchiffrer le message à l'aide de cette clé, révélant ainsi le nombre original. 
Le client combine le nombre déchiffré avec la clé de session partagée, qui est utilisée pour chiffrer la communication et calcule le hachage MD5 de cette valeur. 
Le client renvoie ensuite sur hachage MD5 au serveur comme réponse au message de nombre chiffré. 
Le serveur utilise la même clé de session partagée et le nombre original qu'il a envoyé au client pour calculer lui-même la valeur MD5. Il compare son propre calcul à celui que le client a renvoyé. Si ces deux valeurs correspondent, cela prouve que le client était en possession de la clé privée et le client est authentifié.

**[00:05:40]**

SSL pour Secure Socket Layer est un protocole à négociation ; On parle du Handshake SSL. Il a été développé à l'origine par Netscape en 1994 et il a pour but de sécuriser les transactions Internet par authentification du client, qui est un navigateur la plupart du temps et du serveur et par chiffrement de la session.
SSL à trois fonctions : 
L'authentification du serveur qui permet à un utilisateur d'avoir une confirmation de l'identité du serveur. Cela est fait par des méthodes de chiffrement à clé publique, qu’utilise SSL. Cette opération est importante, car le client doit pouvoir être certain de l'identité de son interlocuteur à qui il va par exemple envoyer son numéro de carte de crédit.
L'authentification du client, selon les mêmes modalités que pour le serveur. Il s'agit de s'assurer que le client est bien celui qu'il prétend être et enfin, 
Le chiffrement des données. Toutes les données qui transitent entre l'émetteur et le destinataire sont chiffrées par l'émetteur et déchiffré par le destinataire, ce qui permet de garantir la confidentialité des données ainsi que leur intégrité grâce à des mécanismes également mis en place dans ce sens. 

TLS pour Transport Layer Security Protocol a été développé par l’IETF et est la version 3.1 de SSL. 

SSL permet l'accès sécurisé à un site Web ou à certaines pages d'un site Web. Ces connexions se différencient des connexions normales, c'est-à-dire non sécurisées par le fait que l'adresse n'est plus un “http://”, mais en “https://”, où le “s” indique sécurisé, ce qui correspond à un cadenas fermé figurant à droite de la barre d'état du navigateur.

**[00:07:36]**

SSL se situe dans le modèle OSI, au sommet de la couche TCP/IP et en dessous de la couche d'application. Pour mettre en place une connexion SSL, Il faut d'abord établir une connexion TCP/IP, car SSL utilise certaines primitives de TCP/IP. Ainsi, SSL peut être vu comme un canal sûr au sein de TCP/IP, où tout le trafic entre deux applications “peer to peer” est échangé de manière chiffrée. Tous les appels de la couche d'applications à la couche TCP sont remplacés par des appels de l'application à SSL et c'est SSL qui se charge des communications avec TCP. 

Quelques implémentations de SSL et de TLS. On les retrouve dans le navigateur Web. La majeure partie des implémentations de SSL et TLS se trouvent dans les navigateurs et serveurs Web, mais on a aussi Open SSL, qui a été implémenté en C. C'est une boîte à outils de chiffrement comportant deux bibliothèques, une de cryptographie générale et une implémentant le protocole SSL, ainsi qu'une commande en ligne. Open SSL supporte SSL 2.0, SSL 3.0 et TLS 1.0. Il est distribué sous une licence de type Apache. 

Nous allons voir maintenant le fonctionnement de SSL. L'authentification de SSL s'appuie sur le chiffrement asymétrique par le biais de certificats à clé publique. Un certificat SSL est un fichier de données hébergé sur le serveur d'un site Web.

**[00:09:15]**

Les certificats SSL contiennent la clé publique et l'identité du site Web, ainsi que des informations connexes. Un client qui tente de communiquer avec le serveur d'origine fait référence à ce fichier pour obtenir la clé publique et vérifier l'identité du serveur. Pour qu'un certificat SSL soit valide, il doit être obtenu auprès d'une autorité de certification pour CA. Une autorité de certification est une organisation extérieure un tiers de confiance qui génère et distribue des certificats SSL. L'autorité de certification signe également le certificat numériquement avec sa propre clé privée, ce qui permet aux clients de le vérifier. La plupart des autorités de certification, mais pas toutes, facturent des frais pour l'émission d'un certificat SSL. 

Clients et serveurs commencent par s'authentifier mutuellement, puis négocient une clé symétrique de session qui servira à assurer la confidentialité des transactions. L'intégrité de ces dernières est assurée par l'application d'un algorithme de hachage HMAC pour Hashed Message Authentication Code. 

SSL se subdivise en quatre sous protocole, comme le montre la figure à droite. 
Le protocole de changement de spécification de chiffrement ou Cipher Change Spec Protocol permet de modifier l'algorithme de chiffrement en cours de communication de sorte à garantir la confidentialité des données transportées. 
Le protocole d'alerte permet d'envoyer des alertes accompagnées de leur importance. Ces alertes peuvent être un certificat inconnu, révoqué ou expiré. Par exemple, les alertes de haut niveau entraînent l'arrêt de la communication. 
Le protocole Handshake a pour objectif d'authentifier le serveur depuis le client, de négocier la version du protocole, de sélectionner les algorithmes de chiffrement, d'utiliser des techniques de chiffrement à clé publique pour générer et distribuer des clés secrètes et d'établir des connexions SSL chiffrées.

**[00:11:28]**

Enfin, le protocole SRP pour SSL Record Protocol est une encapsulation des protocoles situés juste au-dessus et il définit le format qui sera utilisé pour l'échange des données. 

En résumé, nous avons vu qu'en réseau, il existe des besoins clairs de sécurité qu'on a définis par la confidentialité des informations manipulées. L'authentification des parties, l'intégrité des données transmises et la non-répudiation. 
Nous avons vu que le chiffrement est un mécanisme qui utilise les algorithmes et principes mathématiques pour assurer la confidentialité et l'intégrité des données. Il existe deux types d'algorithmes de chiffrement : les symétriques comme des DES et AES et les asymétriques comme RSA. 
Nous avons vu que la clé de session permet de combler les limitations du chiffrement asymétrique en utilisant un chiffrement hybride plus rapide que le chiffrement asymétrique. Ensuite, nous avons vu deux protocoles standardisés et grandement utilisés dans le monde d'aujourd'hui, qui sont basés sur des méthodes de chiffrement. Le protocole SSH, qui permet de créer des sessions interactives sécurisées entre une machine cliente vers une machine serveur. Et il est notamment très utilisé pour la configuration à distance du serveur. Et le protocole SSL qui permet de sécuriser les transactions Internet par authentification du client qui est un navigateur la plupart du temps et du serveur et par chiffrement de la session. Il a donné naissance à HTTPS, qui représente HTTP et SSL.

