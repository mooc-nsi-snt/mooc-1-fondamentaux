# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 4.3.4.2 : Le protocole NAT

**[00:00:00]**

Après avoir vu le routage statique, nous allons voir un protocole particulier utilisé par les routeurs. Il s'agit du protocole NAT. 

Il faut savoir que toute entreprise et toute box internet utilisent le NAT. C'est un protocole qui permet de changer l'adresse IP source d'un paquet par une autre adresse IP. Cela permet de répondre à une problématique de pénurie d'adresses IP v4. En effet, avec l'accroissement exponentiel de l'utilisation d'Internet, les adresses IP v4, qui sont environ 4,3 milliards, soit 2 puissance 32, se retrouvent à court. Le protocole IP V6 est une solution sur le long terme pensé pour résoudre ce problème. Elle est en cours de déploiement. 
Cependant, NAT est une solution sur le court terme qui est actuellement implémentée. Ainsi, deux zones sont définies par le NAT, le réseau privé constitué par l'ensemble des équipements d'un même réseau qui accèdent à Internet et le réseau public qui est Internet. On distingue aussi, pour chaque classe d'adresses, une plage d'adresses privées qui seront utilisées pour l'adressage du réseau privé. Ainsi, on aura une plage d'adresses pour la classe A, pour la classe B et pour la classe C. Ces adresses peuvent être distribuées dans le réseau privé, sans validation par un tiers. Mais en contrepartie, elles ne peuvent être visibles depuis le réseau public. Le NAT va donc faire en sorte que tous les équipements présents dans le réseau privé utilisent une même adresse IP publique pour les échanges sur Internet.

**[00:01:40]**

Maintenant, voyons comment fonctionne le protocole NAT. On considère l'architecture suivante typique d'un réseau de particulier avec le sous réseau d'adresses privé 192.168.0.0/24 et un routeur connecté à Internet. Le routeur a une interface directement connectée au réseau public 200.1.1.0/24. 
On va dérouler ensemble les étapes 1 à 6 lorsque le PC d'adresse en veut échanger des paquets avec Internet. 
Donc, étape une. Le PC d'adresse 192.168.0.1 fait une requête depuis le sous réseau pour un serveur distant d'adresse 8.8.8.8.
La requête arrive au niveau de son routeur ou selon le protocole NAT, l'adresse IP source est remplacée par l'adresse de l'interface publique du routeur 200.1.1.254. 
Ainsi, à l'étape 3, le paquet arrive avec l'adresse du routeur mentionné comme adresse IP source. 
De même, la réponse aux paquets sera adressée à l'adresse publique du routeur, car il est la source connue dans le réseau public. 
À sa réception par le routeur, il fera le processus inverse en changeant son adresse par l'adresse du PC et le paquet sera ainsi transmis au PC qui a fait la requête. La question qui peut se poser de ce fait est comment le routeur fait pour assurer la correspondance entre les adresses privées du sous réseau et son adresse publique ? 
Il utilise une autre information pour distinguer les nœuds.

**[00:03:17]**

C'est la notion de port qui est propre à la couche 4 du modèle OSI, encore appelée la couche transport et qui sera présentée dans le chapitre 5. 

Ainsi, quelques inconvénients du NAT sont que chaque IP ne représente plus une machine unique avec le NAT. Les machines locales ne sont pas directement joignables depuis l'extérieur et la couche réseau n'est pas indépendante de la couche transport. Cela rompt le principe du modèle en couche où chaque couche a un rôle bien défini qui lui est spécifique et de ce fait, il est impossible de communiquer en IP via le NAT sans le protocole de la couche transport qui sera UDP ou TCP. 

En résumé, on a vu ce que c'est que le routeur, une machine possédant plusieurs interfaces et donc plusieurs cartes réseau et qu'un routeur se différencie d'une simple machine parce qu'il accepte des paquets qui ne lui sont pas destinés. 
La table de routage, qui est la structure de données utilisée à la fois par le routeur et les ordinateurs pour aiguiller des paquets ou moyens de passerelle. 
Le routage statique qui permet d'ajouter de façon manuelle des routes dans la table de routage à partir de commandes appropriées. 
Et le protocole NAT, qui permet de résoudre le problème de pénurie d'adresses IPv4 en remplaçant une adresse source privée par l'adresse publique au niveau du routeur.

