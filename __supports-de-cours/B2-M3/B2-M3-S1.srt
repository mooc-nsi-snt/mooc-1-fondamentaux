1
00:00:04,984 --> 00:00:06,566
Montrer qu'une version d'un programme

2
00:00:06,666 --> 00:00:07,961
est plus efficace qu'une autre

3
00:00:08,061 --> 00:00:09,378
n'est a priori pas aisé.

4
00:00:09,539 --> 00:00:12,325
La succession ou l'imbrication des tests et des boucles

5
00:00:12,425 --> 00:00:14,167
et la multitude des possibilités

6
00:00:14,567 --> 00:00:16,597
fait qu'il n'est généralement pas possible

7
00:00:16,697 --> 00:00:19,051
ou raisonnable d'évaluer l'efficacité de l'algorithme

8
00:00:19,151 --> 00:00:20,500
pour chaque cas possible.

9
00:00:20,997 --> 00:00:21,940
Généralement,

10
00:00:22,040 --> 00:00:23,502
il existe plusieurs méthodes

11
00:00:23,602 --> 00:00:24,953
pour résoudre un même problème.

12
00:00:25,053 --> 00:00:26,832
Il faut alors en choisir une

13
00:00:26,932 --> 00:00:29,526
et concevoir un algorithme pour cette méthode

14
00:00:29,626 --> 00:00:31,470
de telle sorte que cet algorithme

15
00:00:31,570 --> 00:00:34,299
satisfasse le mieux possible aux exigences.

16
00:00:34,399 --> 00:00:37,072
Parmi les critères de sélection d'une méthode

17
00:00:37,172 --> 00:00:38,755
et en conséquence d'un algorithme,

18
00:00:38,855 --> 00:00:40,394
deux critères prédominent.

19
00:00:40,494 --> 00:00:41,817
La simplicité

20
00:00:41,917 --> 00:00:43,822
et l'efficacité de cet algorithme.

21
00:00:43,922 --> 00:00:45,817
Si parfois ces critères vont de pair,

22
00:00:45,917 --> 00:00:48,224
bien souvent ils sont contradictoires.

23
00:00:48,324 --> 00:00:49,948
Un algorithme efficace

24
00:00:50,048 --> 00:00:52,500
est en effet bien souvent compliqué

25
00:00:52,600 --> 00:00:54,870
et fait appel à des méthodes fort élaborées.

26
00:00:54,970 --> 00:00:56,873
Le concepteur doit alors choisir

27
00:00:56,973 --> 00:00:59,313
entre la simplicité et l'efficacité.

28
00:00:59,513 --> 00:01:02,142
Si l'algorithme devra être mis en œuvre

29
00:01:02,442 --> 00:01:04,947
sur un nombre limité de données,

30
00:01:05,047 --> 00:01:07,510
qui ne demanderont qu'un nombre limité de calculs,

31
00:01:07,610 --> 00:01:10,392
cet algorithme doit de préférence être simple.

32
00:01:10,492 --> 00:01:12,480
En effet, un algorithme simple

33
00:01:12,580 --> 00:01:14,199
est plus facile à concevoir

34
00:01:14,299 --> 00:01:15,947
et a moins de chance d'être erroné

35
00:01:16,047 --> 00:01:18,666
que ne le sera un algorithme complexe.

36
00:01:18,766 --> 00:01:22,067
Si par contre, un algorithme est fréquemment exécuté

37
00:01:22,167 --> 00:01:23,842
pour une masse importante de données,

38
00:01:24,042 --> 00:01:26,475
il doit être le plus efficace possible.

39
00:01:26,575 --> 00:01:29,727
Au lieu de parler d'efficacité d'un algorithme,

40
00:01:29,827 --> 00:01:32,118
on parlera généralement de la notion opposée,

41
00:01:32,218 --> 00:01:35,117
à savoir de la complexité d'un algorithme.

42
00:01:35,303 --> 00:01:37,328
La complexité d'un algorithme

43
00:01:37,428 --> 00:01:38,496
ou d'un programme

44
00:01:38,596 --> 00:01:40,856
peut être mesurée de diverses façons.

45
00:01:40,956 --> 00:01:43,367
Généralement, le temps d'exécution

46
00:01:43,467 --> 00:01:45,041
est la mesure principale

47
00:01:45,141 --> 00:01:46,670
de la complexité d'un algorithme.

48
00:01:46,913 --> 00:01:48,619
D'autres mesures sont possibles,

49
00:01:48,719 --> 00:01:51,268
dont la quantité d'espace mémoire occupé

50
00:01:51,368 --> 00:01:54,216
par le programme, et en particulier par ses variables,

51
00:01:54,316 --> 00:01:56,456
la quantité d'espace disque nécessaire

52
00:01:56,556 --> 00:01:57,957
pour que le programme s'exécute,

53
00:01:58,057 --> 00:02:00,284
la quantité d'information qui doit être transférée

54
00:02:00,384 --> 00:02:01,763
par lecture ou écriture

55
00:02:01,863 --> 00:02:04,099
entre le programme et les disques

56
00:02:04,199 --> 00:02:07,273
ou entre le programme et des serveurs externes via un réseau,

57
00:02:07,373 --> 00:02:08,331
et cætera.

58
00:02:09,587 --> 00:02:11,800
Nous n'allons pas ici considérer le programme

59
00:02:11,900 --> 00:02:13,762
qui échange un grand nombre d'informations

60
00:02:13,862 --> 00:02:16,235
avec des disques ou avec un autre ordinateur ;

61
00:02:16,335 --> 00:02:18,195
en général, nous analyserons

62
00:02:18,295 --> 00:02:19,800
le temps d'exécution d'un programme

63
00:02:19,900 --> 00:02:21,580
pour évaluer sa complexité.

64
00:02:21,680 --> 00:02:23,350
La mesure de l'espace mémoire

65
00:02:23,450 --> 00:02:24,719
occupé par les variables

66
00:02:24,819 --> 00:02:26,123
sera un autre critère

67
00:02:26,223 --> 00:02:28,048
qui pourra être utilisé ici.

68
00:02:28,448 --> 00:02:30,191
Nous verrons dans ce qui suit

69
00:02:30,291 --> 00:02:32,543
que généralement, la complexité d'un algorithme

70
00:02:32,643 --> 00:02:34,781
est exprimée par une fonction

71
00:02:34,881 --> 00:02:37,519
qui dépend de la taille du problème à résoudre.

