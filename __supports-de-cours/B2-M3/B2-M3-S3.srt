1
00:00:00,300 --> 00:00:02,912
Essayons d'exprimer la complexité d'un algorithme

2
00:00:03,012 --> 00:00:04,478
par une fonction mathématique

3
00:00:04,578 --> 00:00:06,931
du nombre d'instructions élémentaires réalisées.

4
00:00:07,409 --> 00:00:09,610
Celui dépend de la taille du problème à résoudre.

5
00:00:09,710 --> 00:00:12,248
Le travail nécessaire pour qu'un programme s'exécute

6
00:00:12,348 --> 00:00:14,785
dépend de la taille du jeu de données fourni.

7
00:00:14,885 --> 00:00:17,396
Par exemple, si nous analysons le code

8
00:00:17,496 --> 00:00:19,254
qui effectue une recherche du minimum

9
00:00:19,354 --> 00:00:22,585
dans une liste Python s de petits entiers,

10
00:00:22,785 --> 00:00:24,605
il est intuitivement normal

11
00:00:24,705 --> 00:00:27,079
que cet algorithme prenne un temps proportionnel

12
00:00:27,179 --> 00:00:30,448
à len(s) et soit noté, par exemple,

13
00:00:30,548 --> 00:00:31,682
T(len(s)).

14
00:00:32,230 --> 00:00:35,070
Dénotons len(s) est égale à n.

15
00:00:35,170 --> 00:00:36,707
n est la taille de la liste

16
00:00:36,807 --> 00:00:38,323
et donc, par extension également,

17
00:00:38,423 --> 00:00:39,730
la taille du problème.

18
00:00:40,291 --> 00:00:43,290
Donnons ici la partie traitement de cet algorithme

19
00:00:43,390 --> 00:00:46,190
en ayant soin de transformer le for en while

20
00:00:46,290 --> 00:00:47,300
pour mieux détailler

21
00:00:47,400 --> 00:00:49,690
chaque étape d'exécution de l'algorithme

22
00:00:49,790 --> 00:00:53,359
et nous supposons que n est égal à len(s)

23
00:00:53,459 --> 00:00:54,816
connu dans le programme.

24
00:00:55,347 --> 00:00:58,115
Pour obtenir des résultats qui restent valables

25
00:00:58,215 --> 00:00:59,593
quel que soit l'ordinateur utilisé,

26
00:00:59,693 --> 00:01:01,919
il faut réaliser des calculs simples

27
00:01:02,019 --> 00:01:03,602
donnant une approximation

28
00:01:03,702 --> 00:01:05,366
dans une unité qui soit indépendante

29
00:01:05,466 --> 00:01:06,762
du processeur utilisé.

30
00:01:06,862 --> 00:01:09,659
On peut, par exemple, faire l'approximation

31
00:01:09,759 --> 00:01:11,956
que chaque assignation de donnée simple

32
00:01:12,056 --> 00:01:13,586
ou chaque test élémentaire

33
00:01:13,686 --> 00:01:16,367
représente une unité de temps d'exécution.

34
00:01:17,057 --> 00:01:18,800
D'autres hypothèses auraient pu être faites,

35
00:01:18,900 --> 00:01:20,070
par exemple, on aurait pu

36
00:01:20,170 --> 00:01:21,891
ne considérer que les assignations

37
00:01:21,991 --> 00:01:24,623
et rechercher la complexité en nombre d'assignations,

38
00:01:24,723 --> 00:01:26,403
ou ne considérer que les tests

39
00:01:26,503 --> 00:01:27,834
et rechercher la complexité

40
00:01:27,934 --> 00:01:30,261
en nombre de tests effectués par l'algorithme.

41
00:01:31,300 --> 00:01:33,524
La complexité dépend donc fortement

42
00:01:33,624 --> 00:01:34,610
de l'unité prise,

43
00:01:34,710 --> 00:01:36,564
qui doit être clairement précisée.

44
00:01:37,031 --> 00:01:38,414
Avec nos hypothèses,

45
00:01:38,821 --> 00:01:43,083
les instructions des lignes 1, 2 et 7

46
00:01:43,183 --> 00:01:45,410
prennent chacune une unité de temps d'exécution.

47
00:01:45,929 --> 00:01:48,951
La boucle while de la ligne 3 à 6

48
00:01:49,051 --> 00:01:50,847
s'exécute n - 1 fois

49
00:01:50,947 --> 00:01:53,938
mais le test s'effectue n fois.

50
00:01:54,453 --> 00:01:56,809
La ligne 3 prend n unités

51
00:01:56,909 --> 00:01:59,257
et la ligne 6, n - 1 unités.

52
00:01:59,800 --> 00:02:02,524
Le test de la ligne 4 prend une unité,

53
00:02:02,624 --> 00:02:04,346
il est effectué n - 1 fois

54
00:02:04,446 --> 00:02:08,663
et donc la ligne 4 utilisera au total n - 1 unités.

55
00:02:09,092 --> 00:02:12,902
L'assignation de la ligne 5 prend une unité,

56
00:02:13,002 --> 00:02:16,562
elle n'est effectuée que lorsque le test du if est vérifié.

57
00:02:16,662 --> 00:02:18,639
Si l'on ne désire pas uniquement

58
00:02:18,739 --> 00:02:20,664
faire une seule mesure

59
00:02:20,764 --> 00:02:22,406
sur un jeu de données bien précis,

60
00:02:22,506 --> 00:02:23,903
il faut donc expliciter

61
00:02:24,003 --> 00:02:25,914
si l'on désire connaître le temps d'exécution

62
00:02:26,014 --> 00:02:27,160
dans le meilleur des cas,

63
00:02:27,260 --> 00:02:28,216
Tmin(n),

64
00:02:28,316 --> 00:02:29,389
le pire des cas,

65
00:02:29,489 --> 00:02:30,428
Tmax(n)

66
00:02:30,528 --> 00:02:32,652
ou en moyenne, Tmoyen(n).

67
00:02:33,330 --> 00:02:35,111
Ici, Tmin(n) est donné

68
00:02:35,211 --> 00:02:37,560
dans le cas où le test du if n'est jamais vérifié,

69
00:02:37,660 --> 00:02:39,020
ce qui signifie que le minimum

70
00:02:39,120 --> 00:02:41,174
se trouve à la première composante,

71
00:02:41,274 --> 00:02:47,830
on a alors Tmin(n) = 1 + 1 + n + (n - 1)  + (n - 1) + 1,

72
00:02:47,930 --> 00:02:50,339
c'est-à-dire, est égal à 3n + 1.

73
00:02:50,824 --> 00:02:53,798
Tmax est donné dans le cas

74
00:02:53,898 --> 00:02:56,787
où le test du if est à chaque fois vérifié.

75
00:02:56,887 --> 00:02:58,190
Ce qui correspond au cas

76
00:02:58,290 --> 00:03:00,301
où toutes les valeurs de s sont distinctes

77
00:03:00,401 --> 00:03:02,378
et triées en ordre décroissant.

78
00:03:02,706 --> 00:03:10,405
On a alors Tmax(n) = 1 + 1 + n + (n - 1) + (n - 1) + (n - 1) + 1,

79
00:03:10,505 --> 00:03:12,307
c'est-à-dire, est égal à 4n.

80
00:03:12,548 --> 00:03:14,809
Le calcul de Tmoyen(n) est généralement

81
00:03:14,909 --> 00:03:16,417
beaucoup plus difficile à effectuer.

82
00:03:16,839 --> 00:03:19,175
Si nous faisons les hypothèses simplificatrices

83
00:03:19,275 --> 00:03:21,195
que toutes les valeurs de la liste sont différentes,

84
00:03:21,295 --> 00:03:23,024
et distribuées uniformément,

85
00:03:23,124 --> 00:03:25,983
nous obtenons que Tmoyen(n)

86
00:03:26,083 --> 00:03:31,117
est entre 3n + ln(n) et 3n + ln(n)  + 1.

87
00:03:31,667 --> 00:03:33,540
Tout cela fait beaucoup de calculs

88
00:03:33,640 --> 00:03:35,433
juste pour sept lignes de code.

89
00:03:35,962 --> 00:03:37,649
Essayons de trouver plus simple.

