1
00:00:05,168 --> 00:00:07,093
Après les types et les valeurs de base,

2
00:00:07,193 --> 00:00:09,440
nous nous intéressons maintenant aux types construits.

3
00:00:09,540 --> 00:00:11,936
Les types construits ont la particularité commune

4
00:00:12,036 --> 00:00:13,407
de nous permettre de manipuler

5
00:00:13,507 --> 00:00:15,051
plusieurs valeurs en une seule entité.

6
00:00:15,151 --> 00:00:17,633
Les tableaux, les p-uplets et les dictionnaires

7
00:00:17,733 --> 00:00:19,429
sont les trois types construits que nous allons voir.

8
00:00:19,529 --> 00:00:20,948
Et nous commençons par les tableaux.

9
00:00:21,234 --> 00:00:23,765
Le tableau est une collection finie et mutable

10
00:00:23,865 --> 00:00:26,451
d'éléments indexés par les entiers de 0 à n - 1

11
00:00:26,551 --> 00:00:27,819
si notre tableau n'est pas vide bien sûr

12
00:00:27,919 --> 00:00:28,844
et comporte n éléments.

13
00:00:29,281 --> 00:00:31,652
En général, on distingue deux sortes de tableau.

14
00:00:31,752 --> 00:00:32,787
Le tableau statique

15
00:00:32,887 --> 00:00:35,240
de taille fixe, et les éléments sont tous du même type.

16
00:00:35,340 --> 00:00:36,906
C'est ce qu'on rencontre par exemple

17
00:00:37,006 --> 00:00:38,932
avec les langages C, Java et Ocaml.

18
00:00:39,860 --> 00:00:42,459
Le tableau dynamique, lui, est de taille variable ;

19
00:00:42,559 --> 00:00:44,787
les éléments n'ont pas à être tous du même type.

20
00:00:45,947 --> 00:00:47,710
C'est ce que proposent Python et JavaScript.

21
00:00:48,698 --> 00:00:51,014
En Python, c'est l'objet list qui tient lieu de tableau.

22
00:00:51,114 --> 00:00:54,036
On pourra toutefois utiliser la list comme un tableau statique

23
00:00:54,136 --> 00:00:56,532
en s'interdisant d'ajouter et de retirer des éléments.

24
00:00:57,885 --> 00:01:00,027
Mais je vous propose de nous retrouver tout de suite

25
00:01:00,127 --> 00:01:01,615
dans un interprète interactif

26
00:01:01,715 --> 00:01:03,411
et de manipuler ensemble les tableaux.

27
00:01:03,830 --> 00:01:07,169
Nous nous retrouvons donc dans un interprète

28
00:01:07,269 --> 00:01:09,714
pour voir d'abord comment construire un tableau.

29
00:01:10,149 --> 00:01:12,811
La première méthode, donc en extension,

30
00:01:12,911 --> 00:01:14,796
en listant les éléments du tableau

31
00:01:14,896 --> 00:01:16,907
et en les séparant par des virgules,

32
00:01:17,007 --> 00:01:19,354
l'ensemble étant encadré par des crochets.

33
00:01:19,454 --> 00:01:21,333
Je vous rappelle donc que le tableau

34
00:01:21,433 --> 00:01:23,464
est un objet list de Python.

35
00:01:24,640 --> 00:01:27,687
Deuxième technique, la concaténation multiple.

36
00:01:27,787 --> 00:01:29,703
Donc ici, nous concaténons des éléments

37
00:01:29,803 --> 00:01:31,544
pour avoir un tableau de 100 zéros.

38
00:01:31,644 --> 00:01:34,606
Attention lors de l'utilisation de concaténations multiples

39
00:01:34,706 --> 00:01:37,727
d'avoir des éléments qui ne sont pas mutables.

40
00:01:39,494 --> 00:01:42,951
La compréhension de tableau

41
00:01:43,051 --> 00:01:44,383
ou la construction en compréhension

42
00:01:44,744 --> 00:01:48,944
qui s'appuie sur un itérateur,

43
00:01:49,044 --> 00:01:50,497
une expression génératrice,

44
00:01:50,597 --> 00:01:53,283
pour construire des éléments de notre tableau.

45
00:01:53,754 --> 00:01:54,894
Ici par exemple,

46
00:01:55,806 --> 00:01:58,468
les entiers qui correspondent aux codes ASCII

47
00:01:58,568 --> 00:02:00,856
des caractères majuscules de A à Z.

48
00:02:01,974 --> 00:02:04,355
Deuxième exemple de construction en compréhension,

49
00:02:04,455 --> 00:02:08,489
avec les carrés de 1 à 100.

50
00:02:09,889 --> 00:02:12,247
On peut aussi construire en partant du tableau vide,

51
00:02:12,347 --> 00:02:14,922
le tableau vide est la liste vide,

52
00:02:15,022 --> 00:02:16,347
crochet ouvrant crochet fermant.

53
00:02:16,722 --> 00:02:18,290
Ici, nous rajoutons,

54
00:02:18,390 --> 00:02:20,177
par la méthode append,

55
00:02:20,277 --> 00:02:21,804
des éléments à notre tableau.

56
00:02:21,904 --> 00:02:23,604
Les éléments rajoutés sont eux-même des tableaux

57
00:02:23,704 --> 00:02:25,219
construits, comme vous pouvez le voir,

58
00:02:25,319 --> 00:02:27,675
par concaténation multiple.

59
00:02:28,951 --> 00:02:32,217
Et nous avons notre tableau qui est construit

60
00:02:32,317 --> 00:02:35,698
au fur et à mesure du passage dans la boucle

61
00:02:35,798 --> 00:02:37,414
et de l'ajout d'élément.

62
00:02:37,917 --> 00:02:41,825
Enfin, dernière méthode, l'utilisation de la fonction list()

63
00:02:42,358 --> 00:02:44,207
sur un itérable.

64
00:02:44,307 --> 00:02:47,271
Ici, l'itérable est un objet range,

65
00:02:47,782 --> 00:02:52,211
pour créer le tableau comme précédemment,

66
00:02:52,311 --> 00:02:53,452
le tableau des codes ASCII

67
00:02:53,552 --> 00:02:55,354
des caractères de A à Z.

68
00:02:56,738 --> 00:03:00,041
Une deuxième utilisation de la fonction list()

69
00:03:00,141 --> 00:03:01,487
sur une chaîne de caractères

70
00:03:01,587 --> 00:03:03,855
pour obtenir le tableau constitué

71
00:03:03,955 --> 00:03:06,747
des caractères de cette chaîne.

72
00:03:08,802 --> 00:03:11,278
Nous avons vu comment construire un tableau,

73
00:03:11,378 --> 00:03:14,150
nous allons dans une prochaine vidéo

74
00:03:14,250 --> 00:03:15,565
manipuler ces tableaux

75
00:03:15,665 --> 00:03:17,286
et notamment accéder aux éléments.

76
00:03:17,898 --> 00:03:20,284
Je vous propose de manipuler un peu ensemble les tableaux.

77
00:03:20,824 --> 00:03:23,414
Donc donnons-nous un tableau,

78
00:03:23,906 --> 00:03:25,789
ici, un tableau de chaînes de caractères,

79
00:03:26,422 --> 00:03:29,063
et nous pouvons accéder aux éléments du tableau

80
00:03:29,163 --> 00:03:30,527
par l'opérateur crochet.

81
00:03:30,627 --> 00:03:33,157
Donc le nom du tableau, l'opérateur crochet

82
00:03:33,257 --> 00:03:35,206
auquel on passe un indice.

83
00:03:35,306 --> 00:03:36,404
Donc un indice positif,

84
00:03:36,504 --> 00:03:38,177
un entier positif,

85
00:03:38,277 --> 00:03:39,673
ici 0 pour le premier.

86
00:03:39,773 --> 00:03:43,722
Je rappelle que le premier élément est indicé par 0.

87
00:03:44,381 --> 00:03:46,328
On peut donner des indices négatifs,

88
00:03:46,988 --> 00:03:49,748
-1 correspond au dernier élément du tableau.

89
00:03:50,222 --> 00:03:53,807
Et attention aux erreurs d'index

90
00:03:53,907 --> 00:03:56,243
si on fournit un indice

91
00:03:56,343 --> 00:03:57,634
qui est trop grand ou trop petit

92
00:03:57,734 --> 00:03:59,326
par rapport à notre tableau.

93
00:04:00,232 --> 00:04:01,729
Concernant la modification du tableau,

94
00:04:01,829 --> 00:04:04,102
je vous propose de visualiser les choses

95
00:04:04,202 --> 00:04:05,825
avec l'outil PythonTutor.

96
00:04:06,197 --> 00:04:09,117
Donc ici, nous avons un tableau

97
00:04:12,711 --> 00:04:17,461
que nous allons créer, donc le tableau fruits,

98
00:04:17,561 --> 00:04:18,862
et nous voyons ici,

99
00:04:18,962 --> 00:04:20,564
là, ce qui est intéressant avec PythonTutor,

100
00:04:20,664 --> 00:04:23,125
c'est de bien voir les références,

101
00:04:23,225 --> 00:04:24,632
les différentes références,

102
00:04:24,732 --> 00:04:25,739
vers les objets.

103
00:04:25,839 --> 00:04:28,544
Les objets sont les trois chaînes de caractères

104
00:04:28,644 --> 00:04:32,071
et nous avons la référence fruits vers le tableau

105
00:04:32,171 --> 00:04:33,158
qui contient en fait lui-même

106
00:04:33,258 --> 00:04:34,491
des références vers les objets.

107
00:04:35,656 --> 00:04:38,386
Nous créons une variable pour simplement mémoriser

108
00:04:38,486 --> 00:04:41,985
une des références, un des éléments de notre tableau

109
00:04:42,473 --> 00:04:44,850
et nous allons voir comment on change

110
00:04:44,950 --> 00:04:47,570
la valeur de cette composante,

111
00:04:47,670 --> 00:04:48,724
de cet élément du tableau.

112
00:04:48,824 --> 00:04:52,355
Donc changer, ici, le deuxième élément,

113
00:04:52,455 --> 00:04:53,378
celui d'indice 1,

114
00:04:53,902 --> 00:04:56,216
revient à changer la référence,

115
00:04:56,316 --> 00:04:58,659
donc la flèche ne pointe plus vers l'objet "mangue"

116
00:04:58,759 --> 00:05:00,147
mais vers l'objet "melon".

117
00:05:03,124 --> 00:05:04,476
Il nous reste à voir maintenant

118
00:05:04,576 --> 00:05:06,022
comment parcourir les éléments.

119
00:05:07,447 --> 00:05:10,221
Voyons ensemble comment parcourir les éléments d'un tableau.

120
00:05:10,388 --> 00:05:11,893
Donnons-nous un tableau.

121
00:05:13,916 --> 00:05:15,631
La méthode la plus simple

122
00:05:15,731 --> 00:05:19,045
est de parcourir directement les éléments.

123
00:05:19,145 --> 00:05:21,747
Donc ici, notre variable langage

124
00:05:21,847 --> 00:05:26,230
parcourt directement chacune des chaînes de caractères de notre tableau.

125
00:05:28,142 --> 00:05:30,672
On peut aussi parcourir les indices

126
00:05:30,772 --> 00:05:33,412
et accéder aux éléments du tableau

127
00:05:33,512 --> 00:05:35,510
via l'opérateur crochet que nous avons déjà vu.

128
00:05:35,728 --> 00:05:37,213
C'est par exemple nécessaire

129
00:05:37,313 --> 00:05:39,488
si on souhaite modifier l'ordre du parcours

130
00:05:39,588 --> 00:05:42,501
des valeurs de certains éléments.

131
00:05:46,229 --> 00:05:49,338
Un autre cas où le parcours par les indices est obligatoire,

132
00:05:49,438 --> 00:05:51,579
c'est lorsqu'on souhaite parcourir

133
00:05:51,679 --> 00:05:53,056
plusieurs tableaux simultanément.

134
00:05:53,156 --> 00:05:55,446
Ici, nous avons donc deux tableaux, nom et age,

135
00:05:55,546 --> 00:05:58,843
que nous souhaitons parcourir simultanément

136
00:05:58,943 --> 00:06:01,686
puisque chaque prénom

137
00:06:01,786 --> 00:06:03,553
est associé à l'âge

138
00:06:03,653 --> 00:06:06,705
qui se trouve au même indice dans le tableau age.

139
00:06:07,326 --> 00:06:09,976
Et donc ici, nous parcourons les indices

140
00:06:10,076 --> 00:06:12,992
qui sont donc le lien entre le tableau nom et le tableau age.

141
00:06:13,858 --> 00:06:18,710
Voilà. Cela termine notre présentation des tableaux.

142
00:06:18,810 --> 00:06:20,601
Je vous propose de continuer

143
00:06:20,701 --> 00:06:22,036
à manipuler cela

144
00:06:22,136 --> 00:06:25,731
et à compléter les informations avec le document texte

145
00:06:25,831 --> 00:06:27,219
qui est à votre disposition.

