# Scénario du bloc B1, module M2

## Séquence 1 : Introduction et Tableau

1. Vidéo `B1-M2-S1.mp4`
    Cette vidéo introduit le Module 2 qui va présenter les trois types construits :
    - le tableau, 
    - le p-uplet
    - le dictionnaire
    
    Elle enchaine par la première structure : le tableau.

    Pour chacune des structures, vous avez deux entrées possibles et complémentaires :
    - une vidéo présentant l'essentiel des manipulations sur la structure
    - un texte pour approfondir

    L'ordre dans lequel vous abordez les supports n'a pas vraiment d'importance.

2. Texte `bloc_1_section_2_unite_1.md`
3. Quiz Tableau
4. Exercice Creation Tableau 1
5. Exercice Creation Tableau 2
6. Exercice Creation Tableau 3
7. Exercice Recap Tableau 1
8. Exercice Recap Tableau 2
9. Exercice Recap Tableau 3

## Séquence 2 : Le p-uplet

1. Vidéo `B1-M2-S2.mp4`
2. Texte `bloc_1_section_2_unite_2.md`
3. Quiz p-uplet
4. Exercice p-uplet 1
5. Exercice p-uplet 2
6. Exercice p-uplet 3

## Séquence 3 : Le dictionnaire

1. Vidéo `B1-M2-S3.mp4`
2. Texte `bloc_1_section_2_unite_3.md`
3. Quiz Dictionnaire
4. Exercice Dictionnaire 1
5. Exercice Dictionnaire 2
6. Exercice Dictionnaire 3
7. Exercice Dictionnaire 4
8. Exercice Dictionnaire 5
