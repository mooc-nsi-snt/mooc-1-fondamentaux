#  B1-M2 Représentation des données, types construits

## Objectifs

Découvrir ou réviser les trois principaux types structurés et les objets Python qui les représentent : les tableaux, les p-uplets, les dictionnaires.

Les structures, leur construction et leur manipulation seront vues sur de courtes vidéos complétées par des résumés textuels.  

## Prérequis

Une connaissance basique de la programmation Python et des types simples, rappelés au module 1.1

## Sommaire

- 1.2.0 Présentation du Module
- 1.2.1 Les tableaux
- 1.2.2 Les p-uplets
- 1.2.3 Les dictionnaires 

## Temps d'apprentissage : 4 à 6 heures
*Ce temps est donné à titre indicatif. Il peut varier en fonction des participants.*

## Enseignant

**Sébastien Hoarau**

Maitre de Conférence en Informatique à l’Université de la Réunion et membre de l'IREM de la Réunion. Il enseigne l’initiation à la programmation impérative avec le langage Python3 à des 1ère années scientifiques. Initiation aux techno du web aussi : HTML, CSS, JavaScript
