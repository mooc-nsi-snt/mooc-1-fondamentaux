# B1-M1. Représentation des données : types et valeurs de base

## Objectifs

[![Vidéo 0 B1-M1-S0 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M1-S0.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M1-S0.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M1/B1-M1-S0-video-intro.srt" target="_blank">Sous-titre de la vidéo</a>

### Pr&eacute;requis

<p>Aucun pr&eacute;requis n'est n&eacute;cessaire pour suivre ce module</p>

### Support 

<p>Ce Module s'appuie sur le support de cours écrit portant le même titre : <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/raw/master/__supports-de-cours/B1-M1/texte-mooc-1-1-1_pdf_initial/main.pdf" target=_"blank"><strong>Représentation des données : types et valeurs de base</strong></a></p>

### Sommaire : 

- 1. Représentation binaire de l'information
- 2. Unité de quantité d'information
- 3. Écriture des nombres dans différentes bases
    - 3-1. Introduction : la notion de base
    - 3-2. Changement de base
    - 3-3. Manipuler les données en binaire
    - 3-4. Nombres signés
    - 3-5. Nombres avec partie fractionnaire
- 4. Représentation du texte
- 5. Représentation d'images, de sons, de vidéos
- 6. Exercices

####  Temps d'investissement : 4 à 6 heures
*Ce temps est donné à titre indicatif. Il peut varier en fonction des participants.*

## Enseignant

**Gilles Geeraerts, Université Libre de Bruxelles**

Gilles Geeraerts est docteur en sciences informatiques de l’Université libre de Bruxelles. Il y est chargé de cours et y enseigne les principes de base du fonctionnnement des ordinateurs, ainsi que la théorie des langages et de la compilation.

[Site web de l'enseignant](http://di.ulb.ac.be/verif/ggeeraer/#Teaching)
