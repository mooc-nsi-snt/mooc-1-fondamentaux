### Q2. Requêtes avec imbrication sur la base des voyageurs

Pour les requêtes suivantes, en revanche, vous avez droit à
l'imbrication (il serait difficile de faire autrement).

> -   a) Nom des voyageurs qui ne sont pas allés en Corse
> -   b) Noms des voyageurs qui ne vont qu'en Corse s'ils vont quelque
>     part
> -   c) Nom des logements sans piscine
> -   d) Nom des voyageurs qui ne sont allés nulle part
> -   e) Les logements où personne n'est allé
> -   f) Les voyageurs qui n'ont jamais eu l'occasion de faire de la
>     plongée
> -   g) Les voyageurs et les logements où ils n'ont jamais séjourné
> -   h) Les logements où tout le monde est allé

Vous pouvez finalement reprendre quelques-unes des requêtes précédentes
et les exprimer avec l'imbrication.


