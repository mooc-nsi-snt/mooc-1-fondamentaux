# B1-M4 Bases de données

Les contenus de ce module sont extraits du cours de **Philippe Rigaux** **"Cours de bases de données - Modèles et langages"**

Ce cours est mis à disposition selon les termes de la licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International. Cf. http://creativecommons.org/licenses/by-nc-sa/4.0/.

## Objectifs du module

Ce support de cours s’adresse à tous ceux qui veulent <em>concevoir, implanter, alimenter et interroger</em> une base de données (BD), et intégrer cette BD à une application. Dans un contexte académique, il s’adresse aux étudiants en troisième année de Licence (L3). Dans un contexte professionnel, le contenu du cours présente tout ce qu’il est nécessaire de maîtriser quand on conçoit une BD ou que l’on développe des applications qui s’appuient sur une BD. Au-delà de ce public principal, toute personne désirant comprendre les principes, méthodes et outils des systèmes de gestion de données trouvera un intérêt à lire les chapitres consacrés à la conception, à l’interrogation et à la programmation SQL, pour ne citer que les principaux.

## Prérequis

Aucun prérequis spécifique, mais ce module s'adresse à des personnes déjà initiées à l'informatique ce qui est le cas à ce stade de cette formation.Les modules précédents permettent de se familiariser avec la notion de table.</p>

## Temps d'apprentissage

 Une 20taine à une 30taine d'heures selon l'approfondissement.

## Enseignant
### Philippe Rigaux
est professeur des universités au Conservatoire des Arts et Métiers.

