# Fusion de table

- 1.3.1 Introduction sur les données en table
- 1.3.2 Recherches dans une table
- 1.3.3 Trier des données
- 1.3.4 **Fusion de tables**
- 1.3.5 Sauvegarde des données
- 1.3.6 Jointure

####

- Sébastien Hoarau.  Manipuler des données en table : la fusion de tables.

Dans ces trois sections : Fusion de Tables, Sauvegarde des données et Jointure,  

- comme précédemment pour le tri des données on vous invite à suivre ces vidéos, tout en manipulant vous-même sur l'interface de façon à acquérir à la fois les savoir et les savoir-faire sur tous les aspects du sujet ; ainsi l'exercice pratique associé à ce stade est de reproduire puis essayer des variantes ou tenter des extensions des fonctions proposées ; en cas de doute ou de besoin de précisions, ne pas hésiter à [questionner le forum](https://mooc-forums.inria.fr/moocnsi/c/representation-des-donnees/a-propos-du-mooc/201) ;

- de plus, la notion de données en table est inhérente à celle de base de données relationnelles qui utilisent largement des tables et qu'on abordera à la section suivante, 1.4 ; ici nous travaillons sur le "coeur" de ces représentations pour soulever un peu le "capot du moteur" et se préparer aux concepts plus complexes qui vont suivre.


[![Vidéo 1 B1-M3-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M3-S4-V1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M3-S4-a1.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M3/B1-M3-S4a.srt" target="_blank">Sous-titre de la vidéo</a> 

[![Vidéo 2 B1-M3-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M3-S4-a2.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M3-S4-a2.mp4) 

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M3/B1-M3-S4b.srt" target="_blank">Sous-titre de la vidéo</a> 


