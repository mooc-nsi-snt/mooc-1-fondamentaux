# Introduction sur les données en table

- 1.3.1 **Introduction sur les données en table**
- 1.3.2 Recherches dans une table
- 1.3.3 Trier des données
- 1.3.4 Fusion de tables
- 1.3.5 Sauvegarde des données
- 1.3.6 Jointure


####

- Sébastien Hoarau. Comment utiliser nos tableaux et nos dictionnaires pour manipuler nos données en tables.

[![Vidéo 1 B1-M3-S1](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M3-S1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M3-S1.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M3/B1-M3-S1.srt" target="_blank">Sous-titre de la vidéo</a> 

## Documents  complémentaires

<a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/Le_Mooc/bloc_1_repr_donnees_information/1-3_donnees_en_tables/1-3-1_Introduction_sur_les_donn%C3%A9es_en_tables/1-3-1-1-Vocabulaire.html" target="_blank">Fiche Vocabulaire des données en table</a>

<a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/Le_Mooc/bloc_1_repr_donnees_information/1-3_donnees_en_tables/1-3-1_Introduction_sur_les_donn%C3%A9es_en_tables/1-3-1-2_Fiche-texte.html" target="_blank">Texte :  De la feuille de tableur au format CSV</a>





















